package TestCases;

import java.lang.reflect.Method;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import automationFramework.GenericKeywords;
import baseClass.BaseClass;
import scenarios.General.General;
import scenarios.LabTests.LabTests;
import scenarios.Login_Logout.Login_Logout;
import scenarios.PGP_PDP.PGP_PDP;
import scenarios.Register.Register;
import scenarios.Search.Search;
import scenarios.cart.Cart;
import scenarios.checkout.Checkout;
import scenarios.compareProducts.CompareProducts;
import scenarios.myAccount.MyAccount;
import scenarios.newsletter.Newsletter;
import scenarios.wholesale.Wholesale;

@Listeners({ utilities.HtmlReport.class })
public class TestCases {
	private BaseClass obj;

	////////////////////////////////////////////////////////////////////////////////
	// Function Name :
	// Purpose :
	// Parameters :
	// Return Value :
	// Created by :
	// Created on :
	// Remarks :
	/////////////////////////////////////////////////////////////////////////////////

	private void TestStart(String testCaseName) {

		obj.currentTestCaseName = testCaseName;
		obj.setEnvironmentTimeouts();
		obj.reportStart();
		obj.iterationCount.clear();
		obj.iterationCountInTextData();

	}

	@BeforeClass
	@Parameters({ "selenium.machinename", "selenium.host", "selenium.port", "selenium.browser", "selenium.os",
			"selenium.browserVersion", "selenium.osVersion", "TestData.SheetNumber" })
	public void precondition(String machineName, String host, String port, String browser, String os,
			String browserVersion, String osVersion, String sheetNo) {
		obj = new BaseClass();
		if (os.contains("Android")) {
			obj.startServer(host, port);
			obj.waitTime(10);
		}
		obj.setup(machineName, host, port, browser, os, browserVersion, osVersion, sheetNo);
	}

	@Test(alwaysRun = true)
	public void General_ML_TS_001(Method method) {
		TestStart(method.getName());
		General general = new General(obj);
		general.dataRowNo = Integer.parseInt(general.iterationCount.get(0).toString());
		general.verifyApplicationLaunchedSuccessfully();
		obj.testFailure = general.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void General_ML_TS_002(Method method) {
		TestStart(method.getName());
		General general = new General(obj);
		general.dataRowNo = Integer.parseInt(general.iterationCount.get(0).toString());
		general.verifyHeader();
		obj.testFailure = general.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void General_ML_TS_003(Method method) {
		TestStart(method.getName());
		General general = new General(obj);
		general.dataRowNo = Integer.parseInt(general.iterationCount.get(0).toString());
		general.verifyFooter();
		obj.testFailure = general.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void General_ML_TS_004(Method method) {
		TestStart(method.getName());
		General general = new General(obj);
		general.dataRowNo = Integer.parseInt(general.iterationCount.get(0).toString());
		general.verifyLogoLink();
		obj.testFailure = general.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Register_ML_TS_005(Method method) {
		TestStart(method.getName());
		Register register = new Register(obj);
		register.dataRowNo = Integer.parseInt(register.iterationCount.get(0).toString());
		register.register_ValidDetails();
		obj.testFailure = register.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Register_ML_TS_006(Method method) {
		TestStart(method.getName());
		Register register = new Register(obj);
		register.dataRowNo = Integer.parseInt(register.iterationCount.get(0).toString());
		register.register_InvalidDetails();
		obj.testFailure = register.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Login_ML_TS_007(Method method) {
		TestStart(method.getName());
		Login_Logout login = new Login_Logout(obj);
		login.dataRowNo = Integer.parseInt(login.iterationCount.get(0).toString());
		login.login_ValidDetails();
		obj.testFailure = login.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Logout_ML_TS_011(Method method) {
		TestStart(method.getName());
		Login_Logout login = new Login_Logout(obj);
		login.dataRowNo = Integer.parseInt(login.iterationCount.get(0).toString());
		login.logout();
		obj.testFailure = login.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Login_ML_TS_008(Method method) {
		TestStart(method.getName());
		Login_Logout login = new Login_Logout(obj);
		login.dataRowNo = Integer.parseInt(login.iterationCount.get(0).toString());
		login.login_InvalidDetails();
		obj.testFailure = login.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void CreateAccount_ML_TS_010(Method method) {
		TestStart(method.getName());
		Register register = new Register(obj);
		register.createAccount_VerifySignUpForNewsletter();
		register.dataRowNo = Integer.parseInt(register.iterationCount.get(0).toString());
		obj.testFailure = register.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Search_ML_TS_014(Method method) {
		TestStart(method.getName());
		Search search = new Search(obj);
		search.dataRowNo = Integer.parseInt(search.iterationCount.get(0).toString());
		search.invalid_Search();
		obj.testFailure = search.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Search_ML_TS_012_013(Method method) {
		TestStart(method.getName());
		Search search = new Search(obj);
		search.dataRowNo = Integer.parseInt(search.iterationCount.get(0).toString());
		search.valid_Search();
		obj.testFailure = search.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Newsletter_ML_TS_131(Method method) {
		TestStart(method.getName());
		Newsletter newsletter = new Newsletter(obj);
		newsletter.dataRowNo = Integer.parseInt(newsletter.iterationCount.get(0).toString());
		newsletter.submitInvalidDetails();
		obj.testFailure = newsletter.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Newsletter_ML_TS_015_016(Method method) {
		TestStart(method.getName());
		Newsletter newsletter = new Newsletter(obj);
		newsletter.dataRowNo = Integer.parseInt(newsletter.iterationCount.get(0).toString());
		newsletter.submitValidDetails();
		obj.testFailure = newsletter.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Newsletter_ML_TS_017(Method method) {
		TestStart(method.getName());
		Newsletter newsletter = new Newsletter(obj);
		newsletter.dataRowNo = Integer.parseInt(newsletter.iterationCount.get(0).toString());
		newsletter.verifyCouponCode();
		obj.testFailure = newsletter.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void PGP_ML_TS_027(Method method) {
		TestStart(method.getName());
		PGP_PDP pgp_pdp = new PGP_PDP(obj);
		pgp_pdp.dataRowNo = Integer.parseInt(pgp_pdp.iterationCount.get(0).toString());
		pgp_pdp.addToCart_PGP();
		obj.testFailure = pgp_pdp.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void PGP_ML_TS_028(Method method) {
		TestStart(method.getName());
		PGP_PDP pgp_pdp = new PGP_PDP(obj);
		pgp_pdp.dataRowNo = Integer.parseInt(pgp_pdp.iterationCount.get(0).toString());
		pgp_pdp.addProductToComparisonCart_PGP();
		obj.testFailure = pgp_pdp.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void PGP_ML_TS_030(Method method) {
		TestStart(method.getName());
		PGP_PDP pgp_pdp = new PGP_PDP(obj);
		pgp_pdp.dataRowNo = Integer.parseInt(pgp_pdp.iterationCount.get(0).toString());
		pgp_pdp.notAbleToAddProductToWishList_GuestUser_PGP();
		obj.testFailure = pgp_pdp.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void PGP_ML_TS_029(Method method) {
		TestStart(method.getName());
		PGP_PDP pgp_pdp = new PGP_PDP(obj);
		pgp_pdp.dataRowNo = Integer.parseInt(pgp_pdp.iterationCount.get(0).toString());
		pgp_pdp.AddProductToWishList_SignedInUser_PGP();
		obj.testFailure = pgp_pdp.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void PGP_ML_TS_031(Method method) {
		TestStart(method.getName());
		PGP_PDP pgp_pdp = new PGP_PDP(obj);
		pgp_pdp.dataRowNo = Integer.parseInt(pgp_pdp.iterationCount.get(0).toString());
		pgp_pdp.pgpTopdp_Verification();
		obj.testFailure = pgp_pdp.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void PDP_ML_TS_037(Method method) {
		TestStart(method.getName());
		PGP_PDP pgp_pdp = new PGP_PDP(obj);
		pgp_pdp.dataRowNo = Integer.parseInt(pgp_pdp.iterationCount.get(0).toString());
		pgp_pdp.modifyCountAndaddToCart();
		obj.testFailure = pgp_pdp.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void PDP_ML_TS_038(Method method) {
		TestStart(method.getName());
		PGP_PDP pgp_pdp = new PGP_PDP(obj);
		pgp_pdp.dataRowNo = Integer.parseInt(pgp_pdp.iterationCount.get(0).toString());
		pgp_pdp.addProductToComparisonCart_PDP();
		obj.testFailure = pgp_pdp.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void PDP_ML_TS_040(Method method) {
		TestStart(method.getName());
		PGP_PDP pgp_pdp = new PGP_PDP(obj);
		pgp_pdp.dataRowNo = Integer.parseInt(pgp_pdp.iterationCount.get(0).toString());
		pgp_pdp.notAbleToAddProductToWishList_GuestUser_PDP();
		obj.testFailure = pgp_pdp.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void PDP_ML_TS_039(Method method) {
		TestStart(method.getName());
		PGP_PDP pgp_pdp = new PGP_PDP(obj);
		pgp_pdp.dataRowNo = Integer.parseInt(pgp_pdp.iterationCount.get(0).toString());
		pgp_pdp.AddProductToWishList_SignedInUser_PDP();
		obj.testFailure = pgp_pdp.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void MiniCart_ML_TS_044(Method method) {
		TestStart(method.getName());
		scenarios.miniCart.MiniCart miniCart = new scenarios.miniCart.MiniCart(obj);
		miniCart.dataRowNo = Integer.parseInt(miniCart.iterationCount.get(0).toString());
		miniCart.verifyMiniCartProductNumberDisplayed();
		obj.testFailure = miniCart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void MiniCart_ML_TS_045(Method method) {
		TestStart(method.getName());
		scenarios.miniCart.MiniCart miniCart = new scenarios.miniCart.MiniCart(obj);
		miniCart.dataRowNo = Integer.parseInt(miniCart.iterationCount.get(0).toString());
		miniCart.modifyQuantityinMiniCart();
		obj.testFailure = miniCart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void MiniCart_ML_TS_046_049(Method method) {
		TestStart(method.getName());
		scenarios.miniCart.MiniCart miniCart = new scenarios.miniCart.MiniCart(obj);
		miniCart.dataRowNo = Integer.parseInt(miniCart.iterationCount.get(0).toString());
		miniCart.editProductFromMiniCart();
		obj.testFailure = miniCart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void MiniCart_ML_TS_047(Method method) {
		TestStart(method.getName());
		scenarios.miniCart.MiniCart miniCart = new scenarios.miniCart.MiniCart(obj);
		miniCart.dataRowNo = Integer.parseInt(miniCart.iterationCount.get(0).toString());
		miniCart.verifyDeleteProductFromMiniCart();
		obj.testFailure = miniCart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void MiniCart_ML_TS_048(Method method) {
		TestStart(method.getName());
		scenarios.miniCart.MiniCart miniCart = new scenarios.miniCart.MiniCart(obj);
		miniCart.dataRowNo = Integer.parseInt(miniCart.iterationCount.get(0).toString());
		miniCart.verifyCartSubTotal();
		obj.testFailure = miniCart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void MiniCart_ML_TS_050(Method method) {
		TestStart(method.getName());
		scenarios.miniCart.MiniCart miniCart = new scenarios.miniCart.MiniCart(obj);
		miniCart.dataRowNo = Integer.parseInt(miniCart.iterationCount.get(0).toString());
		miniCart.verifyViewAndEditCart();
		obj.testFailure = miniCart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void MiniCart_ML_TS_051(Method method) {
		TestStart(method.getName());
		scenarios.miniCart.MiniCart miniCart = new scenarios.miniCart.MiniCart(obj);
		miniCart.dataRowNo = Integer.parseInt(miniCart.iterationCount.get(0).toString());
		miniCart.verifyGoToCheckout();
		obj.testFailure = miniCart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Cart_ML_TS_052(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.verifyDeleteProductFromCart();
		obj.testFailure = cart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Cart_ML_TS_053(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.verifyUpdateQuantityInCart();
		obj.testFailure = cart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Cart_ML_TS_054(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.applyValidCoupon();
		obj.testFailure = cart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Cart_ML_TS_055(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.applyInvalidCoupon();
		obj.testFailure = cart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Cart_ML_TS_056(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.verifyPriceUpdateInCart();
		obj.testFailure = cart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Cart_ML_TS_058(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.verifyPriceForShippingMethods();
		obj.testFailure = cart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Cart_ML_TS_059(Method method) {
		// Works only for Prod. Since tax is disabled for lower env.
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.verifySalesTax();
		obj.testFailure = cart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void CheckoutShipping_ML_TS_063(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_InvalidShippingDetails();
		obj.testFailure = checkout.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void CheckoutShipping_ML_TS_062(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_ValidShippingDetails();
		obj.testFailure = checkout.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void CheckoutShipping_ML_TS_065(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.verifyPriceForShippingMethods();
		obj.testFailure = checkout.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void CheckoutShipping_ML_TS_066(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_verifyLoginSectionInCheckout();
		obj.testFailure = checkout.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void CheckoutShipping_ML_TS_067(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_verifyLoginInCheckout();
		obj.testFailure = checkout.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void CheckoutShipping_ML_TS_068(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_verifySavedAddressSectionInCheckout();
		obj.testFailure = checkout.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void CheckoutShipping_ML_TS_069(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_verifyNewAddressSectionInCheckout();
		obj.testFailure = checkout.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void CheckoutPayments_ML_TS_071(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_verifyBillingAddressAutofilledSameAsShippingAddress();
		obj.testFailure = checkout.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void CheckoutPayments_ML_TS_072(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_verifyBillingFormDisplayed();
		obj.testFailure = checkout.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void CheckoutPayments_ML_TS_074(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_InvalidBillingDetails();
		obj.testFailure = checkout.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void CheckoutPayments_ML_TS_073(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_ValidBillingDetails();
		obj.testFailure = checkout.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void CheckoutPayments_ML_TS_075(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_NewBillingDetails();
		obj.testFailure = checkout.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void CheckoutPayments_ML_TS_078_079(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_ApplyInvalidAndValidCoupon();
		obj.testFailure = checkout.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void CheckoutPayments_ML_TS_084(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_InvalidCreditCardDetails();
		obj.testFailure = checkout.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void CheckoutPayments_ML_TS_086(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_CompleteCheckoutWithValidDetails_SignedInUser();
		obj.testFailure = checkout.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void CheckoutPayments_ML_TS_087(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_CompleteCheckoutWithValidDetails_GuestUser();
		obj.testFailure = checkout.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Wholesale_ML_TS_095(Method method) {
		TestStart(method.getName());
		Wholesale wholesale = new Wholesale(obj);
		wholesale.dataRowNo = Integer.parseInt(wholesale.iterationCount.get(0).toString());
		wholesale.wholesale_submitWithvalidDetails();
		obj.testFailure = wholesale.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Wholesale_ML_TS_096(Method method) {
		TestStart(method.getName());
		Wholesale wholesale = new Wholesale(obj);
		wholesale.dataRowNo = Integer.parseInt(wholesale.iterationCount.get(0).toString());
		wholesale.wholesale_submitWithInvalidDetails();
		obj.testFailure = wholesale.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void MyAccount_ML_TS_099(Method method) {
		TestStart(method.getName());
		MyAccount myAccount = new MyAccount(obj);
		myAccount.dataRowNo = Integer.parseInt(myAccount.iterationCount.get(0).toString());
		myAccount.myAccount_EditbasicDetails();
		obj.testFailure = myAccount.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void MyAccount_ML_TS_100(Method method) {
		TestStart(method.getName());
		MyAccount myAccount = new MyAccount(obj);
		myAccount.dataRowNo = Integer.parseInt(myAccount.iterationCount.get(0).toString());
		myAccount.myAccount_ChangePassword();
		obj.testFailure = myAccount.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void MyAccount_ML_TS_101(Method method) {
		TestStart(method.getName());
		MyAccount myAccount = new MyAccount(obj);
		myAccount.dataRowNo = Integer.parseInt(myAccount.iterationCount.get(0).toString());
		myAccount.myAccount_ChangeEmail();
		obj.testFailure = myAccount.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void MyAccount_ML_TS_103(Method method) {
		TestStart(method.getName());
		MyAccount myAccount = new MyAccount(obj);
		myAccount.dataRowNo = Integer.parseInt(myAccount.iterationCount.get(0).toString());
		myAccount.myAccount_verifyNewsletterSubscription();
		obj.testFailure = myAccount.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void MyAccountWishList_ML_TS_114(Method method) {
		TestStart(method.getName());
		MyAccount myAccount = new MyAccount(obj);
		myAccount.dataRowNo = Integer.parseInt(myAccount.iterationCount.get(0).toString());
		myAccount.myAccount_verifyAddAllToCartFromWishList();
		obj.testFailure = myAccount.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void MyAccountWishList_ML_TS_115(Method method) {
		TestStart(method.getName());
		MyAccount myAccount = new MyAccount(obj);
		myAccount.dataRowNo = Integer.parseInt(myAccount.iterationCount.get(0).toString());
		myAccount.myAccount_verifyDeleteAllProductsFromWishList();
		obj.testFailure = myAccount.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void CompareProducts_ML_TS_126(Method method) {
		TestStart(method.getName());
		CompareProducts compareProducts = new CompareProducts(obj);
		compareProducts.dataRowNo = Integer.parseInt(compareProducts.iterationCount.get(0).toString());
		compareProducts.addProductToComparisonList();
		obj.testFailure = compareProducts.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void CompareProducts_ML_TS_127(Method method) {
		TestStart(method.getName());
		CompareProducts compareProducts = new CompareProducts(obj);
		compareProducts.dataRowNo = Integer.parseInt(compareProducts.iterationCount.get(0).toString());
		compareProducts.removeProductFromComparisonList();
		obj.testFailure = compareProducts.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void CompareProducts_ML_TS_128(Method method) {
		TestStart(method.getName());
		CompareProducts compareProducts = new CompareProducts(obj);
		compareProducts.dataRowNo = Integer.parseInt(compareProducts.iterationCount.get(0).toString());
		compareProducts.clearComparisonList();
		obj.testFailure = compareProducts.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void LabTests_ML_TS_132(Method method) {
		TestStart(method.getName());
		LabTests labTests = new LabTests(obj);
		labTests.dataRowNo = Integer.parseInt(labTests.iterationCount.get(0).toString());
		labTests.verifyValidSearch();
		obj.testFailure = labTests.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void LabTests_ML_TS_133(Method method) {
		TestStart(method.getName());
		LabTests labTests = new LabTests(obj);
		labTests.dataRowNo = Integer.parseInt(labTests.iterationCount.get(0).toString());
		labTests.verifyInvalidSearch();
		obj.testFailure = labTests.testFailure;
		TestEnd();
	}

	@AfterClass
	public void closeSessions() {
		obj.closeAllSessions();
	}

	private void TestEnd() {
		obj.waitTime(1);
		if (obj.testFailure) {
			GenericKeywords.testFailed();
		}
	}

	@BeforeMethod
	public void before() {
		obj.testFailure = false;
	}

}
