package pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;

public class CompareProducts extends ApplicationKeywords {

	private static final String productsPresent = "Products Present #xpath=//td//strong[contains(@class,'product')]/a";
	private static final String deleteProduct = "Delete Product #xpath=//td[contains(@class,'remove')]//a[contains(@class,'delete')]";
	private static final String alertMessage_OkButton = "Alert #xpath=//span[contains(text(),'OK')]";
	private static final String comparisonListEmptyMessage = "Comparison List Empty Message #xpath=//div/div[contains(text(),'no items to compare')]";

	public CompareProducts(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method to verify presence of the expected product in the compare
	 * products page.
	 * 
	 */
	public void verifyPresenceOfExpectedProduct(String productName) {
		try {
			boolean productPresent = false;
			scrollToViewElement(productsPresent);
			waitForElementToDisplay(productsPresent, 30);
			if (isElementDisplayed(productsPresent)) {
				manualScreenshot("The Product added is present in the compare products page");
				List<WebElement> products = new ArrayList<WebElement>();
				products = findWebElements(productsPresent);
				for (WebElement product : products) {
					if (product.getText().toLowerCase().contains(productName.toLowerCase())) {
						productPresent = true;
						highLighterMethod(product);
						break;
					}
				}
				if (productPresent == true) {
					testStepInfo("The product added to cart " + productName + ", found in compare products page");
				} else
					testStepFailed("Product added to cart " + productName + ", not found in compare products page");
			} else
				testStepFailed("No products present in compare products page");
		} catch (Exception e) {
			testStepFailed("Presence of expected product could not be verified");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to remove all products in Comparison List page
	 */
	public void removeAllProducts() {
		List<WebElement> removeProducts;
		try {
			waitForElementToDisplay(deleteProduct, 15);
			if (isElementDisplayed(deleteProduct) == false) {
				testStepInfo("No Products in Comparison List");
			} else {
				removeProducts = new ArrayList<WebElement>();
				removeProducts = findWebElements(deleteProduct);
				int totalProducts = removeProducts.size();
				if (totalProducts > 0) {
					for (int removeCounter = 0; removeCounter < totalProducts; removeCounter++) {
						removeProducts = new ArrayList<WebElement>();
						removeProducts = findWebElements(deleteProduct);
						for (WebElement removeTheProduct : removeProducts) {
							highLighterMethod(removeTheProduct);
							waitForElementToDisplay(deleteProduct, 20);
							scrollToViewElement(deleteProduct);
							removeTheProduct.click();
							waitForElementToDisplay(alertMessage_OkButton, 10);
							clickOn(alertMessage_OkButton);
							waitTime(3);
							break;
						}
//						if (removeCounter != totalProducts - 1) {
//							removeProducts = new ArrayList<WebElement>();
//							removeProducts = findWebElements(deleteProduct);
//						}
					}
					testStepInfo("All Products Removed from Wish List");
				} else
					testStepInfo("No products to remove from wishList");
			}
		} catch (Exception e) {
			testStepFailed("All products removal from Wish List was not successful");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on remove product in comparison page
	 */
	public void clickRemoveProduct() {
		List<WebElement> removeProducts;
		try {
			waitForElementToDisplay(deleteProduct, 15);
			if (isElementDisplayed(deleteProduct) == false) {
				testStepInfo("No Products in Comparison List");
			} else {
				removeProducts = new ArrayList<WebElement>();
				removeProducts = findWebElements(deleteProduct);
				int totalProducts = removeProducts.size();
				if (totalProducts > 0) {
					for (int removeCounter = 0; removeCounter < totalProducts; removeCounter++) {
						removeProducts = new ArrayList<WebElement>();
						removeProducts = findWebElements(deleteProduct);
						for (WebElement removeTheProduct : removeProducts) {
							highLighterMethod(removeTheProduct);
							waitForElementToDisplay(deleteProduct, 20);
							scrollToViewElement(deleteProduct);
							removeTheProduct.click();
							clickOn(alertMessage_OkButton);
							waitTime(3);
							break;
						}
						break;
					}
				} else
					testStepInfo("No products to remove from wishList");
			}
		} catch (Exception e) {
			testStepFailed("Could not click on remove product");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify Comparison List is empty.
	 */
	public void verifyComparisonListEmptyMessage() {
		try {
			waitForElementToDisplay(comparisonListEmptyMessage, 60);
			if (isElementDisplayed(comparisonListEmptyMessage)) {
				highLighterMethod(comparisonListEmptyMessage);
				testStepInfo("The Comparison list is empty");
			} else
				testStepFailed("The Comparison list is not empty");
		} catch (Exception e) {
			testStepFailed("The Comparison list message was not displayed");
			e.printStackTrace();
		}
	}

}