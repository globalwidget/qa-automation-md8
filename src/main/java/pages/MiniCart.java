package pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;

public class MiniCart extends ApplicationKeywords {

	private static final String productsPresent = "Products Present #xpath=//strong[@class='product-item-name']/a";
	private static final String removeProduct = "Remove Product #xpath=//span[contains(text(),'Remove')]/..";
	private static final String quantityUpdate = "Quantity Update Box #xpath=//input[contains(@id,'qty')]";
	private static final String updateCartIcon = "Update Cart button #xpath=//button[contains(@id,'update')]";
	private static final String proceedToCheckout = "Proceed Checkout #xpath=//button[@title='Proceed to Checkout']";
	private static final String viewEditCart = "View and Edit Cart #xpath=//span[contains(text(),'View and Edit Cart')]/..";
	private static final String alertMessage_OkButton = "Alert #xpath=//span[contains(text(),'OK')]";
	private static final String productQuantityDisplayed = "Product Quantity Displayed #xpath=//a[contains(@href,'cart')]//span[@class='counter-number']";
	private static final String productsPresentOrNot = "Products Present or Not #xpath=//button[contains(@id,'minicart')]/following-sibling::strong";
	private static final String productPrices = "Product Prices #xpath=//span[contains(@class,'minicart-price')]/span";
	private static final String cartSubtotal = "Cart Subtotal #xpath=//div[contains(@class,'amount ')]/span[contains(@class,'price')]";
//	private static final String miniCartEmptyMessage = "Mini Cart Empty #xpath=//strong[contains(text(),'You have no items in your shopping cart.')]";

	public MiniCart(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method to verify presence of the expected product in the Mini
	 * Cart
	 * 
	 */
	public void verifyPresenceOfExpectedProduct(String productName) {
		try {
			boolean productPresent = false;
			if (isElementPresent(productsPresent)) {
				manualScreenshot("The Product added is present in the cart");
				List<WebElement> products = new ArrayList<WebElement>();
				products = findWebElements(productsPresent);
				for (WebElement product : products) {
					if (product.getText().toLowerCase().contains(productName.toLowerCase())) {
						productPresent = true;
						highLighterMethod(product);
						break;
					}
				}
				if (productPresent == true) {
					testStepInfo("The product added to cart " + productName + ", found in My Cart page");
				} else
					testStepFailed("Product added to cart " + productName + ", not found in My Cart page");
			} else
				testStepFailed("No products present in My cart page");
		} catch (Exception e) {
			testStepFailed("Presence of expected product could not be verified");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on delete button in Mini Cart page
	 */
	public String clickOnRemoveProduct() {
		String productName = "";
		try {
			if (isElementDisplayed(removeProduct)) {
				highLighterMethod(removeProduct);
				productName = findWebElements(productsPresent).get(0).getText();
				findWebElements(removeProduct).get(0).click();
				clickOn(alertMessage_OkButton);
				GOR.productAdded = false;
				testStepInfo("The remove button was clicked for the product " + productName);
				return productName;
			} else
				testStepFailed("Could not find any product in My Cart");
		} catch (Exception e) {
			testStepFailed("Could not remove products from cart");
			e.printStackTrace();
		}
		return productName;
	}

	/**
	 * Description: Method to click on View and Edit Mini Cart
	 */
	public void clickViewAndEditCart() {
		try {
			if (isElementDisplayed(viewEditCart)) {
				highLighterMethod(viewEditCart);
				clickOn(viewEditCart);
			} else
				testStepFailed("The View and Edit Cart button is not displayed");
		} catch (Exception e) {
			testStepFailed("The View and Edit Cart button could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Go To Checkout
	 */
	public void clickGoToCheckout() {
		try {
			if (isElementDisplayed(proceedToCheckout)) {
				highLighterMethod(proceedToCheckout);
				clickOn(proceedToCheckout);
			} else
				testStepFailed("The Go To Checkout button is not displayed");
		} catch (Exception e) {
			testStepFailed("The Go To Checkout button could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to remove all products in Mini Cart page
	 */
	public void removeAllProducts() {
		try {
			List<WebElement> removeProducts;
			int counter = 0;
			if (getAttributeValue(productsPresentOrNot, "class").contains("empty")) {
				testStepInfo("No Products in Cart");
			} else {

				removeProducts = new ArrayList<WebElement>();
				removeProducts = findWebElements(removeProduct);
				int totalProducts = removeProducts.size();
				GOR.productAdded = false;
				for (int removeCounter = 0; removeCounter < totalProducts; removeCounter++) {
					removeProducts = new ArrayList<WebElement>();
					removeProducts = findWebElements(removeProduct);
					for (WebElement removeTheProduct : removeProducts) {
						highLighterMethod(removeTheProduct);
						waitForElementToDisplay(removeProduct, 60);
						waitTime(1);
						removeTheProduct.click();
						waitForElementToDisplay(alertMessage_OkButton, 60);
						clickOn(alertMessage_OkButton);
						++counter;
						waitTime(4);
						break;
					}
					if (counter == totalProducts) {
						break;
					}
				}
				testStepInfo("All Products Removed");
			}
		} catch (

		Exception e) {
			testStepFailed("All products removal from mini cart was not successful");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify empty MiniCart
	 */
	public void verifyEmptyMiniCart() {
		try {
			waitTime(4);
			if (getAttributeValue(productsPresentOrNot, "class").contains("empty")) {
				highLighterMethod(productsPresentOrNot);
				testStepInfo("The Mini Cart is empty");
			} else {
				testStepFailed("The Mini Cart is not empty");
			}
		} catch (Exception e) {
			testStepFailed("The Mini Cart is not empty");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to get the product quantity displayed in the Mini Cart.
	 */
	public int getProductQuantityDisplayed() {
		try {
			if (isElementDisplayed(productQuantityDisplayed)) {
				scrollToViewElement(productQuantityDisplayed);
				highLighterMethod(productQuantityDisplayed);
				int quantity = Integer.parseInt(getText(productQuantityDisplayed));
				return quantity;
			} else
				testStepFailed("The product quantity displayed in the Mini Cart could not be fetched",
						"Element not displayed");
		} catch (Exception e) {
			testStepFailed("The product quantity displayed in the Mini Cart could not be fetched");
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * Description: Method to update the cart quantity of the first product in the
	 * cart
	 */
	public void updateCartQuantity(int quantity) {
		try {
			if (isElementDisplayed(quantityUpdate)) {
				highLighterMethod(quantityUpdate);
				findWebElements(quantityUpdate).get(0).click();
				findWebElement(quantityUpdate).sendKeys(Keys.END);
				findWebElement(quantityUpdate).sendKeys(Keys.BACK_SPACE);
				findWebElement(quantityUpdate).sendKeys(Keys.BACK_SPACE);
				findWebElement(quantityUpdate).sendKeys(Keys.BACK_SPACE);
				findWebElements(quantityUpdate).get(0).sendKeys(String.valueOf(quantity));
			} else
				testStepFailed("The box to update quantity of the product is not found");
		} catch (Exception e) {
			testStepFailed("Could not update quantity of the product");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Update Cart Icon
	 */
	public void clickUpdateProductCountIcon() {
		try {
			if (isElementDisplayed(updateCartIcon)) {
				highLighterMethod(updateCartIcon);
				clickOn(updateCartIcon);
			} else
				testStepFailed("The Update Cart button is not displayed");
		} catch (Exception e) {
			testStepFailed("The Update Cart button could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to select the first product
	 */
	public void selectFirstProduct() {
		try {
			if (isElementDisplayed(productsPresent)) {
				WebElement firstProduct = findWebElements(productsPresent).get(0);
				highLighterMethod(firstProduct);
				firstProduct.click();
			} else
				testStepFailed("No Products Present");
		} catch (Exception e) {
			testStepFailed("Could not click on the first Product");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to get title of the first product.
	 */
	public String getFirstProductTitle() {
		try {
			if (isElementPresent(productsPresent)) {
				WebElement firstProductTitle = findWebElements(productsPresent).get(0);
				highLighterMethod(firstProductTitle);
				return firstProductTitle.getText();
			} else {
				testStepFailed("Product Title of the first product not present");
			}
		} catch (Exception e) {
			testStepFailed("Could not get the first product title");
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * Description: Method to update the cart quantity of the first product in the
	 * cart
	 */
	public void checkCartQuantity(int number) {
		try {
			if (isElementDisplayed(quantityUpdate)) {
				highLighterMethod(quantityUpdate);
				int quantity = Integer.parseInt(getAttributeValue(quantityUpdate, "data-item-qty"));
				if (quantity == number) {
					testStepInfo("The quantity expected is present");
				}
			} else
				testStepFailed("The box to update quantity of the product is not found");
		} catch (Exception e) {
			testStepFailed("The quantity expected is not present");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify presence of expected product price.
	 */
	public float verifyProductPriceInMiniCart(float PriceRequired) {
		try {
			if (isElementPresent(productPrices)) {
				List<WebElement> productPrices = findWebElements(MiniCart.productPrices);
				for (WebElement productPrice : productPrices) {
					String price = productPrice.getText().substring(1);
					if (PriceRequired == Float.parseFloat(price)) {
						highLighterMethod(productPrice);
						testStepInfo("The price value " + PriceRequired + "is present in the Mini Cart");
						return PriceRequired;
					}
				}
			} else {
				testStepFailed("No Product Price present in Mini Cart");
			}
		} catch (Exception e) {
			testStepFailed("Could not verify the presence of the expected price in the Mini Cart");
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * Description: Method to get verify expected card total.
	 */
	public void verifyCartSubTotalInMiniCart(float subtotal) {
		try {
			if (isElementPresent(cartSubtotal)) {
				double roundOff = Math.round(subtotal * 100.0) / 100.0;
				String subTotal = getText(cartSubtotal).substring(1);
				double subTotalRoundOff = Double.parseDouble(subTotal);
				if (roundOff == subTotalRoundOff) {
					highLighterMethod(cartSubtotal);
					testStepInfo("The expected subtotal present is present");
				} else {
					testStepFailed("The expected subtotal present is not present");
				}
			} else {
				testStepFailed("Cart Subtotal is not present");
			}
		} catch (Exception e) {
			testStepFailed("The expected subtotal present is not present");
			e.printStackTrace();
		}
	}

//	/**
//	 * Description: Method to verify expected message is displayed in the Alert
//	 */
//	public void verifyExpectedMessageAlert(String message1, String message2, String message3) {
//		try {
//			if (isElementDisplayed(alertMessage)) {
//				highLighterMethod(alertMessage);
//				scrollUp();
//				manualScreenshot("Alert message displayed");
//				String value = getText(alertMessage);
//				if (value.contains(message1) && value.contains(message2) && value.contains(message3))
//					testStepInfo("Relevant Alert Message was displayed");
//				else
//					testStepFailed("Relevant Alert Message was not displayed");
//
//			} else
//				testStepFailed("No Alert message was displayed");
//		} catch (Exception e) {
//			testStepFailed("Could not validate expected Messages");
//			e.printStackTrace();
//		}
//	}
//
//	/**
//	 * Description: Method to verify expected message is displayed in the Alert for
//	 * Invalid Coupon
//	 */
//	public void verifyExpectedMessageAlert_InvalidCoupon(String message1, String message2, String message3) {
//		try {
//			if (isElementDisplayed(alertMessage_invalidCoupon)) {
//				highLighterMethod(alertMessage_invalidCoupon);
//				manualScreenshot("Alert Message is displayed");
//				if (getText(alertMessage_invalidCoupon).contains(message1)
//						&& getText(alertMessage_invalidCoupon).contains(message2)
//						&& getText(alertMessage_invalidCoupon).contains(message3))
//					testStepInfo("Relevant Alert Message was displayed");
//				else
//					testStepFailed("Relevant Alert Message was not displayed");
//
//			} else
//				testStepFailed("No Alert message was displayed");
//		} catch (Exception e) {
//			testStepFailed("Could not validate expected messages");
//			e.printStackTrace();
//		}
//	}
//
//	/**
//	 * Description: Method to check the status of Update Cart - disabled or enabled
//	 */
//	public void checkStatusUpdateCart() {
//		try {
//			if (getAttributeValue(updateCart, "aria-disabled").contains("true")) {
//				testStepInfo("The Update Cart button is disabled");
//			} else
//				testStepInfo("The Update Cart button is not disabled");
//		} catch (Exception e) {
//			testStepInfo("The Cart button status could not fetched");
//			e.printStackTrace();
//		}
//	}
//
}
