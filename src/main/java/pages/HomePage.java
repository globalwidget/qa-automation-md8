package pages;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;

public class HomePage extends ApplicationKeywords {

//	private static final String closeOfferPopup = "Close Offer Pop up #xpath=//div[contains(@formtype,'POP')]/button[contains(@class,'close')]";
	private static final String email_Newsletter = "Newsletter Email #xpath=//form[contains(@class,'cid_2 ')]//input[@type='email']";
	private static final String emptyEmailAlert_Newsletter = "Newsletter Empty Email Alert #xpath=//span[contains(text(),'This field is required')]";
	private static final String invalidEmailAlert_Newsletter = "Invalid Email Alert Newsletter #xpath=//span[contains(text(),'Please enter a valid email address')]";
	private static final String subscribe_Newsletter = "Subscribe Newsletter #xpath=//form[contains(@class,'cid_2 ')]//button[contains(text(),'Subscribe')]";
	private static final String successMessage_Newsletter = "Success Message Newsletter #xpath=//div[contains(text(),'THANK YOU FOR JOINING OUR NEWSLETTER')]";
	private static final String homeFooterLink = "Home Footer Link #xpath=//footer//a[contains(text(),'HOME')]";
//	private static final String securityLogin_Password = "Password for Security Login #xpath=//input[@id='password_protected_pass']";
//	private static final String securityLogin_LoginButton = "Login Button for Security Login #xpath=//input[@id='wp-submit']";

	public HomePage(BaseClass obj) {
		super(obj);
	}

//	/**
//	 * Description: Method to handle the offer pop-up
//	 */
//	public void closeOfferPopup() {
//		try {
//			if (GOR.OfferPopUpHandled == false) {
//				scrollToViewElement(homeFooterLink);
//				waitTime(2);
//				if (isElementPresent(closeOfferPopup)) {
//					GOR.OfferPopUpHandled = true;
//					clickOn(closeOfferPopup);
//				} else {
//					testStepInfo("Offer pop up not present");
//				}
//			} else
//				testStepInfo("Offer Popup already handled");
//		} catch (Exception e) {
//			testStepInfo("Offer pop up not present");
//		}
//	}

	/**
	 * Description: Method to enter email for Newsletter Subscription
	 */
	public void enterEmailNewsletter(String email) {
		try {
			if (isElementDisplayed(email_Newsletter)) {
				highLighterMethod(email_Newsletter);
				typeIn(email_Newsletter, email);
			} else {
				testStepFailed("Could not enter email for Newsletter Subscription in HomePage", "Element not present");
			}
		} catch (Exception e) {
			testStepFailed("Could not enter email for Newsletter Subscription in HomePage");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify empty field error for email text box for
	 * Newsletter Subscription
	 */
	public void verifyEmptyEmailErrorNewsletter() {
		try {
			waitForElementToDisplay(emptyEmailAlert_Newsletter, 5);
			if (isElementDisplayed(emptyEmailAlert_Newsletter)) {
				highLighterMethod(emptyEmailAlert_Newsletter);
				testStepInfo("The Error for empty field was displayed");
			} else {
				testStepFailed("The Error for empty field was not displayed", "Element not present");
			}
		} catch (Exception e) {
			testStepFailed("The Error for empty field was not displayed");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify invalid email error for Newsletter Subscription
	 */
	public void verifyInvalidEmailErrorNewsletter() {
		try {
			if (isElementDisplayed(invalidEmailAlert_Newsletter)) {
				highLighterMethod(invalidEmailAlert_Newsletter);
				testStepInfo("The Error for invalid email was displayed");
			} else {
				testStepFailed("The Error for invalid email was not displayed", "Element not present");
			}
		} catch (Exception e) {
			testStepFailed("The Error for invalid email was not displayed");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Subscribe
	 */
	public void clickSubscribe() {
		try {
			if (isElementDisplayed(subscribe_Newsletter)) {
				highLighterMethod(subscribe_Newsletter);
				clickOn(subscribe_Newsletter);
			} else {
				testStepFailed("Could not click on Subscribe", "Element not present");
			}
		} catch (Exception e) {
			testStepFailed("Could not click on Subscribe");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify Success Message for Newsletter Subscription
	 */
	public void verifySuccessMessage_Newsletter() {
		try {
			if (isElementDisplayed(successMessage_Newsletter)) {
				highLighterMethod(successMessage_Newsletter);
				testStepInfo("The Success Message for Newsletter Subscription was displayed");
			} else {
				testStepFailed("The Success Message for Newsletter Subscription was not displayed",
						"Element not present");
			}
		} catch (Exception e) {
			testStepFailed("The Success Message for Newsletter Subscription was not displayed");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to scroll into view the Newsletter
	 */
	public void scrollToView_Newsletter() {
		try {
			if (isElementPresent(subscribe_Newsletter)) {
				scrollToViewElement(subscribe_Newsletter);

			} else {
				testStepFailed("The Subscribe Button was not displayed", "Element not present");
			}
		} catch (Exception e) {
			testStepFailed("Could not scroll to view Newsletter email box");
			e.printStackTrace();
		}
	}

//	/**
//	 * Description: Method to handle security Login
//	 */
//	public void securityLogin(String password) {
//		try {
//			if (isElementPresent(securityLogin_Password)) {
//				typeIn(securityLogin_Password, password);
//				clickOn(securityLogin_LoginButton);
//				GOR.securityLogin = true;
//
//			} else {
//				testStepFailed("Security Login not displayed", "Element not present");
//			}
//		} catch (Exception e) {
//			testStepFailed("Security Login could not be done");
//			e.printStackTrace();
//		}
//	}
}
