package pages;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;

public class LabTests extends ApplicationKeywords {

	private static final String enterBatchNumber = "BatchNumber searchBox #xpath=//input[@id='hb-lab-input']";
	private static final String searchIcon = "Search Icon #xpath=//button[@id='hb-lab-button']";
	private static final String nullResultImage = "Null Result Image #xpath=//div[@id='results-null-wrapper']//img";
	private static final String viewResults = "View Results #xpath=//div[contains(@id,'results')]//a[contains(text(),'Results')]";

	public LabTests(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method is used to open Lab Tests page.
	 * 
	 */
	public void goTo_LabTests() {
		try {
			navigateTo("https://mysticlabsd8.com/lab-testing/");
		} catch (Exception e) {
			testStepFailed("Could not open Lab Tests page");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method is used to open Lab Tests page.
	 * 
	 */
	public void enterBatchNumber(String batchNumber) {
		try {
			if (isElementDisplayed(enterBatchNumber)) {
				typeIn(enterBatchNumber, batchNumber);
			} else {
				testStepFailed("Could not enter batch number in the search box");
			}

		} catch (Exception e) {
			testStepFailed("Could not open Lab Tests page");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method is used to click search Icon.
	 * 
	 */
	public void clickSearchIcon() {
		try {
			if (isElementDisplayed(searchIcon)) {
				clickOn(searchIcon);
				waitTime(2);
				clickOn(searchIcon);
			} else {
				testStepFailed("Search Icon not displayed");
			}

		} catch (Exception e) {
			testStepFailed("Could not click on search Icon");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method is used to verify presence of null result image.
	 * 
	 */
	public void verifyNullResult() {
		try {
			waitForElementToDisplay(nullResultImage, 12);
			if (isElementDisplayed(nullResultImage)) {
				scrollToViewElement(nullResultImage);
				highLighterMethod(nullResultImage);
				testStepInfo("Relevant Message displayed for invalid batch Number");
			} else {
				testStepFailed("Relevant Message not displayed for invalid batch Number");
			}

		} catch (Exception e) {
			testStepFailed("Relevant Message not displayed for invalid batch Number");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method is used to check View Results.
	 * 
	 */
	public void checkViewResults() {
		try {
			waitForElementToDisplay(viewResults, 60);
			if (isElementDisplayed(viewResults)) {
				scrollToViewElement(viewResults);
				highLighterMethod(viewResults);
				testStepInfo("Files were displayed for the search done by the user");
			} else {
				testStepFailed("Could not click on the View Results of the first file");
			}

		} catch (Exception e) {
			testStepFailed("Files were not displayed for the search done by the user");
			e.printStackTrace();
		}

	}

}
