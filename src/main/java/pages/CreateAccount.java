package pages;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;

public class CreateAccount extends ApplicationKeywords {

	private static final String firstName = "First Name #id=firstname";
	private static final String lastName = "First Name #id=lastname";
	private static final String signUpForNewsletter_Checkbox = "Newsletter Sign Up checkbox #id=is_subscribed";
	private static final String emailAddress = "Email #id=email_address";
	private static final String password = "Password #id=password";
	private static final String confirmPassword = "Confirm Password #id=password-confirmation";
	private static final String createAnAccount = "Create Account Button #xpath=//span[contains(text(),'Create an Account')]/../../button";
	private static final String firstName_EmptyError = "First Name Missing Error #xpath=//div[@id='firstname-error' and contains(text(),'This is a required field.')]";
	private static final String lastName_EmptyError = "Last Name Missing Error #xpath=//div[@id='lastname-error' and contains(text(),'This is a required field.')]";
	private static final String emailAddress_EmptyError = "Email Address Missing Error #xpath=//div[@id='email_address-error' and contains(text(),'This is a required field.')]";
	private static final String passsword_EmptyError = "Password Missing Error #xpath=//div[@id='password-error' and contains(text(),'This is a required field.')]";
	private static final String confirmPassword_EmptyError = "Confirm Password Missing Error #xpath=//div[@id='password-confirmation-error' and contains(text(),'This is a required field.')]";
	private static final String emailAddress_InvalidError = "Email Address Invalid Error #xpath=//div[@id='email_address-error' and contains(text(),'Please enter a valid email address (Ex: johndoe@domain.com).')]";
	private static final String passsword_InvalidLengthError = "Password Invalid Length Error #xpath=//div[@id='password-error' and contains(text(),'Minimum length of this field must be equal or greater than 8 symbols')]";
	private static final String passsword_InvalidError_MinimumDifferentClasses = "Password Invalid Minimum Different Classes Error #xpath=//div[@id='password-error' and contains(text(),'Minimum of different classes of characters in password is 3.')]";
	private static final String confirmPassword_EnterSameValueError = "Confirm Password Enter Same Value Error #xpath=//div[@id='password-confirmation-error' and contains(text(),'Please enter the same value again.')]";
	private static final String accountExistsError = "Account already exists Error #xpath=//div[contains(text(),'There is already an account with this email address. If you are sure that it is your email address')]";
	private static final String captcha_EmptyError = "Confirm Password Missing Error #xpath=//div[@id='captcha_user_create-error' and contains(text(),'This is a required field.')]";
	private static final String registerSuccessMessage = "Register Success Message #xpath=//div[contains(text(),'Thank you for registering')]";

	// p[contains(text(),'You are subscribed to "General Subscription"')]

	public CreateAccount(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method is used to fill details for register
	 * 
	 */
	public void register_FillDetails(String firstName, String lastName, String email, String password,
			String confirmPassword) {
		try {
			if (isElementDisplayed(CreateAccount.firstName)) {

				if (firstName != null) {
					highLighterMethod(CreateAccount.firstName);
					clearEditBox(CreateAccount.firstName);
					typeIn(CreateAccount.firstName, firstName);
				}
				if (lastName != null) {
					highLighterMethod(CreateAccount.lastName);
					clearEditBox(CreateAccount.lastName);
					typeIn(CreateAccount.lastName, lastName);
				}
				if (email != null) {
					highLighterMethod(CreateAccount.emailAddress);
					clearEditBox(CreateAccount.emailAddress);
					typeIn(CreateAccount.emailAddress, email);
				}
				if (password != null) {
					highLighterMethod(CreateAccount.password);
					clearEditBox(CreateAccount.password);
					typeIn(CreateAccount.password, password);
				}
				if (confirmPassword != null) {
					highLighterMethod(CreateAccount.confirmPassword);
					clearEditBox(CreateAccount.confirmPassword);
					typeIn(CreateAccount.confirmPassword, confirmPassword);
				}
			} else {
				testStepFailed("Register Page not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not register successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to click on CreateAccount
	 * 
	 */
	public void clickCreateAccount() {
		try {
			if (isElementDisplayed(createAnAccount)) {
				highLighterMethod(createAnAccount);
				clickOn(createAnAccount);
			} else {
				testStepFailed("Create Account button not displayed");
			}
		} catch (Exception e) {
			testStepFailed("reate Account button could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to click on newsletter check box in create
	 * account page.
	 * 
	 */
	public void clickNewsletterCheckbox() {
		try {
			if (isElementDisplayed(signUpForNewsletter_Checkbox)) {
				highLighterMethod(signUpForNewsletter_Checkbox);
				clickOn(signUpForNewsletter_Checkbox);
			} else {
				testStepFailed("The Newsletter SignUp checkbox was not dispalyed");
			}
		} catch (Exception e) {
			testStepFailed("The Newsletter SignUp checkbox could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to check the presence of all the errors for empty fields
	 */
	public void VerifyEmptyFieldErrors() {
		try {
			{
				waitForElementToDisplay(firstName_EmptyError, 12);
				String[] emptyCardDetailsErrors = { firstName_EmptyError, lastName_EmptyError, emailAddress_EmptyError,
						passsword_EmptyError, confirmPassword_EmptyError, captcha_EmptyError };
				for (String error : emptyCardDetailsErrors) {
					if (isElementDisplayed(error)) {
						highLighterMethod(error);
					} else {
						testStepFailed(error.split("#")[0] + " error was not displayed");
					}
				}
			}

		} catch (Exception e) {
			testStepFailed("Validation of empty field errors was not successful");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify presence of Error Messages for Register
	 */
	public void verifyMessage_Register(String option) {
		try {
			switch (option) {
			case "emailInvalid": {
				if (isElementDisplayed(emailAddress_InvalidError)) {
					highLighterMethod(emailAddress_InvalidError);
					testStepInfo("Error Message for invalid Email was displayed");
				} else {
					testStepFailed("Error Message for invalid Email not displayed");
				}
				break;
			}

			case "passwordInvalidLength": {
				if (isElementDisplayed(passsword_InvalidLengthError)) {
					highLighterMethod(passsword_InvalidLengthError);
					testStepInfo("Error Message for invalid passsword length was displayed");
				} else {
					testStepFailed("Error Message for invalid passsword length not displayed");
				}
				break;
			}

			case "passsword_InvalidError_MinimumDifferentClasses": {
				if (isElementDisplayed(passsword_InvalidError_MinimumDifferentClasses)) {
					highLighterMethod(passsword_InvalidError_MinimumDifferentClasses);
					testStepInfo("Error Message for invalid passsword - Minimum different classes was displayed");
				} else {
					testStepFailed("Error Message for invalid passsword - Minimum different classes was not displayed");
				}
				break;
			}

			case "confirmPasswordInvalid": {
				if (isElementDisplayed(confirmPassword_EnterSameValueError)) {
					highLighterMethod(confirmPassword_EnterSameValueError);
					testStepInfo("Error Message for invalid confirm password value was displayed");
				} else {
					testStepFailed("Error Message for invalid confirm password value was not displayed");
				}
				break;
			}

			case "accountExistsError": {
				if (isElementDisplayed(accountExistsError)) {
					highLighterMethod(accountExistsError);
					testStepInfo("Error Message for pre-existing account was displayed");
				} else {
					testStepFailed("Error Message for pre-existing account was not displayed");
				}
				break;
			}

			case "successMessage": {
				if (isElementDisplayed(registerSuccessMessage)) {
					highLighterMethod(registerSuccessMessage);
					testStepInfo("Success Message for registration was displayed");
				} else {
					testStepFailed("Success Message for registration was not displayed");
				}
				break;
			}

			}
		} catch (Exception e) {
			testStepFailed("Could not verify the presence of the expected message");
			e.printStackTrace();
		}
	}
}
