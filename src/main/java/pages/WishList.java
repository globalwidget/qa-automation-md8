package pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;

public class WishList extends ApplicationKeywords {

	private static final String productsPresent = "Products Present #xpath=//div[contains(@class,'grid')]//strong[contains(@class,'product')]/a";
	private static final String deleteProduct = "Delete Product #xpath=//div[contains(@class,'product-item-info')]//div[contains(@class,'actions-secondary')]//a[contains(@class,'delete')]";
	private static final String addAllToCart = "Add All To Cart #xpath=//button/span[contains(text(),'All to Cart')]";
	private static final String wishListEmptyMessage = "Wish List Empty Message #xpath=//div/span[contains(text(),'no items in your wish list')]";

	public WishList(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method to verify presence of the expected product in the compare
	 * products page.
	 * 
	 */
	public void verifyPresenceOfExpectedProduct(String productName) {
		try {
			boolean productPresent = false;
			waitForElementToDisplay(productsPresent, 120);
//			scrollToViewElement(productsPresent);
			if (isElementDisplayed(productsPresent)) {
				manualScreenshot("The Product added is present in the Wish List page");
				List<WebElement> products = new ArrayList<WebElement>();
				products = findWebElements(productsPresent);
				for (WebElement product : products) {
					if (product.getText().toLowerCase().contains(productName.toLowerCase())) {
						productPresent = true;
						highLighterMethod(product);
						break;
					}
				}
				if (productPresent == true) {
					testStepInfo("The product added to cart " + productName + ", found in Wish List page");
				} else
					testStepFailed("Product added to cart " + productName + ", not found in Wish List page");
			} else
				testStepFailed("No products present in Wish List page");
		} catch (Exception e) {
			testStepFailed("Presence of expected product could not be verified");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to remove all products in Wish List page
	 */
	public void removeAllProducts() {
		List<WebElement> removeProducts;
		try {
			waitForElementToDisplay(productsPresent, 15);
			if (isElementDisplayed(productsPresent) == false) {
				testStepInfo("No Products in Wish List");
			} else {
				removeProducts = new ArrayList<WebElement>();
				removeProducts = findWebElements(deleteProduct);
				int totalProducts = removeProducts.size();
				if (totalProducts > 0) {
					for (int removeCounter = 0; removeCounter < totalProducts; removeCounter++) {
						removeProducts = new ArrayList<WebElement>();
						removeProducts = findWebElements(deleteProduct);
						for (WebElement removeTheProduct : removeProducts) {
							highLighterMethod(removeTheProduct);
							waitForElementToDisplay(deleteProduct, 40);
							scrollToViewElement(removeTheProduct);
							waitTime(3);
							removeTheProduct.click();
							waitTime(3);
							break;
						}
						if (removeCounter != totalProducts - 1) {
							removeProducts = new ArrayList<WebElement>();
							removeProducts = findWebElements(deleteProduct);
						}
					}
					testStepInfo("All Products Removed from Wish List");
				} else
					testStepInfo("No products to remove from wishList");
			}
		} catch (Exception e) {
			testStepFailed("All products removal from Wish List was not successful");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Add All To Cart.
	 */
	public void clickAddAllToCart() {
		try {
			waitForElementToDisplay(addAllToCart, 60);
			waitTime(3);
			scrollToViewElement(addAllToCart);
			if (isElementDisplayed(addAllToCart)) {
				highLighterMethod(addAllToCart);
				clickOn(addAllToCart);
			} else
				testStepFailed("Add All to Cart was not displayed");
		} catch (Exception e) {
			testStepFailed("Add All to Cart could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify Wish List is empty.
	 */
	public void verifyWishListEmptyMessage() {
		try {
			waitForElementToDisplay(wishListEmptyMessage, 120);
			if (isElementDisplayed(wishListEmptyMessage)) {
				highLighterMethod(wishListEmptyMessage);
				testStepInfo("The wish list is empty");
			} else
				testStepFailed("The wish list is not empty");
		} catch (Exception e) {
			testStepFailed("The empty wish list message was not displayed");
			e.printStackTrace();
		}
	}

}
