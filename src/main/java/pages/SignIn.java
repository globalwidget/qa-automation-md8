package pages;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;

public class SignIn extends ApplicationKeywords {

	private static final String email = "Email #id=email";
	private static final String password = "Password #id=pass";
	private static final String signInButton = "Sign In Button #xpath=//span[contains(text(),'Sign In')]/..";
	private static final String forgotPassword = "Forgot Password Link #xpath=//div[contains(@class,'login-container')]//span[contains(text(),'Forgot Your Password?')]";
	private static final String invalidEmailError = "Invalid Email Error #xpath=//div[@id='email-error' and contains(text(),'Please enter a valid email address (Ex: johndoe@domain.com).')]";
	private static final String emailEmptyError = "Email Missing Error #xpath=//div[@id='email-error' and contains(text(),'This is a required field.')]";
	private static final String passwordEmptyError = "Password Missing Error #xpath=//div[@id='pass-error' and contains(text(),'This is a required field.')]";
	private static final String invalidCredentialsError = "Invalid Credentials Error #xpath=//div[contains(text(),'The account sign-in was incorrect or your account ')]";
	private static final String wishListError = "Sign In Button #xpath=//div[contains(text(),'You must login or register to add items to your wishlist')]";

	public SignIn(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method is used to Sign In
	 * 
	 */
	public void signIn(String email, String password) {
		try {
			waitForElementToDisplay(SignIn.email, 120);
			scrollToViewElement(SignIn.email);
			if (isElementDisplayed(signInButton)) {
				highLighterMethod(SignIn.email);
				typeIn(SignIn.email, email);
				highLighterMethod(SignIn.password);
				typeIn(SignIn.password, password);
				highLighterMethod(signInButton);
				testStepInfo("Valid Credentials entered");
				scrollToViewElement(signInButton);
				clickOn(signInButton);
				GOR.loggedIn = true;
				testStepInfo("Sign In button clicked successfully");
			} else {
				testStepFailed("Sign In Page not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not Sign In successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify presence of Error Message for invalid Sign In
	 */
	public void verifyErrorMessage_SignIn(String option) {
		try {
			switch (option) {
			case "emailEmpty": {
				if (isElementDisplayed(emailEmptyError)) {
					highLighterMethod(emailEmptyError);
					testStepInfo("Error Message for empty Email field was displayed");
				} else {
					testStepFailed("Error Message for empty Email field not displayed");
				}
				break;
			}

			case "passwordEmpty": {
				if (isElementDisplayed(passwordEmptyError)) {
					highLighterMethod(passwordEmptyError);
					testStepInfo("Error Message for empty password field was displayed");
				} else {
					testStepFailed("Error Message for empty password field not displayed");
				}
				break;
			}

			case "emailInvalid": {
				if (isElementDisplayed(invalidEmailError)) {
					highLighterMethod(invalidEmailError);
					testStepInfo("Error Message for invalid Email was displayed");
				} else {
					testStepFailed("Error Message for invalid Email not displayed");
				}
				break;
			}

			case "invalidCredentialsError": {
				if (isElementDisplayed(invalidCredentialsError)) {
					highLighterMethod(invalidCredentialsError);
					testStepInfo("Error Message for invalid Credentials was displayed");
				} else {
					testStepFailed("Error Message for invalid Credentials not displayed");
				}
				break;
			}

			case "wishListError": {
				if (isElementDisplayed(wishListError)) {
					highLighterMethod(wishListError);
					testStepInfo("Error Message for adding product to wishlist as guest user was displayed");
				} else {
					testStepFailed("Error Message for adding product to wishlist as guest user was not displayed");
				}
				break;
			}
			}
		} catch (Exception e) {
			testStepFailed("Could not verify error message for Invalid Sign in Credentials successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to check if the user is logged Out.
	 */

	public void verifyPresenceOfSignIn() {

		try {
			if (isElementDisplayed(signInButton) == true) {
				testStepInfo("User is not logged In");
			} else {
				testStepFailed("Logout verification failed");
			}
		} catch (Exception e) {
			testStepFailed("Presence of Sign In button could not be verified");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to click on Forgot Password.
	 */

	public void clickForgotPassword() {

		try {
			if (isElementDisplayed(forgotPassword)) {
				clickOn(forgotPassword);
			} else {
				testStepFailed("Forgot Password not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not click on Forgot Password");
			e.printStackTrace();
		}
	}
}
