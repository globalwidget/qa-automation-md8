package pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;

public class ShoppingCart extends ApplicationKeywords {

	private static final String productsPresent = "Products Present #xpath=//strong[contains(@class,'product')]/a";
	private static final String removeProduct = "Remove Product #xpath=//span[contains(text(),'Remove item')]/..";
	private static final String quantityUpdateBox = "Quantity Update Box #xpath=//tbody//input[@type='number']";
	private static final String updateCart = "Update Cart button #xpath=//span[contains(text(),'Update Shopping Cart')]";
	private static final String continueShopping = "Continue Shopping button #xpath=//span[contains(text(),'Continue Shopping')]/..";
	private static final String proceedCheckout = "Proceed Checkout #xpath=//span[contains(text(),'Proceed to Checkout')]/..";
	private static final String shoppingCartHeader = "Shopping Cart Header #xpath=//h1/span[contains(text(),'Shopping Cart')]";
//	private static final String cartSubtotalElements_Shipping = "Cart Subtotal elements #xpath=//tr[@class='totals shipping excl']/following-sibling::tr";
	private static final String applyDiscountCode = "Apply Discount Code #xpath=//div[@id='block-discount']//strong[@id='block-discount-heading']";
	private static final String discountCodeTextBox = "Discount Code TextBox #xpath=//input[@id='coupon_code']";
	private static final String applyDiscount = "Apply Discount #xpath=//button[@value='Apply Discount']";
	private static final String cancelCoupon = "Cancel Coupon #xpath=//span[contains(text(),'Cancel Coupon')]";
	private static final String alertMessage = "Alert #xpath=//div[@role='alert']";
	private static final String couponCodeMissingError = "Coupon Code Missing Error #xpath=//div[@id='coupon_code-error']";
	private static final String invalidCouponError = "Invalid Coupon Error #xpath=//div[contains(text(),'not valid')]";
	private static final String couponAppliedMessage = "Coupon Applied Message #xpath=//div[contains(text(),'You used coupon code \"news10\"')]";
	private static final String couponCancelledMessage = "Coupon Cancelled Message #xpath=//div[contains(text(),'You canceled the coupon code')]";
	private static final String orderTotal = "Order Total #xpath=//td[@data-th='Order Total']//span[@class='price']";
	private static final String discountPrice = "Discount Price #xpath=//td[@data-th='Discount']//span[@class='price']";
	private static final String estimateShippingTaxHeader = "Estimate Shipping Tax Header #xpath=//div/div[@role='tablist' and @id='block-shipping']";
	private static final String shippingState = "Shipping State #xpath=//select[@name='region_id']";
	private static final String zipCodeShipping = "Shipping Zip Code #xpath=//input[@name='postcode']";
	private static final String tax = "Shipping Tax #xpath=//div[@id='cart-totals']//tr[@class='totals-tax']/td/span";
	private static final String standardShipping = "Shipping - Standard #xpath=//div/input[contains(@id,'s_method_amstrates_amstrates2')]";
	private static final String freeShipping_standard = "Shipping - Free Standard #xpath=//div/input[contains(@id,'s_method_amstrates_amstrates5')]";
	private static final String freeShipping_expedited = "Shipping - Free Expedited #xpath=//div/input[contains(@id,'s_method_freeshipping_freeshipping')]";
	private static final String overnightShipping = "Shipping - Overnight #xpath=//div/input[contains(@value,'custom')]";
	private static final String shippingPrice = "Shipping Price #xpath=//tr[@class='totals shipping excl']/td/span";
	private static final String emptyCartVerify = "Empty Cart Verification #xpath=//div[contains(@class,'column main')]//div[2]";

	public ShoppingCart(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method to verify presence of the expected product in the cart
	 * 
	 */
	public void verifyPresenceOfExpectedProduct(String productName) {
		try {
			boolean productPresent = false;
			if (isElementPresent(productsPresent)) {
				manualScreenshot("The Product added is present in the cart");
				List<WebElement> products = new ArrayList<WebElement>();
				products = findWebElements(productsPresent);
				for (WebElement product : products) {
					if (product.getText().toLowerCase().contains(productName.toLowerCase())) {
						productPresent = true;
						highLighterMethod(product);
						break;
					}
				}
				if (productPresent == true) {
					testStepInfo("The product added to cart " + productName + ", found in My Cart page");
				} else
					testStepFailed("Product added to cart " + productName + ", not found in My Cart page");
			} else
				testStepFailed("No products present in My cart page");
		} catch (Exception e) {
			testStepFailed("Presence of expected product could not be verified");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on delete button in My Cart page
	 */
	public String clickOnRemoveProduct() {
		String productName = "";
		try {
			waitForElementToDisplay(removeProduct, 30);
			if (isElementDisplayed(removeProduct)) {
				highLighterMethod(removeProduct);
				productName = findWebElements(productsPresent).get(0).getText();
				findWebElements(removeProduct).get(0).click();
				GOR.productAdded = false;
				testStepInfo("The remove button was clicked for the product " + productName);
				return productName;
			} else
				testStepFailed("Could not find any product in My Cart");
		} catch (Exception e) {
			testStepFailed("Could not remove products from cart");
			e.printStackTrace();
		}
		return productName;
	}

	/**
	 * Description: Method to remove all products in My Cart page
	 */
	public void removeAllProducts() {
		try {
			List<WebElement> removeProducts;
			int counter = 0;
			if (getAttributeValue(emptyCartVerify, "class").contains("empty")) {
				testStepInfo("No Products in Cart");
			} else {

				removeProducts = new ArrayList<WebElement>();
				removeProducts = findWebElements(removeProduct);
				int totalProducts = removeProducts.size();
				GOR.productAdded = false;
				for (int removeCounter = 0; removeCounter < totalProducts; removeCounter++) {
					removeProducts = new ArrayList<WebElement>();
					removeProducts = findWebElements(removeProduct);
					for (WebElement removeTheProduct : removeProducts) {
						highLighterMethod(removeTheProduct);
						waitForElementToDisplay(removeProduct, 120);
						waitTime(2);
						removeTheProduct.click();
						++counter;
						waitTime(6);
						break;
					}
					if (counter == totalProducts) {
						break;
					}
				}
				testStepInfo("All Products Removed");
			}
		} catch (

		Exception e) {
			testStepFailed("All products removal from cart was not successful");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify expected message is displayed in the Alert
	 */
	public void verifyExpectedMessageAlert(String message1, String message2, String message3) {
		try {
			if (isElementDisplayed(alertMessage)) {
				highLighterMethod(alertMessage);
				scrollUp();
				manualScreenshot("Alert message displayed");
				String value = getText(alertMessage);
				if (value.contains(message1) && value.contains(message2) && value.contains(message3))
					testStepInfo("Relevant Alert Message was displayed");
				else
					testStepFailed("Relevant Alert Message was not displayed");

			} else
				testStepFailed("No Alert message was displayed");
		} catch (Exception e) {
			testStepFailed("Could not validate expected Messages");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to update the cart quantity of the first product in the
	 * cart
	 */
	public void updateCartQuantity(int quantity) {
		try {
			if (isElementDisplayed(quantityUpdateBox)) {
				highLighterMethod(quantityUpdateBox);
				findWebElements(quantityUpdateBox).get(0).clear();
				findWebElements(quantityUpdateBox).get(0).sendKeys(String.valueOf(quantity));
			} else
				testStepFailed("The box to update quantity of the product is not found");
		} catch (Exception e) {
			testStepFailed("Could not update quantity of the product");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to update the cart quantity of the first product in the
	 * cart
	 */
	public void checkCartQuantity(int number) {
		try {
			if (isElementDisplayed(quantityUpdateBox)) {
				highLighterMethod(quantityUpdateBox);
				int quantity = Integer.parseInt(getAttributeValue(quantityUpdateBox, "value"));
				if (quantity == number) {
					testStepInfo("The quantity expected is present");
				}
			} else
				testStepFailed("The box to update quantity of the product is not found");
		} catch (Exception e) {
			testStepFailed("The quantity expected is not present");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Update Cart
	 */
	public void clickUpdateCart() {
		try {
			if (isElementDisplayed(updateCart)) {
				highLighterMethod(updateCart);
				clickOn(updateCart);
				waitTime(3);
				waitForElementToDisplay(updateCart, 60);
			} else
				testStepFailed("The Update Cart button is not displayed");
		} catch (Exception e) {
			testStepFailed("The Update Cart button could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Proceed to Checkout
	 */
	public void clickProceedToCheckOut() {
		try {
			waitForElementToDisplay(proceedCheckout, 60);
			waitTime(3);
			if (isElementDisplayed(proceedCheckout)) {
				highLighterMethod(proceedCheckout);
				clickOn(proceedCheckout);
			} else
				testStepFailed("Proceed to Checkout was not displayed");
		} catch (Exception e) {
			testStepFailed("Proceed to checkout could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to get the product quantity
	 */
	public int getProductQuantity() {
		try {
			if (isElementDisplayed(quantityUpdateBox)) {
				highLighterMethod(quantityUpdateBox);
				int quantity = Integer.parseInt(getAttributeValue(quantityUpdateBox, "value"));
				return quantity;
			} else
				testStepFailed("Could not get the quantity of the product", "Quantity update box is not displayed");
		} catch (Exception e) {
			testStepFailed("Could not get the quantity of the product");
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * Description: Method to verify Shopping Cart page navigation.
	 */
	public void verifyShoppingCartHeader() {
		try {
			if (isElementDisplayed(shoppingCartHeader)) {
				highLighterMethod(shoppingCartHeader);
				testStepInfo("Shopping Cart page opened successfully");
			} else {
				testStepFailed("Shopping Cart page was not opened successfully",
						"Shopping Cart Header was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Shopping Cart page was not opened successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Continue Shopping button
	 */
	public void clickContinueShoppingCart() {
		try {
			if (isElementDisplayed(continueShopping)) {
				highLighterMethod(continueShopping);
				clickOn(continueShopping);
				waitTime(3);
				waitForElementToDisplay(continueShopping, 10);
			} else
				testStepFailed("The Continue Shopping button is not displayed");
		} catch (Exception e) {
			testStepFailed("The Continue Shopping button could not be clicked");
			e.printStackTrace();
		}
	}

//	/**
//	 * Description: Method to check presence or absence of tax in the sub-total
//	 */
//	public void checkTax(boolean presence) {
//		try {
//			boolean taxPresent = false;
//			List<WebElement> cartSutotal = new ArrayList<WebElement>();
//			cartSutotal = findWebElements(cartSubtotalElements_Shipping);
//			for (WebElement element : cartSutotal) {
//				if (element.getAttribute("class").contains("totals-tax"))
//					taxPresent = true;
//			}
//			if (presence == true) {
//				if (taxPresent == true) {
//					highLighterMethod(cartSubtotalElements_Shipping);
//					testStepInfo("Tax was present in the cart subtotal as expected");
//				} else {
//					testStepFailed("Tax was not present in the cart subtotal as expected");
//				}
//			} else if (presence == false) {
//				if (taxPresent == false) {
//					highLighterMethod(cartSubtotalElements_Shipping);
//					testStepInfo("Tax was not present in the cart subtotal as expected");
//				} else {
//					testStepFailed("Tax was present in the cart subtotal");
//				}
//			}
//		} catch (Exception e) {
//			testStepFailed("Presence or Absence of tax could not be checked");
//			e.printStackTrace();
//		}
//	}

	/**
	 * Description: Method to click on the Apply Discount Code Header.
	 */
	public void clickApplyDiscountCodeHeader() {
		try {
			waitForElementToDisplay(applyDiscountCode, 120);
			scrollToViewElement(applyDiscountCode);
			if (isElementDisplayed(applyDiscountCode)) {
				highLighterMethod(applyDiscountCode);
				clickOn(applyDiscountCode);
			} else {
				testStepFailed("Apply Discount Code Header was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not click on Apply Discount Code Header");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to enter discount code.
	 */
	public void enterDiscountCode(String discountCode) {
		try {
			waitForElementToDisplay(discountCodeTextBox, 50);
			if (isElementDisplayed(discountCodeTextBox)) {
				highLighterMethod(discountCodeTextBox);
				typeIn(discountCodeTextBox, discountCode);
			} else {
				testStepFailed("Discount Code TextBox was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not enter Discount Code");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on apply discount.
	 */
	public void clickApplyDiscount() {
		try {
			if (isElementDisplayed(applyDiscount)) {
				highLighterMethod(applyDiscount);
				clickOn(applyDiscount);
			} else {
				testStepFailed("Apply Discount button was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Apply Discount button could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on cancel for discount coupon.
	 */
	public void clickCancel_DiscountCoupon() {
		try {
			if (isElementDisplayed(cancelCoupon)) {
				highLighterMethod(cancelCoupon);
				clickOn(cancelCoupon);
			} else {
				testStepFailed("Cancel button was not displayed for Discount Coupon");
			}
		} catch (Exception e) {
			testStepFailed("Cancel button could not be clicked for Discount Coupon");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify the display of relevant message.
	 */
	public void verifyRelevantMessage(String error) {
		try {
			switch (error) {
			case "Coupon Code Missing": {
				waitForElement(couponCodeMissingError, 6);
				if (isElementDisplayed(couponCodeMissingError)) {
					testStepInfo("Coupon Code Missing Error was displayed.");
					highLighterMethod(couponCodeMissingError);
				} else {
					testStepFailed("Coupon Code Missing Error was not displayed.");
				}
				break;
			}

			case "Invalid Coupon": {
				waitForElement(invalidCouponError, 6);
				if (isElementDisplayed(invalidCouponError)) {
					highLighterMethod(invalidCouponError);
					testStepInfo("Invalid Coupon Error was displayed.");
				} else {
					testStepFailed("Invalid Coupon Error was not displayed.");
				}
				break;
			}

			case "Coupon Applied": {
				waitForElement(couponAppliedMessage, 6);
				if (isElementDisplayed(couponAppliedMessage)) {
					highLighterMethod(couponAppliedMessage);
					testStepInfo("Coupon Applied Successfully message for coupon code - 'web15' was displayed.");
				} else {
					testStepFailed("Invalid Coupon Error was not displayed.");
				}
				break;
			}

			case "Coupon Cancelled": {
				waitForElement(couponCancelledMessage, 6);
				if (isElementDisplayed(couponCancelledMessage)) {
					highLighterMethod(couponCancelledMessage);
					testStepInfo("Coupon Cancelled Message was displayed.");
				} else {
					testStepFailed("Coupon Cancelled Message was not displayed.");
				}
				break;
			}
			}
		} catch (Exception e) {
			testStepFailed("Relevant Error Meesage verification could not be done");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to check Order Total.
	 */
	public void checkOrderTotal(float price_1, float price_2, float price_3) {
		float orderTotal_data;
		try {
			scrollToViewElement(orderTotal);
			waitForElement(orderTotal, 400);
			if (isElementDisplayed(orderTotal)) {
				orderTotal_data = Float.parseFloat(getText(orderTotal).substring(1));
				float sum = (float) (Math.round((price_1 + price_2 + price_3) * 100) / 100.0);
				if (orderTotal_data == sum) {
					highLighterMethod(orderTotal);
					testStepInfo("The expected order total is present");
				} else {
					testStepFailed("The expected order total is not present");
				}
			} else
				testStepFailed("Order Total is not displayed");
		} catch (Exception e) {
			testStepFailed("Order Total could not be checked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to get the discount price.
	 */
	public float getDiscountPrice() {
		float discountPrice_data = 0;
		try {
			waitForElementToDisplay(discountPrice, 60);
			scrollToViewElement(discountPrice);
			if (isElementDisplayed(discountPrice)) {
				highLighterMethod(discountPrice);
				discountPrice_data = Float.parseFloat(getText(ShoppingCart.discountPrice).substring(2));
				return discountPrice_data;
			} else
				testStepFailed("Discount Price is not displayed");
		} catch (Exception e) {
			testStepFailed("Discount Price could not be fetched");
			e.printStackTrace();
		}
		return discountPrice_data;
	}

	/**
	 * Description: Method to get the tax price.
	 */
	public float getTaxPrice() {
		float tax_data = 0;
		try {
			waitForElementToDisplay(tax, 120);
			scrollToViewElement(tax);
			if (isElementDisplayed(tax)) {
				highLighterMethod(tax);
				tax_data = Float.parseFloat(getText(ShoppingCart.tax).substring(1));
				return tax_data;
			} else
				testStepFailed("Tax is not displayed");
		} catch (Exception e) {
			testStepFailed("Tax could not be fetched");
			e.printStackTrace();
		}
		return tax_data;
	}

	/**
	 * Description: Method to get the shipping tax price.
	 */
	public float getShippingPrice() {
		float shippingPrice_data = 0;
		try {
			waitForElementToDisplay(shippingPrice, 60);
			scrollToViewElement(shippingPrice);
			if (isElementDisplayed(shippingPrice)) {
				highLighterMethod(shippingPrice);
				shippingPrice_data = Float.parseFloat(getText(ShoppingCart.shippingPrice).substring(1));
				return shippingPrice_data;
			} else
				testStepFailed("Shipping Price is not displayed");
		} catch (Exception e) {
			testStepFailed("Shipping Price could not be fetched");
			e.printStackTrace();
		}
		return shippingPrice_data;
	}

	/**
	 * Description: Method to click on 'Estimate Shipping Header', if it is not in
	 * collapsed condition.
	 */
	public void collapseEstimateShippingHeader() {
		try {
			waitForElementToDisplay(estimateShippingTaxHeader, 120);
			waitTime(3);
			if (isElementDisplayed(estimateShippingTaxHeader)) {
				if (!(getAttributeValue(estimateShippingTaxHeader, "class").contains("active"))) {
					highLighterMethod(estimateShippingTaxHeader);
					clickOn(estimateShippingTaxHeader);
					waitForElementToDisplay(shippingState, 100);
				}
			} else
				testStepFailed("'Estimate Shipping Header' displayed");
		} catch (Exception e) {
			testStepFailed("'Estimate Shipping Header' could not clicked to collapse");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to fill Shipping Details in cart page.
	 */
	public void fillShippingDetails(String state, String zipCode) {
		try {
			waitForElementToDisplay(shippingState, 100);
			if (isElementDisplayed(shippingState)) {
				if (isElementDisplayed(shippingState)) {
					highLighterMethod(shippingState);
					selectFromDropdown(shippingState, state);
				} else {
					testStepFailed("Shipping State was not displayed");
				}
				scrollToViewElement(shippingState);
				if (isElementDisplayed(zipCodeShipping)) {
					highLighterMethod(zipCodeShipping);
					typeIn(zipCodeShipping, zipCode);
				} else {
					testStepFailed("Zip Code was not displayed");
				}
			} else
				testStepFailed("Shipping State was not displayed");
		} catch (Exception e) {
			testStepFailed("'Estimate Shipping Header' could not clicked to collapse");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on desired shipping option.
	 */
	public void clickDesiredShippingOption(String option) {
		try {
			switch (option) {
			case "FreeStandard": {
				if (isElementDisplayed(freeShipping_standard)) {
					highLighterMethod(freeShipping_standard);
					clickOn(freeShipping_standard);
				} else {
					testStepFailed("Standard Free Shipping option was not displayed");
				}
				break;
			}

			case "FreeExpedited": {
				if (isElementDisplayed(freeShipping_expedited)) {
					highLighterMethod(freeShipping_expedited);
					clickOn(freeShipping_expedited);
				} else {
					testStepFailed("Expedited Free Shipping option was not displayed");
				}
				break;
			}

			case "Standard": {
				if (isElementDisplayed(standardShipping)) {
					highLighterMethod(standardShipping);
					clickOn(standardShipping);
				} else {
					testStepFailed("Standard Shipping option was not displayed");
				}
				break;
			}
			case "Overnight": {
				if (isElementDisplayed(overnightShipping)) {
					highLighterMethod(overnightShipping);
					clickOn(overnightShipping);
				} else {
					testStepFailed("Overnight Shipping option was not displayed");
				}
				break;
			}
			}
		} catch (Exception e) {
			testStepFailed("Desired Shipping option could not be clicked");
			e.printStackTrace();
		}
	}
}
