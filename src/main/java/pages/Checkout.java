package pages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebElement;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;

public class Checkout extends ApplicationKeywords {

	private static final String email_Shipping = "Email Shipping #xpath=//li[@id='shipping']//input[@id='customer-email']";
	private static final String firstName_Shipping = "First Name Shipping #xpath=//input[@name='firstname']";
	private static final String lastName_Shipping = "Last Name Shipping #xpath=//input[@name='lastname']";
	private static final String company_Shipping = "Company Shipping #xpath=//input[@name='company']";
	private static final String streetAddressLineOne_Shipping = "Street Address Line One Shipping #xpath=//input[@name='street[0]']";
	private static final String streetAddressLineTwo_Shipping = "Street Address Line Two Shipping #xpath=//input[@name='street[1]']";
	private static final String streetAddressLineThree_Shipping = "Street Address Line Three Shipping #xpath=//input[@name='street[2]']";
	private static final String city_Shipping = "City Shipping #xpath=//input[@name='city']";
	private static final String state_Shipping = "State Shipping #xpath=//select[@name='region_id']";
	private static final String zip_Shipping = "Zip Shipping #xpath=//input[@name='postcode']";
	private static final String country_Shipping = "Country Shipping #xpath=//select[@name='country_id']";
	private static final String phoneNumber_Shipping = "Phone Number Shipping #xpath=//input[@name='telephone']";
//	private static final String suggestedAddress = "Suggested Address #xpath=//li[@id='address-validation']";
	private static final String next = "Next #xpath=//span[contains(text(),'Next')]";

	private static final String error_EmailMissing = "Email Missing Error #xpath=//div[@id='customer-email-error' and contains(text(),'This is a required field.')]";
	private static final String error_FirstNameMissing = "Shipping First Name Missing Error #xpath=//input[@name='firstname']/following-sibling::div[@class='field-error']";
	private static final String error_LastNameMissing = "Shipping Last Name Missing Error #xpath=//input[@name='lastname']/following-sibling::div[@class='field-error']";
	private static final String error_StreetAddressMissing = "Shipping Street Address Missing Error #xpath=//input[@name='street[0]']/following-sibling::div[@class='field-error']";
	private static final String error_CityMissing = "Shipping City Missing Error #xpath=//input[@name='city']/following-sibling::div[@class='field-error']";
	private static final String error_StateMissing = "Shipping State Missing Error #xpath=//select[@name='region_id']/following-sibling::div[@class='field-error']";
	private static final String error_PostCodeMissing = "Shipping Post Code Missing Error #xpath=//input[@name='postcode']/following-sibling::div[@class='field-error']";
	private static final String error_PhoneNumberMissing = "Shipping Phone Number Code Missing Error #xpath=//input[@name='telephone']/following-sibling::div[@class='field-error']";
	private static final String error_ShippingMethodMissing = "Shipping Method Missing Error #xpath=//span[contains(text(),'shipping method is missing')]";

	private static final String error_InvalidEmailAddress = "Invalid Email Error #xpath=//div[contains(text(),'Please enter a valid email address')]";
	private static final String warning_InvaildZipCode = "Invalid Zip Code Error #xpath=//span[contains(text(),'Provided Zip/Postal Code seems to be invalid. Exam')]";

//	private static final String paymentMethod_CreditCard = "Credit Card #xpath=//input[@id='authnetcim']";
	private static final String applyDiscountCode = "Apply Discount Code - Header #xpath=//div[contains(@class,'payment')]//span[@id='block-discount-heading']/span";
	private static final String discountCodeTextBox = "Discount Code TextBox #xpath=//input[@id='discount-code']";
	private static final String applyDiscount = "Apply Discount #xpath=//button[@value='Apply Discount']";
	private static final String discountCodeMissingError = "Discount Code Missing Error #xpath=//div[@id='discount-code-error']";
	private static final String invalidCouponError = "Invalid Coupon Error #xpath=//div[contains(text(),\"The coupon code isn't valid. Verify the code\")]";
	private static final String cancelCoupon = "Cancel Coupon #xpath=//span[contains(text(),'Cancel coupon')]";

	private static final String billingAddress = "Auto-filled Billing Address #xpath=//div[@class='billing-address-details']";
	private static final String sameBillingShippingAddress = "Same Billing and Shipping Addrerss checkbox #xpath=//input[@id='billing-address-same-as-shipping-authnetcim']";
	private static final String firstName_Billing = "First Name Billing #xpath=//div[@class='billing-address-form']//input[@name='firstname']";
	private static final String lastName_Billing = "Last Name Billing #xpath=//div[@class='billing-address-form']//input[@name='lastname']";
	private static final String company_Billing = "Company Billing #xpath=//div[@class='billing-address-form']//input[@name='company']";
	private static final String streetAddressLineOne_Billing = "Street Address Line One Billing #xpath=//div[@class='billing-address-form']//input[@name='street[0]']";
	private static final String streetAddressLineTwo_Billing = "Street Address Line Two Billing #xpath=//div[@class='billing-address-form']//input[@name='street[1]']";
	private static final String streetAddressLineThree_Billing = "Street Address Line Three Billing #xpath=//div[@class='billing-address-form']//input[@name='street[2]']";
	private static final String city_Billing = "City Billing #xpath=//div[@class='billing-address-form']//input[@name='city']";
	private static final String state_Billing = "State Billing #xpath=//div[@class='billing-address-form']//select[@name='region_id']";
	private static final String zip_Billing = "Zip Billing #xpath=//div[@class='billing-address-form']//input[@name='postcode']";
	private static final String country_Billing = "Country Billing #xpath=//div[@class='billing-address-form']//select[@name='country_id']";
	private static final String phoneNumber_Billing = "Phone Number Billing #xpath=//div[@class='billing-address-form']//input[@name='telephone']";
	private static final String placeOrder = " Place Order #xpath=//button[@title='Place Order']";
	private static final String termsCheckbox = "Terms Checkbox #id=agreement_authnetcim_9";
	private static final String update = " Update #xpath=//span[contains(text(),'Update')]";
//	private static final String cancel = " Cancel #xpath=//span[contains(text(),'Cancel')]";

	private static final String cardNumber = "Card Number #id=authnetcim-cc-number";
	private static final String expiration_Month = "Expiration Month #id=authnetcim-cc-exp-month";
	private static final String expiration_Year = "Expiration Year #id=authnetcim-cc-exp-year";
	private static final String cardVerificationNumber = "Card Verification Number #id=authnetcim-cc-cid";

	private static final String successCheckout = "Checkout Success Message #xpath=//span[contains(text(),'Thank you for your purchase!')]";
//	private static final String error_cardNumberMissing = "Empty Card Number Field Error Message #xpath=//div[@id='anet_creditcard_cc_number-error' and contains(text(),'Please enter a valid number in this field')]";
	private static final String error_cardNumberInvalid = "Card Number Invalid Message #xpath=//div[@id='authnetcim-cc-number-error' and contains(text(),'Please enter a valid credit card number')]";
	private static final String error_cardTypeError = "Card Number Number Does not match type Message #xpath=//div[@id='authnetcim-cc-number-error' and contains(text(),'does not match credit card type')]";
	private static final String error_expirationDateInvalid = "Expiration Date - Month Invalid Message #id=authnetcim-cc-exp-month-error";
	private static final String error_termsUnchecked = "Terms Unchecked Error Message #id=agreement[1]-error";
//	private static final String error_expirationDate_MonthInvalid = "Expiration Date - Month Invalid Message #id=anet_creditcard_expiration-error";
//	private static final String error_expirationDate_YearInvalid = "Expiration Date - Year Invalid Message #id=anet_creditcard_expiration_yr-error";
//	private static final String error_VerificationNumberMissing = "Security Code Missing Message #xpath=//div[@id='anet_creditcard_cc_cid-error' and contains(text(),'Please enter a valid number in this field')]";
	private static final String error_VerificationNumberInvalid = "Security Code Invalid Message #xpath=//div[@id='authnetcim-cc-cid-error' and contains(text(),'Please enter a valid credit card verification number')]";

	private static final String shippingAddressHeader = "Shipping Address Header #xpath=//div[contains(text(),'Shipping Address')]";
	private static final String productsPresent = "Products Present #xpath=//strong[contains(@class,'product')]";
	private static final String shippingProgressBar = "Shipping Progress Bar #xpath=//li[contains(@class,'progress-bar')]/span[contains(text(),'Shipping')]";
	private static final String itemsInCartHeader = "Items In Cart Header #xpath=//div[contains(@class,'items-in-cart')]/div[1]";
	private static final String shippingPrice = "Shipping Price #xpath=//tr[@class='totals shipping excl']/td/span";
	private static final String standardShipping_vape = "Shipping - Standard - Vape Products #xpath=//input[contains(@id,'s_method_amstrates2')]";
	private static final String freeStandardShipping_vape = "Shipping - Free Standard  - Vape Products #xpath=//input[contains(@id,'s_method_amstrates5')]";
	private static final String standardShipping_withoutVape = "Shipping - Standard - Without Vape Products #xpath=//input[contains(@id,'s_method_amstrates_amstrates2')]";
	private static final String upsOvernightShipping = "UPS Overnight Shipping #xpath=//input[contains(@id,'s_method_customshipping_customshipping')]";
	private static final String freeStandardShipping_withoutVape = "Shipping - Free Standard  - Without Vape Products #xpath=//input[contains(@id,'s_method_amstrates_amstrates5')]";
	private static final String freeExpeditedShipping_withoutVape = "Shipping - Free Expedited  - Without Vape Products #xpath=//input[contains(@id,'s_method_freeshipping_freeshipping')]";
	private static final String orderTotal = "Order Total #xpath=//td[@data-th='Order Total']//span[@class='price']";
	private static final String password = "Password #xpath=//input[@id='customer-password']";
	private static final String login = "Login #xpath=//span[contains(text(),'Login')]/..";
	private static final String savedAddressBlock = "Saved Address Block #xpath=//div[@class='shipping-address-items']/div";
	private static final String savedAddressBlock_New = "Saved Address Block #xpath=//div[@class='shipping-address-items']/div[2]";
	private static final String newAddressButton = "New Address Button #xpath=//span[contains(text(),'New Address')]/..";
	private static final String shipHere = "Ship Here Button #xpath=//span[contains(text(),'Ship Here')]/..";
	private static final String saveAddressCheckboxShipping = "Save in Address Book checkbox - Shipping #xpath=//input[@id='shipping-save-in-address-book']";
	private static final String saveAddressCheckboxBilling = "Save in Address Book checkbox - Billing #xpath=//input[@id='billing-save-in-address-book-authnetcim']";
	private static final String billingAddressDropdown = "Billing Address Dropdown #xpath=//select[@name='billing_address_id']";
	private static final String orderSummaryExpand = "Expand Order Summary #xpath=//div[contains(@class,'items-in-cart')]/div[1]";

	public Checkout(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method to fill the details required for Shipping
	 */
	public void fillDetailsShipping(HashMap<String, String> shippingData) {
		try {
			waitForElementToDisplay(firstName_Shipping, 180);
			if (isElementDisplayed(firstName_Shipping)) {
				Map<String, String> data_Details_Shipping = new LinkedHashMap<String, String>(12);
				data_Details_Shipping.put(email_Shipping, shippingData.get("email_data"));
				data_Details_Shipping.put(firstName_Shipping, shippingData.get("firstName_data"));
				data_Details_Shipping.put(lastName_Shipping, shippingData.get("lastName_data"));
				data_Details_Shipping.put(company_Shipping, shippingData.get("company_data"));
				data_Details_Shipping.put(country_Shipping, shippingData.get("billingCountry_data"));
				data_Details_Shipping.put(streetAddressLineOne_Shipping,
						shippingData.get("streetAddressFirstLine_data"));
				data_Details_Shipping.put(streetAddressLineTwo_Shipping,
						shippingData.get("streetAddressSecondLine_data"));
				data_Details_Shipping.put(streetAddressLineThree_Shipping,
						shippingData.get("streetAddressThirdLine_data"));
				data_Details_Shipping.put(city_Shipping, shippingData.get("city_data"));
				data_Details_Shipping.put(state_Shipping, shippingData.get("state_data"));
				data_Details_Shipping.put(zip_Shipping, shippingData.get("postCode_data"));
				data_Details_Shipping.put(phoneNumber_Shipping, shippingData.get("phoneNumber_data"));
				for (Map.Entry<String, String> entry : data_Details_Shipping.entrySet()) {
					if (entry.getValue() == null || entry.getValue().isEmpty())
						continue;
					if (isElementPresent(entry.getKey())) {
						if (isElementDisplayed(entry.getKey())) {
							if (entry.getKey().contains("Country") || entry.getKey().contains("State")) {
								{
									highLighterMethod(entry.getKey());
									selectFromDropdown(entry.getKey(), entry.getValue());
								}
							} else {
								highLighterMethod(entry.getKey());
								clickOn(entry.getKey());
								clearEditBox(entry.getKey());
								typeIn(entry.getKey(), entry.getValue());
							}

						} else {
							testStepFailed(entry.getKey().split("#")[0] + " field was not found");
						}
					}
				}
			} else {
				testStepFailed("First Name field was not displayed");
			}
		} catch (Exception e) {
			testStepFailedDB("Could not fill the shipping details successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to fill the details required for billing
	 */
	public void fillDetailsBilling(HashMap<String, String> billingData) {
		try {
			waitForElementToDisplay(firstName_Billing, 1000);
			if (isElementDisplayed(firstName_Billing)) {
				Map<String, String> data_Details_Billing = new LinkedHashMap<String, String>(11);
				data_Details_Billing.put(firstName_Billing, billingData.get("firstName_data"));
				data_Details_Billing.put(lastName_Billing, billingData.get("lastName_data"));
				data_Details_Billing.put(company_Billing, billingData.get("company_data"));
				data_Details_Billing.put(country_Billing, billingData.get("billingCountry_data"));
				data_Details_Billing.put(streetAddressLineOne_Billing, billingData.get("streetAddressFirstLine_data"));
				data_Details_Billing.put(streetAddressLineTwo_Billing, billingData.get("streetAddressSecondLine_data"));
				data_Details_Billing.put(streetAddressLineThree_Billing,
						billingData.get("streetAddressThirdLine_data"));
				data_Details_Billing.put(city_Billing, billingData.get("city_data"));
				data_Details_Billing.put(state_Billing, billingData.get("state_data"));
				data_Details_Billing.put(zip_Billing, billingData.get("postCode_data"));
				data_Details_Billing.put(phoneNumber_Billing, billingData.get("phoneNumber_data"));
				for (Map.Entry<String, String> entry : data_Details_Billing.entrySet()) {
					if (isElementPresent(entry.getKey())) {
						if (isElementDisplayed(entry.getKey())) {
							if (entry.getValue() == null || entry.getValue().isEmpty())
								continue;
							else {
								if (entry.getKey().contains("Country") || entry.getKey().contains("State")) {
									{
										highLighterMethod(entry.getKey());
										selectFromDropdown(entry.getKey(), entry.getValue());
									}
								} else {
									highLighterMethod(entry.getKey());
									clickOn(entry.getKey());
									clearEditBox(entry.getKey());
									typeIn(entry.getKey(), entry.getValue());
								}
							}

						} else {
							testStepFailed(entry.getKey().split("#")[0] + " field was not found");
						}
					}
				}
			} else {
				testStepFailed("First Name field was not displayed");
			}
		} catch (Exception e) {
			testStepFailedDB("Could not fill the billing details successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to clear shipping Details.
	 */
	public void ClearDetailsShipping() {
		try {
			waitForElementToDisplay(email_Shipping, 10);
			String[] fields_Shipping = { email_Shipping, firstName_Shipping, lastName_Shipping, company_Shipping,
					streetAddressLineOne_Shipping, streetAddressLineTwo_Shipping, streetAddressLineThree_Shipping,
					city_Shipping, state_Shipping, zip_Shipping, country_Shipping, phoneNumber_Shipping };
			if (isElementDisplayed(firstName_Shipping)) {
				for (String field_Shipping : fields_Shipping) {
					if (isElementPresent(field_Shipping))
						if (isElementDisplayed(field_Shipping)) {
							if (field_Shipping.contains("Country") || field_Shipping.contains("State")) {
								highLighterMethod(field_Shipping);
								selectFromDropdown(field_Shipping, 0);
							} else {
								highLighterMethod(field_Shipping);
								clickOn(field_Shipping);
								clearEditBox(field_Shipping);
							}
						} else {
							testStepFailed(field_Shipping.split("#")[0] + " field was not found");
						}
				}
			} else {
				testStepFailed("First Name Text Box was not displayed");
			}
		} catch (

		Exception e) {
			testStepFailed("The shipping fields could not be cleared successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to clear billing Details.
	 */
	public void ClearDetailsBilling() {
		try {
			waitForElementToDisplay(firstName_Billing, 8);
			String[] fields_Billing = { firstName_Billing, lastName_Billing, company_Billing,
					streetAddressLineOne_Billing, streetAddressLineTwo_Billing, streetAddressLineThree_Billing,
					city_Billing, state_Billing, zip_Billing, country_Billing, phoneNumber_Billing };
			if (isElementDisplayed(firstName_Billing)) {
				for (String field_Billing : fields_Billing) {
					if (isElementPresent(field_Billing))
						if (isElementDisplayed(field_Billing)) {
							if (field_Billing.contains("Country") || field_Billing.contains("State")) {
								{
									highLighterMethod(field_Billing);
									selectFromDropdown(field_Billing, 0);
								}
							} else {
								highLighterMethod(field_Billing);
								clickOn(field_Billing);
								clearEditBox(field_Billing);
							}
						} else {
							testStepFailed(field_Billing.split("#")[0] + " field was not found");
						}
				}
			}
		} catch (Exception e) {
			testStepFailed("The billing fields could not be cleared successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on next in Shipping Tab
	 */
	public void clickNext() {
		try {
			if (isElementDisplayed(next)) {
				highLighterMethod(next);
				clickOn(next);
				waitForElementToDisplay(next, 15);
				waitTime(3);
			} else {
				testStepFailed("Next button was not displayed");
			}
		} catch (Exception e) {
			testStepFailedDB("Next could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Place Order.
	 */
	public void clickPlaceOrder() {
		try {
			if (isElementDisplayed(placeOrder)) {
				highLighterMethod(placeOrder);
				clickOn(placeOrder);
			} else {
				testStepFailed("Place Order button was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not click on Place Order");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Place Order.
	 */
	public void clickTermsCheckbox() {
		try {
			if (isElementDisplayed(termsCheckbox)) {
				highLighterMethod(termsCheckbox);
				clickOn(termsCheckbox);
			} else {
				testStepFailed("Terms Checkbox was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not click on Terms Checkbox");
			e.printStackTrace();
		}
	}

//	/**
//	 * Description: Method to click on Place Order.
//	 */
//	public void selectCreditCardPayment() {
//		try {
//			waitForElementToDisplay(paymentMethod_CreditCard, 60);
//			if (isElementDisplayed(paymentMethod_CreditCard)) {
//				highLighterMethod(paymentMethod_CreditCard);
//				clickOn(paymentMethod_CreditCard);
//			} else {
//				testStepFailed("Credit Card radio button was not displayed");
//			}
//		} catch (Exception e) {
//			testStepFailed("Could not select credit card for payment method");
//			e.printStackTrace();
//		}
//	}

	/**
	 * Description: Method to click Billing Shipping Address Same Check box.
	 */
	public void clickBillingShippingAddressSame() {
		try {
			if (isElementDisplayed(sameBillingShippingAddress)) {
				highLighterMethod(sameBillingShippingAddress);
				clickOn(sameBillingShippingAddress);
			} else {
				testStepFailed("Billing Shipping Address Same - Check box was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not click Billing Shipping Address Same - Check box");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify billing form is displayed.
	 */
	public void verifyBillingFormDisplayed() {
		try {
			if (isElementDisplayed(firstName_Billing)) {
				highLighterMethod(firstName_Billing);
				testStepInfo("The Billing Address form is displayed");

			} else {
				testStepFailed("The Billing Address form is not displayed", "First Name text box is not displayed");
			}
		} catch (Exception e) {
			testStepFailed("The Billing Address form is not displayed");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to fill the details for credit card payment
	 */
	public void fillDetailsforCreditCard(String cardNumber_data, String expirationMonth_data,
			String expirationYear_data, String cardVerificationNumber_data) {
		try {
			waitForElementToDisplay(cardNumber, 120);
			if (isElementDisplayed(cardNumber)) {
				if (cardNumber_data != null) {
					highLighterMethod(cardNumber);
					clearEditBox(cardNumber);
					typeIn(cardNumber, cardNumber_data);
					testStepInfo("Card number data was entered in the relevant field");
				}
				if (expirationMonth_data != null) {
					highLighterMethod(expiration_Month);
					selectFromDropdown(expiration_Month, expirationMonth_data);
					testStepInfo("Expiration date Month was selected successfully");
				}
				if (expirationYear_data != null) {
					highLighterMethod(expiration_Year);
					selectFromDropdown(expiration_Year, expirationYear_data);
					testStepInfo("Expiration date Year was selected successfully");
				}
				if (cardVerificationNumber_data != null) {
					highLighterMethod(cardVerificationNumber);
					clearEditBox(cardVerificationNumber);
					typeIn(cardVerificationNumber, cardVerificationNumber_data);
					testStepInfo("Card verification Code was entered in the relevant field");
				}
			} else {
				testStepFailed("The Card Number field was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Details could not be filled successfully for credit card");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify the success message displayed after order is
	 * placed.
	 */
	public void verifySuccessMessage() {
		try {
			if (isElementDisplayed(successCheckout)) {
				GOR.productAdded = false;
				testStepInfo("The Success Message for checkout was displayed.");
				highLighterMethod(successCheckout);
				manualScreenshot("Success Message");
			} else {
				testStepFailed("The Success Message for checkout was not displayed.");
			}
		} catch (Exception e) {
			testStepFailed("Could not verify the Success Message for checkout");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify the display of relevant message.
	 */
	public void verifyRelevantMessage(String error) {
		try {
			switch (error) {
			case "Invalid Card Number": {
				waitForElement(error_expirationDateInvalid, 6);
				if (isElementDisplayed(error_cardNumberInvalid)) {
					testStepInfo("The Error Message for Invalid Card Number was displayed");
					highLighterMethod(error_cardNumberInvalid);
				} else {
					testStepFailed("The Error Message for Invalid Card Number was not displayed.");
				}
				break;
			}

			case "Card Number does not match type": {
				waitForElement(error_cardTypeError, 6);
				if (isElementDisplayed(error_cardTypeError)) {
					testStepInfo("The Error Message for Card Number Number Does not match type was displayed");
					highLighterMethod(error_cardTypeError);
				} else {
					testStepFailed("The Error Message for Card Number Number Does not match type was not displayed.");
				}
				break;
			}

			case "Invalid Verification Number": {
				waitForElement(error_VerificationNumberInvalid, 6);
				if (isElementDisplayed(error_VerificationNumberInvalid)) {
					testStepInfo("The Error Message for invalid verification number was displayed.");
					highLighterMethod(error_VerificationNumberInvalid);
				} else {
					testStepFailed("The Error Message for invalid verification number was not displayed.");
				}
				break;
			}

			case "Invalid expiry date": {
				waitForElement(error_expirationDateInvalid, 6);
				if (isElementDisplayed(error_expirationDateInvalid)) {
					testStepInfo("The Error Message for invalid expiry date was displayed.");
					highLighterMethod(error_expirationDateInvalid);
				} else {
					testStepFailed("The Error Message for invalid expiry date was not displayed.");
				}
				break;
			}

			case "terms unchecked error": {
				waitForElement(error_termsUnchecked, 6);
				if (isElementDisplayed(error_termsUnchecked)) {
					testStepInfo("The Error Message for unchecked terms box was displayed.");
					highLighterMethod(error_termsUnchecked);
				} else {
					testStepFailed("The Error Message for unchecked terms box was not displayed.");
				}
				break;
			}

			case "Missing Shipping Method": {
				waitForElement(error_ShippingMethodMissing, 6);
				scrollToViewElement(error_ShippingMethodMissing);
				if (isElementDisplayed(error_ShippingMethodMissing)) {
					testStepInfo("The Error Message for not selecting Shipping Method was displayed.");
					highLighterMethod(error_ShippingMethodMissing);
				} else {
					testStepFailed("The Error Message for not selecting Shipping Method was not displayed.");
				}
				break;
			}

			case "Invalid Postal Code": {
				waitForElement(warning_InvaildZipCode, 6);
				scrollToViewElement(warning_InvaildZipCode);
				if (isElementDisplayed(warning_InvaildZipCode)) {
					testStepInfo("The Error Message for Invalid Post Code was displayed.");
					highLighterMethod(warning_InvaildZipCode);
				} else {
					testStepFailed("The Error Message for Invalid Post Code was not displayed.");
				}
				break;
			}

			case "Invalid Email": {
				waitForElement(error_InvalidEmailAddress, 6);
				scrollToViewElement(email_Shipping);
				if (isElementDisplayed(error_InvalidEmailAddress)) {
					testStepInfo("The Error Message for Invalid Email Address was displayed.");
					highLighterMethod(error_InvalidEmailAddress);
				} else {
					testStepFailed("The Error Message for Invalid Email Address was not displayed.");
				}
				break;
			}

			case "Review and Payments": {
				waitForElement(applyDiscountCode, 120);
				if (isElementDisplayed(applyDiscountCode)) {
					testStepInfo("The Review and Payments tab was opened successfully");
					highLighterMethod(applyDiscountCode);
				} else {
					testStepFailed("The Review and Payments tab could not be opened successfully");
				}
				break;
			}

//			case "Suggested Address": {
//				waitForElement(suggestedAddress, 6);
//				if (isElementDisplayed(suggestedAddress)) {
//					testStepInfo("Suggested Address was displayed.");
//					highLighterMethod(suggestedAddress);
//				} else {
//					testStepFailed("Suggested Address was not displayed.");
//				}
//				break;
//			}

			case "Discount Code Missing": {
				waitForElement(discountCodeMissingError, 6);
				if (isElementDisplayed(discountCodeMissingError)) {
					testStepInfo("Discount Code Missing Error was displayed.");
					highLighterMethod(discountCodeMissingError);
				} else {
					testStepFailed("Discount Code Missing Error was not displayed.");
				}
				break;
			}

			case "Invalid Coupon": {
				waitForElement(invalidCouponError, 6);
				if (isElementDisplayed(invalidCouponError)) {
					highLighterMethod(invalidCouponError);
					testStepInfo("Invalid Coupon Error was displayed.");
				} else {
					testStepFailed("Invalid Coupon Error was not displayed.");
				}
				break;
			}
			}
		} catch (Exception e) {
			testStepFailed("Relevant Error Meesage verification could not be done");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to check the presence of all the errors for empty fields
	 */
	public void VerifyEmptyFieldErrors(boolean email) {
		try {
			{
				waitForElementToDisplay(error_FirstNameMissing, 12);
				String[] emptyCardDetailsErrors = { error_EmailMissing, error_FirstNameMissing, error_LastNameMissing,
						error_StreetAddressMissing, error_CityMissing, error_StateMissing, error_PostCodeMissing,
						error_PhoneNumberMissing };
				for (String error : emptyCardDetailsErrors) {
					if (error.contains("Email")) {
						if (email == false)
							continue;
					}
					if (isElementDisplayed(error)) {
						highLighterMethod(error);
					} else {
						testStepFailed(error.split("#")[0] + " error was not displayed");
					}
				}
			}

		} catch (Exception e) {
			testStepFailed("Validation of empty field errors was not successful");
			e.printStackTrace();
		}
	}

//	/**
//	 * Description: Method to check the presence of the errors for empty card
//	 * details
//	 */
//	public void VerifyEmptyCardDetailsErrors() {
//		try {
//			String[] emptyCardDetailsErrors = { error_cardNumberMissing, error_expirationDate_MonthInvalid,
//					error_expirationDate_YearInvalid, error_VerificationNumberMissing };
//			for (String error : emptyCardDetailsErrors) {
//				if (isElementDisplayed(error)) {
//					highLighterMethod(error);
//				} else {
//					testStepFailed(error.split("#")[0] + " error for empty field was not displayed");
//				}
//			}
//		} catch (Exception e) {
//			testStepFailed("Validation of errors for empty card details could not be done");
//			e.printStackTrace();
//		}
//	}

//
//	/**
//	 * Description: Method to type the numbers from keyboard according the the input
//	 * passed
//	 */
//	public void typeNumbersFromKeyboard(String element, String numberData) {
//		try {
//			char input[] = numberData.toCharArray();
//			for (char number : input) {
//				switch (Integer.parseInt(String.valueOf(number))) {
//				case 1:
//					findWebElement(element).sendKeys(Keys.NUMPAD1);
//					break;
//				case 2:
//					findWebElement(element).sendKeys(Keys.NUMPAD2);
//					break;
//				case 3:
//					findWebElement(element).sendKeys(Keys.NUMPAD3);
//					break;
//				case 4:
//					findWebElement(element).sendKeys(Keys.NUMPAD4);
//					break;
//				case 5:
//					findWebElement(element).sendKeys(Keys.NUMPAD5);
//					break;
//				case 6:
//					findWebElement(element).sendKeys(Keys.NUMPAD6);
//					break;
//				case 7:
//					findWebElement(element).sendKeys(Keys.NUMPAD7);
//					break;
//				case 8:
//					findWebElement(element).sendKeys(Keys.NUMPAD8);
//					break;
//				case 9:
//					findWebElement(element).sendKeys(Keys.NUMPAD9);
//					break;
//				case 0:
//					findWebElement(element).sendKeys(Keys.NUMPAD0);
//					break;
//				}
//
//			}
//		} catch (Exception e) {
//			testStepFailed("Could not type numbers from Keyboard successfully");
//			e.printStackTrace();
//		}
//	}

	/**
	 * Description: Method to verify Checkout page opened successfully.
	 */
	public void verifyShippingHeader() {
		try {
			waitForElementToDisplay(shippingAddressHeader, 120);
			if (isElementDisplayed(shippingAddressHeader)) {
				highLighterMethod(shippingAddressHeader);
				testStepInfo("Shipping Tab under Checkout page opened successfully");
			} else {
				testStepFailed("Checkout page was not opened successfully",
						"Shipping Address Header was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Checkout page was not opened successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify if 'Items in Cart' has products displayed and
	 * click on it to collapse and display products, if not visible.
	 * 
	 */
	public void checkItemsInCartProductsDisplay() {
		try {
			waitForElementToDisplay(itemsInCartHeader, 120);
			if (isElementDisplayed(itemsInCartHeader)) {
				String productsAreDisplayed = getAttributeValue(itemsInCartHeader, "aria-selected");
				if (productsAreDisplayed.contains("true")) {
					testStepInfo("Products in 'Items In Cart' are already displayed");
				} else {
					clickOn(itemsInCartHeader);
				}
			}
		} catch (Exception e) {
			testStepFailed("Display of products present under 'Items in Cart' could not be verified");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify presence of the expected product in the
	 * checkout
	 * 
	 */
	public void verifyPresenceOfExpectedProduct(String productName) {
		try {
			boolean productPresent = false;
			if (isElementPresent(productsPresent)) {
				manualScreenshot("The Product added is present in the cart");
				List<WebElement> products = new ArrayList<WebElement>();
				products = findWebElements(productsPresent);
				for (WebElement product : products) {
					if (product.getText().toLowerCase().contains(productName.toLowerCase())) {
						productPresent = true;
						highLighterMethod(product);
						break;
					}
				}
				if (productPresent == true) {
					testStepInfo("The product added to cart " + productName + ", found in My Cart page");
				} else
					testStepFailed("Product added to cart " + productName + ", not found in My Cart page");
			} else
				testStepFailed("No products present in My cart page");
		} catch (Exception e) {
			testStepFailed("Presence of expected product could not be verified");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click verify the auto-filled from Shipping Address to
	 * Billing Address.
	 */
	public void verifyDataInBillingAddress(String firstName, String lastName, String streetAddress_LineOne,
			String streetAddress_LineTwo, String streetAddress_LineThree, String city, String state, int zip,
			String country, String phoneNumber) {
		try {
			if (isElementDisplayed(billingAddress)) {
				String billingAddress = getText(Checkout.billingAddress);
				if (billingAddress.contains(firstName) && billingAddress.contains(lastName)
						&& billingAddress.contains(streetAddress_LineOne)
						&& billingAddress.contains(streetAddress_LineTwo)
						&& billingAddress.contains(streetAddress_LineThree) && billingAddress.contains(city)
						&& billingAddress.contains(state) && billingAddress.contains(String.valueOf(zip))
						&& billingAddress.contains(country) && billingAddress.contains(phoneNumber)) {
					highLighterMethod(Checkout.billingAddress);
					testStepInfo(
							"The  Shipping Address  is getting displayed as Billing Address when the \"My billing and shipping address are the same\" checkbox is selected");
				} else {
					testStepFailed(
							"Shipping Address is not getting displayed  as Billing Address when the \"My billing and shipping address are the same\" checkbox is selected");
				}
			} else {
				testStepFailed("Billing Address is not displayed");
			}
		} catch (Exception e) {
			testStepFailed(
					"Could not verify whether Shipping Address is getting displayed  as Billing Address when the \"My billing and shipping address are the same\" checkbox is selected");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Update for Billing Address.
	 */
	public void clickUpdate() {
		try {
			if (isElementDisplayed(update)) {
				highLighterMethod(update);
				clickOn(update);
			} else {
				testStepFailed("Update button was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not click on Update Button");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on the Shipping Progress Bar.
	 */
	public void clickShippingProgressBar() {
		try {
			waitForElementToDisplay(shippingProgressBar, 60);
			if (isElementDisplayed(shippingProgressBar)) {
				highLighterMethod(shippingProgressBar);
				clickOn(shippingProgressBar);
			} else {
				testStepFailed("Shipping Progress Bar was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not click on Shipping Progress Bar");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on the Apply Discount Code Header.
	 */
	public void clickApplyDiscountCodeHeader() {
		try {
			waitForElementToDisplay(applyDiscountCode, 120);
			scrollToViewElement(applyDiscountCode);
			if (isElementDisplayed(applyDiscountCode)) {
				highLighterMethod(applyDiscountCode);
				clickOn(applyDiscountCode);
			} else {
				testStepFailed("Apply Discount Code Header was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not click on Apply Discount Code Header");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to enter discount code.
	 */
	public void enterDiscountCode(String discountCode) {
		try {
			if (isElementDisplayed(discountCodeTextBox)) {
				highLighterMethod(discountCodeTextBox);
				typeIn(discountCodeTextBox, discountCode);
			} else {
				testStepFailed("Discount Code TextBox was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not enter Discount Code");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on apply discount.
	 */
	public void clickApplyDiscount() {
		try {
			if (isElementDisplayed(applyDiscount)) {
				highLighterMethod(applyDiscount);
				clickOn(applyDiscount);
			} else {
				testStepFailed("Apply Discount button was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Apply Discount button could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on cancel for discount coupon.
	 */
	public void clickCancel_DiscountCoupon() {
		try {
			if (isElementDisplayed(cancelCoupon)) {
				highLighterMethod(cancelCoupon);
				clickOn(cancelCoupon);
			} else {
				testStepFailed("Cancel button was not displayed for Discount Coupon");
			}
		} catch (Exception e) {
			testStepFailed("Cancel button could not be clicked for Discount Coupon");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to get the shipping tax price.
	 */
	public float getShippingPrice() {
		float shippingPrice_data = 0;
		try {
			waitForElementToDisplay(shippingPrice, 120);
			scrollToViewElement(shippingPrice);
			if (isElementDisplayed(shippingPrice)) {
				highLighterMethod(shippingPrice);
				shippingPrice_data = Float.parseFloat(getText(Checkout.shippingPrice).substring(1));
				return shippingPrice_data;
			} else
				testStepFailed("Shipping Price is not displayed");
		} catch (Exception e) {
			testStepFailed("Shipping Price could not be fetched");
			e.printStackTrace();
		}
		return shippingPrice_data;
	}

	/**
	 * Description: Method to click on desired shipping option.
	 */
	public void clickDesiredShippingOption(String option) {
		try {
			switch (option) {
			case "standardShipping_vape": {
				if (isElementDisplayed(standardShipping_vape)) {
					highLighterMethod(standardShipping_vape);
					clickOn(standardShipping_vape);
				} else {
					testStepFailed("Standard Shipping option for vape products was not displayed");
				}
				break;
			}

			case "freeStandardShipping_vape": {
				waitForElementToDisplay(freeStandardShipping_vape, 120);
				if (isElementDisplayed(freeStandardShipping_vape)) {
					highLighterMethod(freeStandardShipping_vape);
					clickOn(freeStandardShipping_vape);
				} else {
					testStepFailed("Free Standard Shipping option for vape products was not displayed");
				}
				break;
			}

			case "standardShipping_withoutVape": {
				waitForElementToDisplay(standardShipping_withoutVape, 180);
				if (isElementDisplayed(standardShipping_withoutVape)) {
					highLighterMethod(standardShipping_withoutVape);
					clickOn(standardShipping_withoutVape);
				} else {
					testStepFailed("Standard Shipping option for without vape products scenario was not displayed");
				}
				break;
			}

			case "upsOvernightShipping": {
				waitForElementToDisplay(upsOvernightShipping, 120);
				scrollToViewElement(upsOvernightShipping);
				if (isElementDisplayed(upsOvernightShipping)) {
					highLighterMethod(upsOvernightShipping);
					clickOn(upsOvernightShipping);
				} else {
					testStepFailed("UPS Overnight Shipping was not displayed");
				}
				break;
			}

			case "freeStandardShipping_withoutVape": {
				if (isElementDisplayed(freeStandardShipping_withoutVape)) {
					highLighterMethod(freeStandardShipping_withoutVape);
					clickOn(freeStandardShipping_withoutVape);
				} else {
					testStepFailed(
							"Free Standard Shipping option for without vape products scenario was not displayed");
				}
				break;
			}

			case "freeExpeditedShipping_withoutVape": {
				if (isElementDisplayed(freeExpeditedShipping_withoutVape)) {
					highLighterMethod(freeExpeditedShipping_withoutVape);
					clickOn(freeExpeditedShipping_withoutVape);
				} else {
					testStepFailed(
							"Free Expedited Shipping option for without vape products scenario was not displayed");
				}
				break;
			}
			}
		} catch (Exception e) {
			testStepFailed("Desired Shipping option could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to check Order Total.
	 */
	public void checkOrderTotal(float price_1, float price_2, float price_3) {
		float orderTotal_data;
		try {
			waitForElementToDisplay(orderTotal, 300);
			scrollToViewElement(orderTotal);
			if (isElementDisplayed(orderTotal)) {
				orderTotal_data = Float.parseFloat(getText(orderTotal).substring(1));
				float sum = (float) (Math.round((price_1 + price_2 + price_3) * 100) / 100.0);
				if (orderTotal_data == sum) {
					highLighterMethod(orderTotal);
					testStepInfo("The expected order total is present");
				} else {
					testStepFailed("The expected order total is not present");
				}
			} else
				testStepFailed("Order Total is not displayed");
		} catch (Exception e) {
			testStepFailed("Order Total could not be checked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify the presence of the Login Section.
	 */
	public void verifyLoginSection() {
		try {
			waitForElementToDisplay(login, 60);
			if (isElementDisplayed(login) && isElementDisplayed(password)) {
				highLighterMethod(login);
				highLighterMethod(password);
				testStepInfo("The Login Section was displayed");
			} else {
				testStepFailed("Login Section was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Login Section was not displayed");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to login.
	 */
	public void login(String passwordData) {
		try {
			waitForElementToDisplay(login, 60);
			if (isElementDisplayed(login) && isElementDisplayed(password)) {
				highLighterMethod(password);
				typeIn(password, passwordData);
				highLighterMethod(login);
				clickOn(login);
			} else {
				testStepFailed("Login Section was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Login could not be done in checkout page");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify the presence of the Saved Address Section.
	 */
	public void verifySavedAddressSection() {
		try {
			waitForElementToDisplay(savedAddressBlock, 60);
			if (isElementDisplayed(savedAddressBlock)) {
				highLighterMethod(savedAddressBlock);
				testStepInfo("The Saved Address Block was displayed");
			} else {
				testStepFailed("Saved Address Section was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Saved Address Section was not displayed");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on New Address button.
	 */
	public void clickNewAddressButton() {
		try {
			if (isElementDisplayed(newAddressButton)) {
				highLighterMethod(newAddressButton);
				clickOn(newAddressButton);
			} else {
				testStepFailed("New Address button was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("New Address button could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Ship Here button.
	 */
	public void clickShipHereButton() {
		try {
			if (isElementDisplayed(shipHere)) {
				highLighterMethod(shipHere);
				clickOn(shipHere);
			} else {
				testStepFailed("Ship Here button was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Ship Here button could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Save Address Check box - Shipping.
	 */
	public void clickSaveAddressCheckboxShipping() {
		try {
			if (isElementDisplayed(saveAddressCheckboxShipping)) {
				highLighterMethod(saveAddressCheckboxShipping);
				clickOn(saveAddressCheckboxShipping);
			} else {
				testStepFailed("Save Address checkbox for Shipping was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Save Address checkbox for Shipping could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Save Address Check box - Billing.
	 */
	public void clickSaveAddressCheckboxBilling() {
		try {
			if (isElementDisplayed(saveAddressCheckboxBilling)) {
				highLighterMethod(saveAddressCheckboxBilling);
				clickOn(saveAddressCheckboxBilling);
			} else {
				testStepFailed("Save Address checkbox for Billing was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Save Address checkbox for Billing could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify the presence of the New Address block.
	 */
	public void verifyNewAddresBlock(HashMap<String, String> shippingData) {
		try {
			waitForElementToDisplay(savedAddressBlock_New, 60);
			if (isElementDisplayed(savedAddressBlock_New)) {
				String content = getText(savedAddressBlock_New);
				if (content.contains(shippingData.get("firstName_data"))
						&& content.contains(shippingData.get("lastName_data")) && content.contains("United States")
						&& content.contains(shippingData.get("streetAddressFirstLine_data"))
						&& content.contains(shippingData.get("streetAddressSecondLine_data"))
						&& content.contains(shippingData.get("streetAddressThirdLine_data"))
						&& content.contains(shippingData.get("city_data"))
						&& content.contains(shippingData.get("state_data"))
						&& content.contains(shippingData.get("postCode_data"))
						&& content.contains(shippingData.get("phoneNumber_data"))) {
					highLighterMethod(savedAddressBlock_New);
					testStepInfo("The New Address Block was displayed");
				}
			} else {
				testStepFailed("New Address was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("New Address was not displayed");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to select 'New Address' from Billing Address Drop down.
	 */
	public void selectNewAddressFromDropdown() {
		try {
			if (isElementDisplayed(billingAddressDropdown)) {
				highLighterMethod(billingAddressDropdown);
				selectFromDropdown(billingAddressDropdown, 1);
			} else {
				testStepFailed("Billing Address Dropdown  was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Billing Address Dropdown could not be accessed");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to expand order summary in checkout page.
	 */
	public void expandOrderSummary() {
		try {
			String ariaExpanded = "";
			if (isElementDisplayed(orderSummaryExpand)) {
				ariaExpanded = getAttributeValue(orderSummaryExpand, "aria-expanded");
				if (ariaExpanded.contains("false")) {
					highLighterMethod(orderSummaryExpand);
					clickOn(orderSummaryExpand);
				} else if (ariaExpanded.contains("true")) {
					testStepInfo("Order Summary already expanded");
				}
			} else {
				testStepFailed("Order Summary expand button was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Order Summary could not be expanded");
			e.printStackTrace();
		}
	}

}
