package pages;

import org.apache.tools.ant.taskdefs.WaitFor;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;

public class Newsletter extends ApplicationKeywords {

	private static final String email = "Email #xpath=//div[not(contains(@class,'mobile')) and (contains(@class,'klaviyo'))]//input[@type='email']";
	private static final String subscribe = "Subscribe #xpath=//div[contains(@class,'desktop')]//button[contains(text(),'SUBSCRIBE')]";
	private static final String emailEmptyError = "Email Empty Error #xpath=//span[contains(text(),'This field is required')]";
	private static final String emailInvalidError = "Email Invalid Error #xpath=//span[contains(text(),'Please enter a valid email address')]";
	private static final String successMessage = "Success Message #xpath=//strong[contains(text(),'Thanks for subscribing!')]";
	private static final String instantCouponCode = "Coupon Code #xpath=//span[contains(text(),'Use promo code NEWS10 to save 10% on your next purchase')]";

	public Newsletter(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method to enter email.
	 */
	public void enterEmail(String email_data) {
		try {
			if (isElementDisplayed(email)) {
				highLighterMethod(email);
				typeIn(email, email_data);
			}
		} catch (Exception e) {
			testStepFailed("Data could not be entered in relevant email textbox");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to enter email.
	 */
	public void clickSubscribe() {
		try {
			if (isElementDisplayed(subscribe)) {
				highLighterMethod(subscribe);
				clickOn(subscribe);
			}
		} catch (Exception e) {
			testStepFailed("Subscribe could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify errors.
	 */
	public void verifyErrors(String option) {
		try {
			switch (option) {
			case "emptyEmail": {
				if (isElementDisplayed(emailEmptyError)) {
					scrollToViewElement(email);
					highLighterMethod(emailEmptyError);
					testStepInfo("The Error for empty email field was displayed");
				} else {
					testStepFailed("The Error for empty email field was not displayed", "Element not present");
				}
				break;
			}
			case "invalidEmail": {
				if (isElementDisplayed(emailInvalidError)) {
					scrollToViewElement(email);
					highLighterMethod(emailInvalidError);
					testStepInfo("The Error for invalid email entry was displayed");
				} else {
					testStepFailed("The Error for invalid email entry was not displayed", "Element not present");
				}
				break;
			}
			}
		} catch (Exception e) {
			testStepFailed("The error message could not be validated");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify success Message and instant coupon code message
	 * for Newsletter Subscription
	 */
	public void verify_SuccessMessage_InstantCouponCode() {
		try {
			waitForElementToDisplay(successMessage, 30);
			if (isElementDisplayed(successMessage)) {
				highLighterMethod(successMessage);
				testStepInfo("The Success Message for Newsletter Subscription was displayed");
			} else {
				testStepFailed("The Success Message for Newsletter Subscription was not displayed",
						"Element not present");
			}

			if (isElementDisplayed(instantCouponCode)) {
				highLighterMethod(instantCouponCode);
				testStepInfo("The instant coupon code for Newsletter Subscription was displayed");
			} else {
				testStepFailed("The instant coupon code for Newsletter Subscription was not displayed",
						"Element not present");
			}
		} catch (Exception e) {
			testStepFailed(
					"The Success Message and instant coupon code message for Newsletter Subscription could not be validated");
			e.printStackTrace();
		}
	}

}
