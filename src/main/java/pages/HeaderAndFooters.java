package pages;

import java.util.Set;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;

public class HeaderAndFooters extends ApplicationKeywords {

	private static final String signIn = "Sign In #xpath=//div[contains(@class,'header')]//a[contains(text(),'Sign In')]";
	private static final String createAccount = "Create An Account #xpath=//div[contains(@class,'header')]//a[contains(text(),'Create an Account')]";
	private static final String createAnAccountButton = "Create Account Button #xpath=//span[contains(text(),'Create an Account')]/../../button";
	private static final String signInButton = "Sign In Button #xpath=//button[contains(@class,'primary')]//span[contains(text(),'Sign In')]";

	// Header
	private static final String home = "Home #xpath=//li//a[contains(text(),'HOME')]";
	private static final String products = "Products #xpath=//li//span[contains(text(),'PRODUCTS')]";
	private static final String products_Delta8Gummies = "Delta-8 Gummies #xpath=//ul[@id='mobile-menu-37-1']//a[contains(text(),'Delta 8 Gummies')]";
	private static final String products_Delta8Vape = "Delta-8 Vape #xpath=//ul[@id='mobile-menu-37-1']//a[contains(text(),'Vape')]";
	private static final String products_Delta8Oils = "Delta-8 Oils #xpath=//ul[@id='mobile-menu-37-1']//a[contains(text(),'Delta 8 Oils')]";
	private static final String products_Trifecta = "Delta-8 Vape #xpath=//ul[@id='mobile-menu-37-1']//a[contains(text(),'Trifecta')]";
	private static final String products_Delta8PainRub = "Delta-8 Pain Rub #xpath=//ul[@id='mobile-menu-37-1']//a[contains(text(),'Pain Rub')]";

	private static final String faq = "FAQ #xpath=//li//span[contains(text(),'FAQ')]";
	private static final String aboutUs = "About Us #xpath=//a/span[contains(text(),'ABOUT US')]";
	private static final String wholesale = "Wholesale #xpath=//a/span[contains(text(),'WHOLESALE')]";
	private static final String contact = "Contact #xpath=//span[contains(text(),'CONTACT')]";
	private static final String miniCart = "MiniCart #xpath=//div[@data-block='minicart']/a";
	private static final String logo = "MiniCart #xpath=//div[contains(@class,'header ')]//a[contains(@title,'logo')]";

	// Search
	private static final String searchBox = "Search #id=search";
	private static final String searchButton = "Search #xpath=//span[contains(text(),'Search')]/../..//button";
	private static final String noSearchResults_Message_SearchPage = "No Search Results Message #xpath=//div[contains(text(),'Your search returned no results.')]";
//	private static final String minimumQueryLength_Message_SearchPage = "Minimum Query Length Message #xpath=//div[contains(text(),'Minimum Search query length is 3')]";
	private static final String numberOfItems_SearchPage = "Number of items in Search Page #xpath=//div[contains(@class,'toolbar')][1]//p[@id='toolbar-amount']";
	private static final String gummiesSearchProductTitle_SearchPage = "Gummies Search Product Title #xpath=//strong/a[contains(text(),'Gummies')]";

	// Footer
	private static final String homeFooterLink = "Home Footer Link #xpath=//span[contains(@class,'footer')]//a[contains(text(),'HOME')]";
	private static final String aboutFooterLink = "About Footer Link #xpath=//span[contains(@class,'footer')]//a[contains(text(),'ABOUT')]";
	private static final String wholesaleFooterLink = "Wholesale Footer Link #xpath=//span//a[contains(text(),'WHOLESALE')]";
	private static final String termsFooterLink = "Terms Footer Link #xpath=//span[contains(@class,'footer')]/a[contains(text(),'TERMS')]";
	private static final String privacyFooterLink = "Privacy Footer Link #xpath=//span[contains(@class,'footer')]/a[contains(text(),'PRIVACY')]";
	private static final String shippingFooterLink = "Shipping Footer Link #xpath=//span/a[contains(text(),'SHIPPING')]";
	private static final String returnsFooterLink = "Shipping Footer Link #xpath=//span/a[contains(text(),'RETURNS')]";
	private static final String newsletterFooterLink = "Newsletter Footer Link #xpath=//span/a[contains(text(),'NEWSLETTER')]";
//	private static final String contactFooterLink = "Contact #xpath=//span//a[contains(text(),'Contact')]";
	private static final String labTestingFooterLink = "Lab Testing Footer Link #xpath=//span//a[contains(text(),'LAB')]";
	private static final String ccpaFooterLink = "CCPA Footer Link #xpath=//span/a[contains(text(),'CCPA')]";
	private static final String instagramFooterLink = "Instagram Footer Link #xpath=//div//a[contains(@href,'instagram')]";
//	private static final String twitterFooterLink = "Twitter Footer Link #xpath=//div//a[contains(@href,'twitter')]";
//	private static final String faqFooterLink = "FAQ Footer Link #xpath=//span//a[contains(@href,'faq')]";
	private static final String logoFooterLink = "Logo Footer Link #xpath=//figure//img[contains(@title,'logo')][1]";

	private static final String image_HomePage = "Home Banner Slide #xpath=//div[contains(@class,'slick-slide') and @data-element][1]";
	private static final String miniCartEmptyMessage = "Mini Cart Empty #xpath=//strong[contains(text(),'You have no items in your shopping cart.')]";
//	private static final String productsPresent = "Products Present #xpath=//strong[@class='product-item-name']/a";
	private static final String miniCartSubtotalHeader = "Mini Cart Subtotal Header #xpath=//span[contains(text(),'Cart Subtotal')]";
	private static final String shoppingCartHeader = "Shopping Cart Header #xpath=//h1[@class='page-title']/span[contains(text(),'Shopping Cart')]";
	private static final String shippingAddressHeader = "Shipping Address Header #xpath=//li/div[contains(text(),'Shipping Address')]";
	private static final String emptyCartMessage = "Empty Cart Message #xpath=//p[contains(text(),'You have no items in your shopping cart.')]";
	private static final String productsHeader = "All Products Header #xpath=//h1//span[contains(text(),'Products')]";
	private static final String products_Delta8GummiesHeader = "Delta-8 Gummies Header #xpath=//h1/span[contains(text(),'Delta 8 Gummies')]";
	private static final String products_Delta8VapeHeader = "Delta-8 Vape Header #xpath=//h1/span[contains(text(),'Delta 8 Vape')]";
	private static final String products_Delta8OilsHeader = "Delta-8 Vape Header #xpath=//h1/span[contains(text(),'Delta 8 Oils')]";
	private static final String products_TrifectaHeader = "Delta-8 Vape Header #xpath=//h1/span[contains(text(),'Trifecta')]";
	private static final String products_Delta8PainRubHeader = "Delta-8 Pain Rub Header #xpath=//h1/span[contains(text(),'Delta 8 Pain Rub')]";
	private static final String aboutUsHeader = "About Us Header #xpath=//h1[contains(text(),'About Mystic Labs')]";
	private static final String faqHeader = "FAQ Header #xpath=//h1[contains(text(),'FAQ')]";
	private static final String labTestsHeader = "Lab Tests Header #xpath=//div/h1[contains(text(),'Lab Test')]";
	private static final String wholesaleHeader = "Wholesale Header #xpath=//div/h1[contains(text(),'Wholesale')]";
	private static final String contactUsHeader = "Contact Us Header #xpath=//div/h1[contains(text(),'Contact')]";

	private static final String termsConditionsHeader = "Terms and Conditions Header #xpath=//div/h1[contains(text(),'Terms and Conditions')]";
	private static final String privacyPolicyHeader = "Privacy Policy Header #xpath=//div/h1[contains(text(),'Privacy Policy')]";
	private static final String shippingPolicyHeader = "Shipping Policy Header #xpath=//div//h1[contains(text(),'Shipping Policy')]";
	private static final String refundPolicyHeader = "Relief Policy Header #xpath=//div//h1[contains(text(),'Refund Policy')]";
	private static final String newsletterHeader = "Newsletter Header #xpath=//div//h1[contains(text(),'Newsletter')]";
	private static final String ccpaHeader = "CCPA Header Header #xpath=//h1[contains(text(),'CCPA Data Request')]";

	private static final String myAccountDropdown = "My Account dropdown #xpath=//span[contains(text(),'Change')]/..";
	private static final String myAccountDropdown_MyAccount = "My Account dropdown - My Account #xpath=//a[contains(text(),'My Account')]";
	private static final String myAccountDropdown_WishList = "My Account dropdown - Wish List #xpath=//ul[@class='header links']//a[contains(text(),'My Wish List')]";
	private static final String myAccountDropdown_SignOut = "My Account dropdown - Sign Out #xpath=//div[@class='customer-menu']//a[contains(text(),'Sign Out')]";
	private static final String over21AgeButton = "I'm over 21 button #xpath=//button[contains(text(),\"I'm over 21\")]";

	public HeaderAndFooters(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method to click on the Mystic Labs Logo
	 */
	public void clickLogo() {
		try {
			if (isElementPresent(logo)) {
				highLighterMethod(logo);
				clickOn(logo);
			} else {
				testStepFailed("Could not click on the Mystic Labs Logo", "Element not present");
			}
		} catch (Exception e) {
			testStepFailed("Could not click on Mystic Labs Logo successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to go to the SignIn page
	 */
	public void goToSignIn() {
		try {
			if (isElementPresent(signIn)) {
				highLighterMethod(signIn);
				clickOn(signIn);
			} else {
				testStepFailed("Could not click on the Sign In button in HomePage", "Element not present");
			}
		} catch (Exception e) {
			testStepFailed("Could not click on Sign In successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to go to the Create An Account page
	 */
	public void goToCreateAnAccount() {
		try {
			if (isElementPresent(createAccount)) {
				highLighterMethod(createAccount);
				clickOn(createAccount);
			} else {
				testStepFailed("Could not click on the Create An Account button in HomePage", "Element not present");
			}
		} catch (Exception e) {
			testStepFailed("Could not click on Create An Account successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to click on home in Header.
	 */
	public void goTo_Home() {

		try {
			if (isElementDisplayed(home)) {
				highLighterMethod(home);
				clickOn(home);
			} else {
				testStepFailed("Home not present in Header Menu");
			}
		} catch (Exception e) {
			testStepFailed("Home could not be clicked successfully");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method is used to click on Mini cart Icon in Header
	 */
	public void clickOnMiniCart() {

		try {
			waitForElementToDisplay(miniCart, 120);
			waitTime(3);
			if (isElementDisplayed(miniCart)) {
				highLighterMethod(miniCart);
				clickOn(miniCart);
			} else {
				testStepFailed("Mini Cart not present in Header");
			}
		} catch (Exception e) {
			testStepFailed("Mini Cart could not be clicked successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to navigate in Products from Header menu.
	 */
	public void goTo_Products(int subMenu) {

		try {
			if (subMenu == 0) {
				if (isElementPresent(products)) {
					highLighterMethod(products);
					clickOn(products);
				} else {
					testStepFailed("Products not present in Header Menu");
				}
			} else {
				switch (subMenu) {
				case 1: {
					if (isElementPresent(products_Delta8Gummies)) {
						navigateMenu_Two(products, products_Delta8Gummies);
					} else {
						testStepFailed("Delta-8 Gummies not present under Products");
					}
					break;
				}

				case 2: {
					if (isElementPresent(products_Delta8Vape)) {
						navigateMenu_Two(products, products_Delta8Vape);
					} else {
						testStepFailed("Delta-8 Vape not present under Products");
					}
					break;
				}

				case 3: {
					if (isElementPresent(products_Delta8Oils)) {
						navigateMenu_Two(products, products_Delta8Oils);
					} else {
						testStepFailed("Delta-8 Oils not present under Products");
					}
					break;
				}

				case 4: {
					if (isElementPresent(products_Trifecta)) {
						navigateMenu_Two(products, products_Trifecta);
					} else {
						testStepFailed("Trifecta Products not present under Products");
					}
					break;
				}

				case 5: {
					if (isElementPresent(products_Delta8PainRub)) {
						navigateMenu_Two(products, products_Delta8PainRub);
					} else {
						testStepFailed("Delta-8 Pain Rub not present under Products");
					}
					break;
				}
				}
			}
		} catch (Exception e) {
			testStepFailed("Desired option from Products in Header Menu could not be clicked successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to click on About Us
	 */
	public void goTo_AboutUs() {

		try {
			if (isElementPresent(aboutUs)) {
				highLighterMethod(aboutUs);
				clickOn(aboutUs);
			} else {
				testStepFailed("About Us not present in Header Menu");
			}
		} catch (Exception e) {
			testStepFailed("About Us could not be clicked successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to click on FAQ
	 */
	public void goTo_faq() {

		try {
			if (isElementPresent(faq)) {
				highLighterMethod(faq);
				clickOn(faq);
			} else {
				testStepFailed("FAQ not present in Header Menu");
			}
		} catch (Exception e) {
			testStepFailed("FAQ could not be clicked successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to click on Wholesale
	 */
	public void goTo_Wholesale() {

		try {
			if (isElementPresent(wholesale)) {
				highLighterMethod(wholesale);
				clickOn(wholesale);
			} else {
				testStepFailed("Wholesale not present in Header Menu");
			}
		} catch (Exception e) {
			testStepFailed("Wholesale could not be clicked successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to click on contact
	 */
	public void goTo_Contact() {

		try {
			if (isElementPresent(contact)) {
				highLighterMethod(contact);
				clickOn(contact);
			} else {
				testStepFailed("Contact not present in Header Menu");
			}
		} catch (Exception e) {
			testStepFailed("Contact could not be clicked successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to enter query into search box.
	 */
	public void enterQuery_SearchBox(String query) {

		try {
			if (isElementPresent(searchBox)) {
				highLighterMethod(searchBox);
				typeIn(searchBox, query);
			} else {
				testStepFailed("Search box not present in Header Menu");
			}
		} catch (Exception e) {
			testStepFailed("Query could not be entered into the search box successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to click on search button.
	 */
	public void clickSearchButton() {

		try {
			if (isElementPresent(searchButton)) {
				highLighterMethod(searchButton);
				clickOn(searchButton);
			} else {
				testStepFailed("Search button not present in Header Menu");
			}
		} catch (Exception e) {
			testStepFailed("Search button could not be clicked successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to verify the routing.
	 */
	public void verifyNavigation(String option) {

		try {
			switch (option) {
			case "home": {
				if (isElementDisplayed(image_HomePage)) {
					highLighterMethod(image_HomePage);
					scrollToViewElement(image_HomePage);
					testStepInfo("The relevant page was opened upon clicking Home");
				} else {
					testStepFailed("Home Page navigation could not be verfied");
				}
				break;
			}

			case "emptyMiniCart": {
				if (isElementDisplayed(miniCartEmptyMessage)) {
					highLighterMethod(miniCartEmptyMessage);
					testStepInfo("The empty cart message was displayed in the Mini Cart");
				} else {
					testStepFailed("The empty cart message was not displayed in the Mini Cart");
				}
				break;
			}

			case "miniCartWithProducts": {
				if (isElementDisplayed(miniCartSubtotalHeader)) {
					highLighterMethod(miniCartSubtotalHeader);
					testStepInfo("Mini cart with a product opened successfully");
				} else {
					testStepFailed("Mini cart with a product opened successfully",
							"Mini Cart Subtotal Header not displayed");
				}
				break;
			}

			case "shoppingCart": {
				if (isElementDisplayed(shoppingCartHeader)) {
					highLighterMethod(shoppingCartHeader);
					testStepInfo("Shopping Cart with a product opened successfully");
				} else {
					testStepFailed("Shopping Cart with a product opened successfully",
							"Shopping Cart Header not displayed");
				}
				break;
			}

			case "emptyCart": {
				if (isElementDisplayed(emptyCartMessage)) {
					highLighterMethod(emptyCartMessage);
					testStepInfo("Empty Cart message was displayed in the Shopping Cart page");
				} else {
					testStepFailed("Empty Cart message was not displayed in the Shopping Cart page");
				}
				break;
			}

			case "checkout": {
				waitForElementToDisplay(shippingAddressHeader, 120);
				if (isElementDisplayed(shippingAddressHeader)) {
					highLighterMethod(shippingAddressHeader);
					testStepInfo("The checkout page was displayed");
				} else {
					testStepFailed("he checkout page was not displayed", "Shipping Address Header was not displayed");
				}
				break;
			}

			case "products": {
				if (isElementDisplayed(productsHeader)) {
					highLighterMethod(productsHeader);
					testStepInfo("Products page was opened successfully");
				} else {
					testStepFailed("Products page could not be opened successfully",
							"Products Header was not displayed");
				}
				break;
			}

			case "Delta-8 Gummies": {
				if (isElementDisplayed(products_Delta8GummiesHeader)) {
					highLighterMethod(products_Delta8GummiesHeader);
					testStepInfo("Delta-8 Gummies page was opened successfully");
				} else {
					testStepFailed("Delta-8 Gummies page could not be opened successfully",
							"Delta-8 Gummies Header was not displayed");
				}
				break;
			}

			case "Delta-8 Vape": {
				if (isElementDisplayed(products_Delta8VapeHeader)) {
					highLighterMethod(products_Delta8VapeHeader);
					testStepInfo("Delta-8 Vape page was opened successfully");
				} else {
					testStepFailed("Delta-8 Vape page could not be opened successfully",
							"Delta-8 Vape Header was not displayed");
				}
				break;
			}

			case "Delta-8 Oils": {
				if (isElementDisplayed(products_Delta8OilsHeader)) {
					highLighterMethod(products_Delta8OilsHeader);
					testStepInfo("Delta-8 Oils page was opened successfully");
				} else {
					testStepFailed("Delta-8 Oils page could not be opened successfully",
							"Delta-8 Oils Header was not displayed");
				}
				break;
			}

			case "Trifecta Products": {
				if (isElementDisplayed(products_TrifectaHeader)) {
					highLighterMethod(products_TrifectaHeader);
					testStepInfo("Trifecta Products page was opened successfully");
				} else {
					testStepFailed("Trifecta Products page could not be opened successfully",
							"Trifecta Products Header was not displayed");
				}
				break;
			}

			case "Delta 8 Pain Rub": {
				if (isElementDisplayed(products_Delta8PainRubHeader)) {
					highLighterMethod(products_Delta8PainRubHeader);
					testStepInfo("Delta-8 Pain Rub page was opened successfully");
				} else {
					testStepFailed("Delta-8 Pain Rub page could not be opened successfully",
							"Delta-8 Pain Rub Header was not displayed");
				}
				break;
			}

			case "About Us": {
				if (isElementDisplayed(aboutUsHeader)) {
					highLighterMethod(aboutUsHeader);
					testStepInfo("About Us page was opened successfully");
				} else {
					testStepFailed("About Us page could not be opened successfully",
							"About Us Header was not displayed");
				}
				break;
			}

			case "FAQ": {
				if (isElementDisplayed(faqHeader)) {
					highLighterMethod(faqHeader);
					testStepInfo("FAQ page was opened successfully");
				} else {
					testStepFailed("FAQ page could not be opened successfully", "FAQ Header was not displayed");
				}
				break;
			}
			case "Wholesale": {
				if (isElementDisplayed(wholesaleHeader)) {
					highLighterMethod(wholesaleHeader);
					testStepInfo("Wholesale page was opened successfully");
				} else {
					testStepFailed("Wholesale page could not be opened successfully",
							"Wholesale Header was not displayed");
				}
				break;
			}

			case "contact": {
				if (isElementDisplayed(contactUsHeader)) {
					highLighterMethod(contactUsHeader);
					testStepInfo("Contact page was opened successfully");
				} else {
					testStepFailed("Contact page could not be opened successfully", "Contact Header was not displayed");
				}
				break;
			}

			case "shipping": {
				if (isElementDisplayed(shippingPolicyHeader)) {
					highLighterMethod(shippingPolicyHeader);
					testStepInfo("Shipping page was opened successfully");
				} else {
					testStepFailed("Shipping page could not be opened successfully",
							"Shipping Header was not displayed");
				}
				break;
			}

			case "refund": {
				if (isElementDisplayed(refundPolicyHeader)) {
					highLighterMethod(refundPolicyHeader);
					testStepInfo("Refund page was opened successfully");
				} else {
					testStepFailed("Refund page could not be opened successfully", "Refund Header was not displayed");
				}
				break;
			}

			case "terms": {
				if (isElementDisplayed(termsConditionsHeader)) {
					highLighterMethod(termsConditionsHeader);
					testStepInfo("Terms page was opened successfully");
				} else {
					testStepFailed("Terms page could not be opened successfully", "Terms Header was not displayed");
				}
				break;
			}

			case "privacy": {
				if (isElementDisplayed(privacyPolicyHeader)) {
					highLighterMethod(privacyPolicyHeader);
					testStepInfo("Privacy page was opened successfully");
				} else {
					testStepFailed("Privacy page could not be opened successfully", "Privacy Header was not displayed");
				}
				break;
			}

			case "Newsletter": {
				if (isElementDisplayed(newsletterHeader)) {
					highLighterMethod(newsletterHeader);
					testStepInfo("Newsletter page was opened successfully");
				} else {
					testStepFailed("Newsletter page could not be opened successfully",
							"Newsletter Header was not displayed");
				}
				break;
			}

			case "ccpa": {
				switchToLastTab();
				if (isElementDisplayed(ccpaHeader)) {
					highLighterMethod(ccpaHeader);
					testStepInfo("The relevant page was opened upon clicking CCPA in Footer");
					driver.close();
					switchToLastTab();
				} else {
					Set<String> numberOfWindows = driver.getWindowHandles();
					if (numberOfWindows.size() > 1) {
						driver.close();
					}
					switchToLastTab();
					testStepFailed("The relevant page was not opened upon clicking CCPA in Footer",
							"CCPA Header was not displayed");
				}
				break;
			}

			case "instagram": {
				switchToLastTab();
				if (driver.getCurrentUrl().contains("https://www.instagram.com/mysticlabsd8")
						|| driver.getCurrentUrl().contains("https://www.instagram.com/accounts/login")) {
					testStepInfo("Instagram page of Mystic Labs was opened");
					driver.close();
					switchToLastTab();
				} else {
					Set<String> numberOfWindows = driver.getWindowHandles();
					if (numberOfWindows.size() > 1) {
						driver.close();
					}
					switchToLastTab();
				}
				break;
			}

			case "LabTests": {
				if (isElementDisplayed(labTestsHeader)) {
					highLighterMethod(labTestsHeader);
					testStepInfo("Lab Tests page was opened successfully");
				} else {
					testStepFailed("Lab Tests page could not be opened successfully",
							"Lab Tests Header was not displayed");
				}
				break;
			}

//			case "twitter": {
//				switchToLastTab();
//				if (driver.getCurrentUrl().contains("https://twitter.com/HyperBrainIQ")) {
//					testStepInfo("Twitter page of Hyper Brain was opened");
//					driver.close();
//					switchToLastTab();
//				} else {
//					Set<String> numberOfWindows = driver.getWindowHandles();
//					if (numberOfWindows.size() > 1) {
//						driver.close();
//					}
//					switchToLastTab();
//					testStepFailed("Twitter page of Hyper Brain could not be opened");
//				}
//				break;
//			}

			case "noSearchResults Message": {
				if (isElementDisplayed(noSearchResults_Message_SearchPage)) {
					highLighterMethod(noSearchResults_Message_SearchPage);
					testStepInfo("No Search Results Message was displayed");
				} else {
					testStepFailed("No Search Results Message was not displayed");
				}
				break;
			}

			case "Minimum Query Length": {
				if (isElementDisplayed(searchButton)) {
					String attribute = getAttributeValue(searchButton, "disabled");
					if (attribute.contains("true")) {
						highLighterMethod(searchButton);
						testStepInfo("Search button was disabled due to Minimum Query Length");
					}
				} else {
					testStepFailed("Minimum Query Length Message was not displayed");
				}
				break;
			}

			case "Number of Items": {
				if (isElementDisplayed(numberOfItems_SearchPage)) {
					highLighterMethod(numberOfItems_SearchPage);
					testStepInfo("A valid search was performed successfully");
				} else {
					testStepFailed("A valid search could not be performed", "Number of items is not displayed");
				}
				break;
			}

			case "Gummies Search Product Title": {
				if (isElementDisplayed(gummiesSearchProductTitle_SearchPage)) {
					scrollToViewElement(gummiesSearchProductTitle_SearchPage);
					highLighterMethod(gummiesSearchProductTitle_SearchPage);
					testStepInfo("Valid Product was displayed for the search done");
				} else {
					testStepFailed("Valid Product was not displayed for the search done");
				}
				break;
			}

			case "RegisterPage": {
				if (isElementDisplayed(createAnAccountButton)) {
					highLighterMethod(createAnAccountButton);
					testStepInfo("Create Account page opened successfully");
				} else {
					testStepFailed("Create Account page could not be opened");
				}
				break;
			}

			case "SignInPage": {
				if (isElementDisplayed(signInButton)) {
					highLighterMethod(signInButton);
					testStepInfo("Sign In page opened successfully");
				} else {
					testStepFailed("Sign In page could not be opened");
				}
				break;
			}
			}
		} catch (Exception e) {
			testStepFailed("Relevant verification could not be done successfully");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method is used to navigate through Footer.
	 */
	public void navigateFooter(String option) {

		try {
			switch (option) {

			case "instagram": {
				if (isElementPresent(instagramFooterLink)) {
					highLighterMethod(instagramFooterLink);
					clickOn(instagramFooterLink);
				} else {
					testStepFailed("Instagram Icon not displayed in Footer");
				}
				break;
			}

//			case "twitter": {
//				if (isElementPresent(twitterFooterLink)) {
//					highLighterMethod(twitterFooterLink);
//					clickOn(twitterFooterLink);
//				} else {
//					testStepFailed("Twitter Icon not displayed in Footer");
//				}
//				break;
//			}

			case "home": {
				if (isElementDisplayed(homeFooterLink)) {
					highLighterMethod(homeFooterLink);
					clickOn(homeFooterLink);
				} else {
					testStepFailed("Home not displayed in Footer");
				}
				break;
			}

			case "about": {
				if (isElementPresent(aboutFooterLink)) {
					highLighterMethod(aboutFooterLink);
					clickOn(aboutFooterLink);
				} else {
					testStepFailed("About not displayed in Footer");
				}
				break;
			}

//			case "faq": {
//				if (isElementDisplayed(faqFooterLink)) {
//					highLighterMethod(faqFooterLink);
//					clickOn(faqFooterLink);
//				} else {
//					testStepFailed("Instagram Icon not displayed in Footer");
//				}
//				break;
//			}

			case "Wholesale": {
				if (isElementDisplayed(wholesaleFooterLink)) {
					highLighterMethod(wholesaleFooterLink);
					clickOn(wholesaleFooterLink);
				} else {
					testStepFailed("Wholesale was not displayed in Footer");
				}
				break;
			}

			case "terms": {
				if (isElementPresent(termsFooterLink)) {
					highLighterMethod(termsFooterLink);
					clickOn(termsFooterLink);
				} else {
					testStepFailed("Terms not displayed in Footer");
				}
				break;
			}

			case "privacy": {
				if (isElementPresent(privacyFooterLink)) {
					highLighterMethod(privacyFooterLink);
					clickOn(privacyFooterLink);
				} else {
					testStepFailed("Privacy not displayed in Footer");
				}
				break;
			}

			case "shipping": {
				if (isElementPresent(shippingFooterLink)) {
					highLighterMethod(shippingFooterLink);
					clickOn(shippingFooterLink);
				} else {
					testStepFailed("Shipping not displayed in Footer");
				}
				break;
			}

			case "returns": {
				if (isElementPresent(returnsFooterLink)) {
					highLighterMethod(returnsFooterLink);
					clickOn(returnsFooterLink);
				} else {
					testStepFailed("Returns not displayed in Footer");
				}
				break;
			}

			case "newsletter": {
				scrollToViewElement(newsletterFooterLink);
				waitForElementToDisplay(newsletterFooterLink, 30);
				if (isElementDisplayed(newsletterFooterLink)) {
					highLighterMethod(newsletterFooterLink);
					clickOn(newsletterFooterLink);
				} else {
					testStepFailed("Refund not displayed in Footer");
				}
				break;
			}

//			case "contact": {
//				if (isElementPresent(contactFooterLink)) {
//					highLighterMethod(contactFooterLink);
//					clickOn(contactFooterLink);
//				} else {
//					testStepFailed("Contact not displayed in Footer");
//				}
//				break;
//			}

			case "labTesting": {
				if (isElementPresent(labTestingFooterLink)) {
					highLighterMethod(labTestingFooterLink);
					clickOn(labTestingFooterLink);
				} else {
					testStepFailed("LAB TESTING not displayed in Footer");
				}
				break;
			}

			case "ccpa": {
				if (isElementPresent(ccpaFooterLink)) {
					highLighterMethod(ccpaFooterLink);
					clickOn(ccpaFooterLink);
				} else {
					testStepFailed("CCPA not displayed in Footer");
				}
				break;
			}

			case "footerLogo": {
				if (isElementDisplayed(logoFooterLink)) {
					highLighterMethod(logoFooterLink);
					clickOn(logoFooterLink);
				} else {
					testStepFailed("Footer Logo was not displayed in Footer");
				}
				break;
			}
			}
		} catch (Exception e) {
			testStepFailed("Requested navigation in footer could not be done");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to navigate the My Account Header Menu.
	 */
	public void navigateMyAccountMenu(String option) {

		try {

			waitForElementToDisplay(myAccountDropdown, 120);
			if (isElementDisplayed(myAccountDropdown)) {
				mouseOver(myAccountDropdown);
				clickOn(myAccountDropdown);
			} else {
				testStepFailed("My Account dropdown is not displayed in the header");
			}
			switch (option) {
			case "myAccount": {
				if (isElementDisplayed(myAccountDropdown_MyAccount)) {
					highLighterMethod(myAccountDropdown_MyAccount);
					clickOn(myAccountDropdown_MyAccount);
				} else {
					testStepFailed("My Account option is not displayed in the dropdown");
				}
				break;
			}

			case "wishList": {
				if (isElementDisplayed(myAccountDropdown_WishList)) {
					highLighterMethod(myAccountDropdown_WishList);
					clickOn(myAccountDropdown_WishList);
				} else {
					testStepFailed("WishList option is not displayed in the dropdown");
				}
				break;
			}

			case "signOut": {
				if (isElementDisplayed(myAccountDropdown_SignOut)) {
					highLighterMethod(myAccountDropdown_SignOut);
					clickOn(myAccountDropdown_SignOut);
					GOR.loggedIn = false;
				} else {
					testStepFailed("Sign Out option is not displayed in the dropdown");
				}
				break;
			}
			}
		} catch (Exception e) {
			testStepFailed("Requested option could not be selected from the My Account dropdown");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to accept the age restriction pop-up.
	 */
	public void clickOver21Age() {

		try {
			waitForElementToDisplay(over21AgeButton, 120);
			if (isElementDisplayed(over21AgeButton)) {
				highLighterMethod(over21AgeButton);
				clickOn(over21AgeButton);
				GOR.agePopUpHandled = true;
			} else {
				testStepFailed("Over 21 Age button not present in Header Menu");
			}
		} catch (Exception e) {
			testStepFailed("Over 21 Age button could not be clicked successfully");
			e.printStackTrace();
		}
	}

}
