package pages;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;

public class Cart_AllProducts extends ApplicationKeywords {

	private static final String getAllProducts_gummies = "All Products - Gummies #xpath=//ol[contains(@class,'list')]//a[contains(@href,'gummies')]//span[contains(@class,'product-image-container')]";
	private static final String getAllProducts_Notgummies_withoutdropdown = "All Products Not Gummies - Withoutdropdown #xpath=//div[contains(@id,'product')]/a[not(contains(@href,'gummies')) and not(contains(@href,'cart')) ]//span[contains(@class,'product-image-container')]";
	private static final String title_FirstAvailableProduct_Gummies_PGP = "Title of First Available Gummies Product - PGP #xpath=//strong//a[contains(@href,'gummies') and contains(@class,'product-item')]";
	private static final String title_FirstAvailableProduct_WithoutDropdown_PGP = "Title of First Available Product Without dropdown - PGP #xpath=//strong//a[not(contains(@href,'gummies')) and not(contains(@href,'carts')) and contains(@class,'product-item')]";
	private static final String addToCart = "Add To Cart #xpath=//span[contains(text(),'Add to Cart')]/..";
	private static final String addToComparisonList = "Add To Comparison List #xpath=//a[contains(@class,'tocompare')]";
	private static final String addToWishList = "Add To Wish List #xpath=//a[contains(@class,'towishlist')]";
	private static final String addToWishList_PGP = "Add To Wish List - PGP #xpath=//div[contains(@id,'product')]/a[not(contains(@href,'gummies')) and not(contains(@href,'cart')) ]//span[contains(@class,'product-image-container')]/../..//a[contains(@class,'towishlist')]";
	private static final String shoppingCartLink = "Shopping Cart Link #xpath=//a[contains(text(),'shopping cart')]";
	private static final String title_selectedProduct_PDP = "Title of product selected - PDP #xpath=//h1[contains(@class,'title')]";
	private static final String quantityUpdateBox = "Quantity Update Box #xpath=//div[contains(@class,'control')]/input[@type='number']";
	private static final String productPrice = "Product Price #xpath=//span[contains(@id,'product-price')]/span";
	private static final String comparsionListLink = "Comparison List Link #xpath=//a[contains(text(),'comparison list')]";
	private static final String productAddedtoCartMessage = "Add To Cart Success Message #xpath=//div/div[contains(text(),'added')]";
	private static final String gummyFlavorDropdown = "Gummy Flavor Dropdown #xpath=//div/select[@id='attribute662']";
	private static final String addToCartForm = "Add To Cart Form #xpath=//form[contains(@id,'cart')]";
	// Excluded Delta-8 Vape Cart from Vape products to avoid drop down for overall
	// convenience in running the script.
	private static final String vapeProducts = "Vape Products #xpath=//strong//a[contains(@class,'product-item') and contains(text(),'Vape') and not(contains(text(),'Delta-8 Vape Cart'))]";
	private static final String shoppingOptionHeading = "Shopping Option Heading #xpath=//div/strong[contains(text(),'Shopping Options')]";
	private static final String closefreeProductOfferPopUp = "Free Product Offer Pop up #xpath=//aside[contains(@class,'main')]//header/button[contains(@class,close)]";
	private static final String closenewsletterOfferPopUp = "Newsletter Product Offer Pop up #xpath=//div[contains(@class,'click')]//button[contains(@class,'close')]";

	public Cart_AllProducts(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method to select the first available gummies product on the page
	 */
	public void selectFirstAvailableGummiesProduct() {
		try {
			int index = 0;
			waitForElement(getAllProducts_gummies, 10);
			scrollToViewElement(shoppingOptionHeading);
			if (isElementDisplayed(getAllProducts_gummies)) {
				java.util.List<WebElement> allProducts = new ArrayList<WebElement>();
				allProducts = findWebElements(getAllProducts_gummies);
				for (int i = 1; i <= allProducts.size(); i++) {
					String checkAvailability = "All Available Products - Gummies #xpath=" + "("
							+ getAllProducts_gummies.split("#xpath=")[1] + ")[" + i
							+ "]/../..//form[@data-role='tocart-form']";
					if (findWebElements(checkAvailability).size() > 0) {
						index = i - 1;
						break;
					}
				}
				scrollToViewElement(allProducts.get(index));
				waitTime(3);
				allProducts.get(index).click();
				testStepInfo("The first available gummies product is clicked");
			} else {
				testStepFailed("Products were not displayed");
			}
		} catch (Exception e) {
			testStepFailed("First available gummies Product could not be selected");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to select the first vape product on the page
	 */
	public void selectFirstVapeProduct() {
		try {
			waitForElementToDisplay(vapeProducts, 10);
			if (isElementDisplayed(vapeProducts)) {
				java.util.List<WebElement> allProducts = new ArrayList<WebElement>();
				allProducts = findWebElements(vapeProducts);
				scrollToViewElement(allProducts.get(0));
				waitTime(3);
				allProducts.get(0).click();
				testStepInfo("The first vape product is clicked");
			} else {
				testStepFailed("Vape Products were not displayed");
			}
		} catch (Exception e) {
			testStepFailed("First vape Product could not be selected");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to get product name of First Gummies product in PGP Page
	 */
	public String getProductNameFirstGummiesProduct_PGP() {
		try {
			int index = 0;
			if (isElementPresent(title_FirstAvailableProduct_Gummies_PGP)) {
				java.util.List<WebElement> productNames = new ArrayList<WebElement>();
				java.util.List<WebElement> allProducts = new ArrayList<WebElement>();
				productNames = findWebElements(title_FirstAvailableProduct_Gummies_PGP);
				allProducts = findWebElements(getAllProducts_gummies);
				for (int i = 1; i <= allProducts.size(); i++) {
					String checkAvailability = "All Available Products - Gummies #xpath=" + "("
							+ getAllProducts_gummies.split("#xpath=")[1] + ")[" + i
							+ "]/../..//form[@data-role='tocart-form']";
					if (findWebElements(checkAvailability).size() > 0) {
						index = i - 1;
						break;
					}
				}
				scrollToViewElement(allProducts.get(index));
				if (isElementDisplayed(title_FirstAvailableProduct_Gummies_PGP)) {
					String value = productNames.get(index).getText();
					return value;
				} else {
					testStepFailed("Could not get the product name of a gummies product from PGP Page",
							"Element not displayed");
				}
			} else {
				testStepFailed("Could not get the product name of a gummies product from PGP Page",
						"Element not present");
			}
		} catch (Exception e) {
			testStepFailed("Could not get the product name from PGP Page");
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Description: Method to get product name of First product without dropdown in
	 * PGP Page
	 */
	public String getProductNameFirstProductWithoutDropdown_PGP() {
		try {
			int index = 0;
			if (isElementPresent(getAllProducts_Notgummies_withoutdropdown)) {
				java.util.List<WebElement> productNames = new ArrayList<WebElement>();
				java.util.List<WebElement> allProducts = new ArrayList<WebElement>();
				productNames = findWebElements(getAllProducts_Notgummies_withoutdropdown);
				allProducts = findWebElements(getAllProducts_Notgummies_withoutdropdown);
				for (int i = 1; i <= allProducts.size(); i++) {
					String checkAvailability = "All Available Products - Gummies #xpath=" + "("
							+ getAllProducts_Notgummies_withoutdropdown.split("#xpath=")[1] + ")[" + i
							+ "]/../..//form[@data-role='tocart-form']";
					if (findWebElements(checkAvailability).size() > 0) {
						index = i - 1;
						break;
					}
				}
				scrollToViewElement(allProducts.get(index));
				if (isElementDisplayed(title_FirstAvailableProduct_WithoutDropdown_PGP)) {
					String value = productNames.get(index).getText();
					return value;
				} else {
					testStepFailed("Could not get the product name of a gummies product from PGP Page",
							"Element not displayed");
				}
			} else {
				testStepFailed("Could not get the product name of a gummies product from PGP Page",
						"Element not present");
			}
		} catch (Exception e) {
			testStepFailed("Could not get the product name from PGP Page");
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Description: Method to get product name of First Non-Gummies product in PGP
	 * Page (Excluded Delta-8 Vape Cart is excluded due to drop down to add that
	 * product)
	 */
	public String getProductNameFirstNonGummiesProduct_PGP() {
		try {
			int index = 0;
			if (isElementPresent(getAllProducts_Notgummies_withoutdropdown)) {
				java.util.List<WebElement> productNames = new ArrayList<WebElement>();
				java.util.List<WebElement> allProducts = new ArrayList<WebElement>();
				productNames = findWebElements(getAllProducts_Notgummies_withoutdropdown);
				allProducts = findWebElements(getAllProducts_Notgummies_withoutdropdown);
				for (int i = 1; i <= allProducts.size(); i++) {
					String checkAvailability = "All Available Products  - Non-Gummies #xpath=" + "("
							+ getAllProducts_Notgummies_withoutdropdown.split("#xpath=")[1] + ")[" + i
							+ "]/../..//form[@data-role='tocart-form']";
					if (findWebElements(checkAvailability).size() > 0) {
						index = i - 1;
						break;
					}
				}
				scrollToViewElement(allProducts.get(index));
				if (isElementDisplayed(getAllProducts_Notgummies_withoutdropdown)) {
					String value = productNames.get(index).getText();
					return value;
				} else {
					testStepFailed("Could not get the product name of a non-gummies product from PGP Page",
							"Element not displayed");
				}
			} else {
				testStepFailed("Could not get the product name of a non-gummies product from PGP Page",
						"Element not present");
			}
		} catch (Exception e) {
			testStepFailed("Could not get the product name from PGP Page");
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Description: Method to select a non-gummies product on the page.
	 */
	public void selectSecondProduct_NonGummies() {
		try {
			waitForElementToDisplay(getAllProducts_Notgummies_withoutdropdown, 10);
			if (isElementDisplayed(getAllProducts_Notgummies_withoutdropdown)) {
				java.util.List<WebElement> allProducts = new ArrayList<WebElement>();
				allProducts = findWebElements(getAllProducts_Notgummies_withoutdropdown);
				scrollToViewElement(allProducts.get(1));
				waitTime(3);
				allProducts.get(1).click();
				testStepInfo("A non-Gummmies product is clicked");
			} else {
				testStepFailed("non-Gummmies were not displayed");
			}
		} catch (Exception e) {
			testStepFailed("non-Gummmies Product could not be selected");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to select a gummies product on the page.
	 */
	public void selectSecondProduct_Gummies() {
		try {
			waitForElementToDisplay(getAllProducts_gummies, 10);
			if (isElementDisplayed(getAllProducts_gummies)) {
				java.util.List<WebElement> allProducts = new ArrayList<WebElement>();
				allProducts = findWebElements(getAllProducts_gummies);
				scrollToViewElement(allProducts.get(1));
				waitTime(3);
				allProducts.get(1).click();
				testStepInfo("A Gummmies product is clicked");
			} else {
				testStepFailed("Gummmies were not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Gummmies Product could not be selected");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Add to Cart
	 */
	public void clickAddToCart() {
		try {
			if (isElementDisplayed(addToCart)) {
				highLighterMethod(addToCart);
				clickOn(addToCart);
				waitForElementToDisplay(productAddedtoCartMessage, 120);
				testStepInfo("The add to cart button was clicked");
			} else {
				testStepFailed("The add to cart button was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Add to cart could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Add to Cart for the first Non-Gummies product
	 * in PGP Page. (Excluded Delta-8 Vape Cart is excluded due to drop down to add
	 * that product)
	 */
	public void clickAddToCartNonGummies_PGP() {
		try {
			String addToCart_NonGummies = "AddToCart_NonGummies #xpath="
					+ getAllProducts_Notgummies_withoutdropdown.split("xpath=")[1] + "/../.."
					+ addToCart.split("xpath=")[1];
			if (isElementDisplayed(getAllProducts_Notgummies_withoutdropdown)) {
				java.util.List<WebElement> allProducts = new ArrayList<WebElement>();
				allProducts = findWebElements(getAllProducts_Notgummies_withoutdropdown);

				Actions action = new Actions(driver);
				WebElement target = allProducts.get(0);
				action.moveToElement(target).perform();
				if (isElementDisplayed(addToCart_NonGummies)) {
					java.util.List<WebElement> addToCart_NonGummies_List = new ArrayList<WebElement>();
					addToCart_NonGummies_List = findWebElements(addToCart_NonGummies);
					highLighterMethod(addToCart_NonGummies_List.get(0));
					addToCart_NonGummies_List.get(0).click();
					testStepInfo("The add to cart button of a Non-Gummies product in PGP Page was clicked");

				} else {
					testStepFailed("The add to cart button of a Non-Gummies product in PGP Page was not displayed");
				}
			}
		} catch (Exception e) {
			testStepFailed("Add to cart of a Non-Gummies product could not be clicked in the PGP Page");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Add to Comparison List in PGP Page for first
	 * Gummies product.
	 */
	public void clickAddToComparisonList_FirstAvailableGummies_PGP() {
		try {
			int index = 0;
			if (isElementDisplayed(getAllProducts_gummies)) {
				java.util.List<WebElement> allProducts = new ArrayList<WebElement>();
				allProducts = findWebElements(getAllProducts_gummies);
				for (int i = 1; i <= allProducts.size(); i++) {
					String checkAvailability = "All Available Products - Gummies #xpath=" + "("
							+ getAllProducts_gummies.split("#xpath=")[1] + ")[" + i
							+ "]/../..//form[@data-role='tocart-form']";
					if (findWebElements(checkAvailability).size() > 0) {
						index = i - 1;
						break;
					}
				}
				Actions action = new Actions(driver);
				WebElement target = allProducts.get(index);
				action.moveToElement(target).perform();
				if (isElementDisplayed(addToComparisonList)) {
					java.util.List<WebElement> allProducts_addToCart = new ArrayList<WebElement>();
					allProducts_addToCart = findWebElements(addToComparisonList);
					highLighterMethod(allProducts_addToCart.get(index));
					allProducts_addToCart.get(index).click();
					testStepInfo("The add to Comparison List button of a product in PGP Page was clicked");

				} else {
					testStepFailed("The add to Comparison List button of a product in PGP Page was not displayed");
				}
			}
		} catch (Exception e) {
			testStepFailed("Add to Comparison List - could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Add to Wish List in PGP Page for first
	 * product without dropdown.
	 */
	public void clickAddToWishList_PGP_WithoutDropdown() {
		try {
			if (isElementDisplayed(getAllProducts_Notgummies_withoutdropdown)) {
				java.util.List<WebElement> allProducts = new ArrayList<WebElement>();
				allProducts = findWebElements(getAllProducts_Notgummies_withoutdropdown);
				java.util.List<WebElement> allProducts_addToCart = new ArrayList<WebElement>();
				allProducts_addToCart = findWebElements(addToWishList_PGP);
				Actions action = new Actions(driver);
				WebElement target = allProducts.get(0);
				action.moveToElement(target).perform();
				if (isElementPresent(addToWishList)) {
					highLighterMethod(allProducts_addToCart.get(0));
					allProducts_addToCart.get(0).click();
					testStepInfo("The add to Wish List button for a product without dropdown in PGP Page was clicked");

				} else {
					testStepFailed(
							"The add to Wish List button for a product without dropdown in PGP Page was not displayed");
				}
			}
		} catch (Exception e) {
			testStepFailed("Add to Wish List - could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Add to Comparison List in PDP Page for first
	 * product.
	 */
	public void clickAddToComparisonList_PDP() {
		try {
			if (isElementDisplayed(addToComparisonList)) {
				highLighterMethod(addToComparisonList);
				clickOn(addToComparisonList);
			} else {
				testStepFailed("The add to Comparison List button of a product in PDP Page was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Add to Comparison List - could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Add to Wish List in PDP Page for first
	 * product.
	 */
	public void clickAddToWishList_PDP() {
		try {
			if (isElementDisplayed(addToWishList)) {
				highLighterMethod(addToWishList);
				clickOn(addToWishList);
			} else {
				testStepFailed("The add to Wish List button of a product in PDP Page was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Add to Wish List - could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on the Shopping Cart Link
	 */
	public void clickShoppingCartLink() {
		try {
			if (isElementDisplayed(shoppingCartLink)) {
				highLighterMethod(shoppingCartLink);
				clickOn(shoppingCartLink);
				testStepInfo("The Shopping Cart Link was clicked");
				GOR.productAdded = true;
			} else {
				testStepFailed("The Shopping Cart Link was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Shopping Cart Link could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on the Shopping Cart Link
	 */
	public void clickWishListLink() {
		try {
			if (isElementDisplayed(shoppingCartLink)) {
				highLighterMethod(shoppingCartLink);
				clickOn(shoppingCartLink);
				testStepInfo("The Shopping Cart Link was clicked");
			} else {
				testStepFailed("The Shopping Cart Link was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Shopping Cart Link could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to get title of the product after selecting it from PGP
	 * Page.
	 */
	public String getProductTitle_PDP() {
		try {
			if (isElementDisplayed(title_selectedProduct_PDP)) {
				highLighterMethod(title_selectedProduct_PDP);
				return getText(title_selectedProduct_PDP);
			} else {
				testStepFailed("Product Title of the selected product not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not get the product title");
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * Description: Method to update product quantity.
	 */
	public void updateProductQuantity(int quantity) {
		try {
			if (isElementDisplayed(quantityUpdateBox)) {
				highLighterMethod(quantityUpdateBox);
				typeIn(quantityUpdateBox, String.valueOf(quantity));
			} else {
				testStepFailed("Could not update the quantity of the product in the PDP page",
						"Quantity update box not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not update the quantity of the product in the PDP page");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to get product price.
	 */
	public float getProductPrice() {
		try {
			waitForElementToDisplay(productPrice, 120);
			if (isElementDisplayed(productPrice)) {
				highLighterMethod(productPrice);
				String price = getText(productPrice).substring(1);
				float productPrice = Float.parseFloat(price);
				return productPrice;
			} else {
				testStepFailed("Could not get the price of the product in the PDP page", "Product Price not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not get the price of the product in the PDP page");
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * Description: Method to click on the Comparison List Link
	 */
	public void clickComparisonListLink() {
		try {
			waitForElementToDisplay(comparsionListLink, 30);
			if (isElementDisplayed(comparsionListLink)) {
				highLighterMethod(comparsionListLink);
				clickOn(comparsionListLink);
				testStepInfo("The Comparison List Link was clicked");
			} else {
				testStepFailed("The Comparison List Link was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Comparison List Link could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to choose Gummy flavor.
	 */
	public void chooseFromGummyFlavorDropdown() {
		try {
			java.util.List<WebElement> addToCartForm_child = new ArrayList<WebElement>();
			WebElement productOptions = findWebElement(Cart_AllProducts.addToCartForm);
			addToCartForm_child = productOptions.findElements(By.xpath(".//select"));
			if (addToCartForm_child.size() > 0) {
				int i = 2;
				String value = "true";
				if (isElementDisplayed(gummyFlavorDropdown)) {
					// To avoid disbaled options
					while (value != null) {
						String gummyFlavor_Enabled = gummyFlavorDropdown + "/option[" + i + "]";
						value = getAttributeValue(gummyFlavor_Enabled, "disabled");
						if (value != null)
							i++;
					}
					highLighterMethod(gummyFlavorDropdown);
					selectFromDropdown(gummyFlavorDropdown, i - 1);
				} else {
					testStepInfo("The Gummy Flavor Dropdown was not displayed");
				}
			} else {
				testStepInfo("No Dropdown to handle");
			}
		} catch (Exception e) {
			testStepFailed("Gummy Flavor Dropdown could not be accessed");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to handle the Newsletter offer pop-up
	 */
	public void closeNewsletterOfferPopup() {
		try {
			if (GOR.OfferPopUpHandled == false) {
				waitForElementToDisplay(closenewsletterOfferPopUp, 10);
				if (isElementDisplayed(closenewsletterOfferPopUp)) {
					GOR.OfferPopUpHandled = true;
					highLighterMethod(closenewsletterOfferPopUp);
					clickOn(closenewsletterOfferPopUp);
				} else {
					testStepInfo("Newsletter Offer pop up not present");
				}
			} else
				testStepInfo("Newsletter Offer Popup already handled");
		} catch (Exception e) {
			testStepInfo("Newsletter Offer pop up not present");
		}
	}

	/**
	 * Description: Method to handle the Free Product offer pop-up
	 */
	public void closeFreeProductOfferPopup() {
		try {
			if (GOR.freeProductOfferPopUpHandled == false) {
				if (isElementDisplayed(closefreeProductOfferPopUp)) {
					GOR.freeProductOfferPopUpHandled = true;
					highLighterMethod(closefreeProductOfferPopUp);
					clickOn(closefreeProductOfferPopUp);
				} else
					testStepInfo("Free Product Offer Popup already handled");
			}
		} catch (Exception e) {
			testStepInfo("Free Product Offer pop up not present");
		}
	}
}