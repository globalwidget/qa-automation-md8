package pages;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;

public class MyAccount extends ApplicationKeywords {

	private static final String newsLetterSubscribedMessage = "Newsletter Subscribed Message #xpath=// p[contains(text(),'You are subscribed to \"General Subscription\"')]";
	private static final String myWishList = "My Wish List #xpath=//ul[@class='nav items']//a[contains(text(),'Wish List')]";
	private static final String myAccount = "My Account #xpath=//li/strong[contains(text(),'My Account')]";
	private static final String myAccount_contactEdit = "My Account #xpath=//a[contains(@href,'account/edit')]//span[contains(text(),'Edit')]";
	private static final String myAccount_contact_firstName = "Contact Information - First Name #xpath=//input[@id='firstname']";
	private static final String myAccount_contact_lastName = "Contact Information - Last Name #xpath=//input[@id='lastname']";
	private static final String saveButton = "Contact Information - Save #xpath=//button/span[contains(text(),'Save')]";
	private static final String myAccount_ChangePassword = "My Account - Change Password #xpath=//a[contains(text(),'Change Password')]";
	private static final String myAccount_ChangePassword_CurrentPassword = "Current Password #xpath=//input[@id='current-password']";
	private static final String myAccount_ChangePassword_NewPassword = "New Password #xpath=//input[@id='password']";
	private static final String myAccount_ChangePassword_ConfirmNewPassword = "Confirm New Password #xpath=//input[@id='password-confirmation']";
	private static final String myAccount_Email = "My Account - Change Email #xpath=//div/input[@id='change-email']";
	private static final String myAccount_EmailTextBox = "My Account - Email TextBox #xpath=//div/input[@id='email']";
	private static final String myAccount_newsletterSubscription = "My Account - Newsletter Subscriptions #xpath=//li/a[contains(text(),'Newsletter Subscriptions')]";
	private static final String newsletter_generalSubscription_checkbox = "Newsletter Subscription - Genral Subscription Checkbox #xpath=//div/input[@id='subscription']";

	public MyAccount(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method to verify presence of expected Message.
	 */
	public void verifyMessage_MyAccount(String option) {
		try {
			switch (option) {
			case "newsLetterSubscribedMessage": {
				if (isElementDisplayed(newsLetterSubscribedMessage)) {
					highLighterMethod(newsLetterSubscribedMessage);
					testStepInfo("Newsletter Subcribed Message was displayed in the My Account page");
				} else {
					testStepFailed("Newsletter Subcribed Message was not displayed in the My Account page");
				}
				break;
			}
			}
		} catch (Exception e) {
			testStepFailed("The relevant message verification could not be done.");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on My Wish List.
	 */
	public void clickOnmyWishList() {
		try {
			if (isElementDisplayed(myWishList)) {
				highLighterMethod(myWishList);
				clickOn(myWishList);
				testStepInfo("My Wish List was displayed in the My Account page");
			} else {
				testStepFailed("My Wish List was not displayed in the My Account page");
			}
		} catch (Exception e) {
			testStepFailed("My Wish List could not be clicked.");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on My Account.
	 */
	public void clickOnMyAccount() {
		try {
			if (isElementDisplayed(myAccount)) {
				highLighterMethod(myAccount);
				clickOn(myAccount);
				testStepInfo("My Account was displayed in the My Account page");
			} else {
				testStepFailed("My Account was not displayed in the My Account page");
			}
		} catch (Exception e) {
			testStepFailed("My Account could not be clicked.");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Edit under contact information of My Account.
	 */
	public void clickEditContactInformation() {
		try {
			if (isElementDisplayed(myAccount_contactEdit)) {
				highLighterMethod(myAccount_contactEdit);
				clickOn(myAccount_contactEdit);
				testStepInfo("Edit under contact information was displayed under the My Account");
			} else {
				testStepFailed("Edit under contact information was not displayed under the My Account");
			}
		} catch (Exception e) {
			testStepFailed("Edit under contact information of My Account could not be clicked.");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to enter first Name and last Name under contact
	 * information of My Account.
	 */
	public void enterFirstNameLastName(String firstName, String lastName) {
		try {
			if (isElementDisplayed(myAccount_contact_firstName)) {
				highLighterMethod(myAccount_contact_firstName);
				typeIn(myAccount_contact_firstName, firstName);
			} else {
				testStepFailed("First Name under contact information was not displayed");
			}

			if (isElementDisplayed(myAccount_contact_lastName)) {
				highLighterMethod(myAccount_contact_lastName);
				typeIn(myAccount_contact_lastName, lastName);
			} else {
				testStepFailed("First Name and Last Name under contact information was not displayed");
			}

		} catch (Exception e) {
			testStepFailed("Could not enter First Name and Last Name under contact information of My Account");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Save.
	 */
	public void clickSave() {
		try {
			if (isElementDisplayed(saveButton)) {
				highLighterMethod(saveButton);
				clickOn(saveButton);
				testStepInfo("Save under contact information was displayed");
			} else {
				testStepFailed("Save under contact information was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Save under contact information could not be clicked.");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Change Password under My Account.
	 */
	public void clickChangePassword() {
		try {
			if (isElementDisplayed(myAccount_ChangePassword)) {
				highLighterMethod(myAccount_ChangePassword);
				clickOn(myAccount_ChangePassword);
				testStepInfo("Change password under My Account was displayed");
			} else {
				testStepFailed("Change password under My Account was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Change password under My Account could not be clicked.");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Change Email under My Account.
	 */
	public void clickChangeEmail() {
		try {
			if (isElementDisplayed(myAccount_Email)) {
				highLighterMethod(myAccount_Email);
				clickOn(myAccount_Email);
				testStepInfo("Change Email under My Account was displayed");
			} else {
				testStepFailed("Change Email under My Account was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Change Email under My Account could not be clicked.");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to enter details for changing email.
	 */
	public void enterDetailsEmail(String email, String currentPassword) {
		try {
			if (isElementDisplayed(myAccount_EmailTextBox)) {
				highLighterMethod(myAccount_EmailTextBox);
				typeIn(myAccount_EmailTextBox, email);
			} else {
				testStepFailed("Email Text Box field was not displayed");
			}

			if (isElementDisplayed(myAccount_ChangePassword_CurrentPassword)) {
				highLighterMethod(myAccount_ChangePassword_CurrentPassword);
				typeIn(myAccount_ChangePassword_CurrentPassword, currentPassword);
			} else {
				testStepFailed("Current Password field was not displayed");
			}

		} catch (Exception e) {
			testStepFailed("Could not enter details for changing email");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to enter old and new password to change the password.
	 */
	public void enterOldAndNewPassword(String oldPassword, String newPassword) {
		try {
			if (isElementDisplayed(myAccount_ChangePassword_CurrentPassword)) {
				highLighterMethod(myAccount_ChangePassword_CurrentPassword);
				typeIn(myAccount_ChangePassword_CurrentPassword, oldPassword);
			} else {
				testStepFailed("Current Password field was not displayed");
			}

			if (isElementDisplayed(myAccount_ChangePassword_NewPassword)) {
				highLighterMethod(myAccount_ChangePassword_NewPassword);
				typeIn(myAccount_ChangePassword_NewPassword, newPassword);
			} else {
				testStepFailed("New Password field was not displayed");
			}

			if (isElementDisplayed(myAccount_ChangePassword_ConfirmNewPassword)) {
				highLighterMethod(myAccount_ChangePassword_ConfirmNewPassword);
				typeIn(myAccount_ChangePassword_ConfirmNewPassword, newPassword);
			} else {
				testStepFailed("Confirm New Password field was not displayed");
			}

		} catch (Exception e) {
			testStepFailed("Could not enter old and new password");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Newsletter Subscription tab.
	 */
	public void clickNewsletterSubscriptionTab() {
		try {
			if (isElementDisplayed(myAccount_newsletterSubscription)) {
				highLighterMethod(myAccount_newsletterSubscription);
				clickOn(myAccount_newsletterSubscription);
			} else {
				testStepFailed("Newsletter Subscription under My Account was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Newsletter Subscription under My Account could not be clicked.");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on General Subscription check box of Newsletter.
	 */
	public void clickGeneralSubscription() {
		try {
			if (isElementDisplayed(newsletter_generalSubscription_checkbox)) {
				highLighterMethod(newsletter_generalSubscription_checkbox);
				clickOn(newsletter_generalSubscription_checkbox);
			} else {
				testStepFailed("General Subscription checkbox under Newsletter was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("General Subscription checkbox under Newsletter could not be clicked.");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify Newsletter is subscribed.
	 */
	public void verifyGeneralSubscriptionChecked() {
		try {
			String value = getAttributeValue(newsletter_generalSubscription_checkbox, "checked");
			if (isElementDisplayed(newsletter_generalSubscription_checkbox)
					&& getAttributeValue(newsletter_generalSubscription_checkbox, "checked").contains("true")) {
				highLighterMethod(newsletter_generalSubscription_checkbox);
				testStepInfo("General Subscription checkbox under Newsletter was checked");
			} else {
				testStepFailed("General Subscription checkbox under Newsletter was not checked");
			}
		} catch (Exception e) {
			testStepFailed("General Subscription checkbox under Newsletter was not checked");
			e.printStackTrace();
		}
	}
}
