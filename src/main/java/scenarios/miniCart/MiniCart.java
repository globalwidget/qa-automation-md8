package scenarios.miniCart;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.Cart_AllProducts;
import pages.Checkout;
import pages.HeaderAndFooters;
import pages.ShoppingCart;

public class MiniCart extends ApplicationKeywords {
	BaseClass obj;
	Cart_AllProducts cart_AllProducts;
	pages.MiniCart miniCart;
	HeaderAndFooters headerAndfooter;
	ShoppingCart shoppingCart;
	Checkout checkout;
	private boolean status = false;

	public MiniCart(BaseClass obj) {
		super(obj);
		this.obj = obj;
		cart_AllProducts = new Cart_AllProducts(obj);
		miniCart = new pages.MiniCart(obj);
		headerAndfooter = new HeaderAndFooters(obj);
		shoppingCart = new ShoppingCart(obj);
		checkout = new Checkout(obj);
	}

	/*
	 * TestCaseid : ML_TS_044 Mini-Cart Description : Verify the mini cart numbers
	 * are getting updated / displayed as per the number of products in cart.
	 */
	public void verifyMiniCartProductNumberDisplayed() {
		try {
			int quantity;
			if (GOR.agePopUpHandled == false) {
				headerAndfooter.clickOver21Age();
			}
			headerAndfooter.goTo_Home();
			headerAndfooter.clickOnMiniCart();
			miniCart.removeAllProducts();
			headerAndfooter.goTo_Products(0);
			cart_AllProducts.selectFirstAvailableGummiesProduct();
			cart_AllProducts.chooseFromGummyFlavorDropdown();
			cart_AllProducts.clickAddToCart();
			if (GOR.freeProductOfferPopUpHandled == false) {
				cart_AllProducts.closeFreeProductOfferPopup();
			}
			quantity = miniCart.getProductQuantityDisplayed();
			if (quantity != 1) {
				testStepFailed("The number products added does not match with that displayed in the Mini Cart");
			}
			headerAndfooter.goTo_Products(0);
			cart_AllProducts.selectSecondProduct_NonGummies();
			cart_AllProducts.updateProductQuantity(3);
			cart_AllProducts.clickAddToCart();
			if (GOR.freeProductOfferPopUpHandled == false) {
				cart_AllProducts.closeFreeProductOfferPopup();
			}
			quantity = miniCart.getProductQuantityDisplayed();
			if (quantity != 4) {
				testStepFailed("The number products added does not match with that displayed in the Mini Cart");
			}
		} catch (Exception e) {
			testStepFailed("The product count updated in the PDP page is not reflected in the Shopping Cart");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || headerAndfooter.testFailure || miniCart.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : ML_TS_045 Description : Verify if the user is able to modify the
	 * quantity of the product in the mini cart.
	 */
	public void modifyQuantityinMiniCart() {
		try {
			int quantity_MiniCart = 10;
			int quantity_shoppingCart;
			if (GOR.agePopUpHandled == false) {
				headerAndfooter.clickOver21Age();
			}
			headerAndfooter.clickOnMiniCart();
			miniCart.removeAllProducts();
			headerAndfooter.goTo_Products(0);
			cart_AllProducts.selectFirstAvailableGummiesProduct();
			cart_AllProducts.chooseFromGummyFlavorDropdown();
			cart_AllProducts.clickAddToCart();
			if (GOR.freeProductOfferPopUpHandled == false) {
				cart_AllProducts.closeFreeProductOfferPopup();
			}
			headerAndfooter.clickOnMiniCart();
			miniCart.updateCartQuantity(quantity_MiniCart);
			miniCart.clickUpdateProductCountIcon();
			miniCart.clickViewAndEditCart();
			quantity_shoppingCart = shoppingCart.getProductQuantity();
			headerAndfooter.goTo_Home();
			if (quantity_shoppingCart == quantity_MiniCart) {
				testStepInfo("The user is able to modify the quantity of the product in the Mini Cart");
			} else {
				testStepFailed("The user is not able to modify the quantity of the product in the Mini Cart");
			}

		} catch (Exception e) {
			testStepFailed("The user is not able to modify the quantity of the product in the Mini Cart");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || headerAndfooter.testFailure || miniCart.testFailure
				|| shoppingCart.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : MiniCart_ML_TS_046_049 Description : Verify if the user is able
	 * to edit the item from mini cart.
	 */
	public void editProductFromMiniCart() {
		try {
			int quantity_MiniCart;
			int quantity_PDP = 9;
			String productName_PDP;
			String productName_MiniCart;
			if (GOR.agePopUpHandled == false) {
				headerAndfooter.clickOver21Age();
			}
			headerAndfooter.goTo_Home();
			headerAndfooter.clickOnMiniCart();
			miniCart.removeAllProducts();
			headerAndfooter.goTo_Products(0);
			cart_AllProducts.selectFirstAvailableGummiesProduct();
			cart_AllProducts.chooseFromGummyFlavorDropdown();
			cart_AllProducts.clickAddToCart();
			if (GOR.freeProductOfferPopUpHandled == false) {
				cart_AllProducts.closeFreeProductOfferPopup();
			}
			headerAndfooter.clickOnMiniCart();
			miniCart.selectFirstProduct();
			productName_PDP = cart_AllProducts.getProductTitle_PDP();
			cart_AllProducts.updateProductQuantity(quantity_PDP);
			cart_AllProducts.chooseFromGummyFlavorDropdown();
			cart_AllProducts.clickAddToCart();
			if (GOR.freeProductOfferPopUpHandled == false) {
				cart_AllProducts.closeFreeProductOfferPopup();
			}
			headerAndfooter.clickOnMiniCart();
			productName_MiniCart = miniCart.getFirstProductTitle();
			if (productName_PDP.contains(productName_MiniCart)) {
				testStepInfo("The product added is present in the Mini Cart");
			} else {
				testStepFailed("The product added is not present in the Mini Cart");
			}
			quantity_MiniCart = quantity_PDP + 1;
			miniCart.checkCartQuantity(quantity_MiniCart);
		} catch (Exception e) {
			testStepFailed("The user is not able to edit the item in the Mini Cart");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || headerAndfooter.testFailure || miniCart.testFailure
				|| shoppingCart.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : MiniCart_ML_TS_047 Description : Verify if the user is able to
	 * delete the product from mini cart.
	 */
	public void verifyDeleteProductFromMiniCart() {
		try {
			String productRemoved;
			if (GOR.agePopUpHandled == false) {
				headerAndfooter.clickOver21Age();
			}
			headerAndfooter.goTo_Home();
			headerAndfooter.clickOnMiniCart();
			miniCart.removeAllProducts();
			headerAndfooter.goTo_Products(0);
			cart_AllProducts.selectFirstAvailableGummiesProduct();
			cart_AllProducts.chooseFromGummyFlavorDropdown();
			cart_AllProducts.clickAddToCart();
			if (GOR.freeProductOfferPopUpHandled == false) {
				cart_AllProducts.closeFreeProductOfferPopup();
			}
			headerAndfooter.clickOnMiniCart();
			productRemoved = miniCart.clickOnRemoveProduct();
			miniCart.verifyEmptyMiniCart();
			testStepInfo("The product " + productRemoved + "was removed from the Mini Cart");
		} catch (Exception e) {
			testStepFailed("Could not verify deleting a product from the Mini Cart");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || headerAndfooter.testFailure || miniCart.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : MiniCart_ML_TS_048 Description : Verify if the cart sub total is
	 * getting updated / displayed as per the action made.
	 */
	public void verifyCartSubTotal() {
		try {
			if (GOR.agePopUpHandled == false) {
				headerAndfooter.clickOver21Age();
			}
			headerAndfooter.goTo_Home();
			headerAndfooter.clickOnMiniCart();
			miniCart.removeAllProducts();
			headerAndfooter.goTo_Products(0);
			cart_AllProducts.selectFirstAvailableGummiesProduct();
			cart_AllProducts.chooseFromGummyFlavorDropdown();
			float firstProductPrice = cart_AllProducts.getProductPrice();

			cart_AllProducts.clickAddToCart();
			if (GOR.freeProductOfferPopUpHandled == false) {
				cart_AllProducts.closeFreeProductOfferPopup();
			}
			waitTime(3);
			headerAndfooter.clickOnMiniCart();
			firstProductPrice = miniCart.verifyProductPriceInMiniCart(firstProductPrice);
			miniCart.verifyCartSubTotalInMiniCart(firstProductPrice);
			headerAndfooter.goTo_Products(0);
			cart_AllProducts.selectSecondProduct_NonGummies();
			float secondProductPrice = cart_AllProducts.getProductPrice();

			cart_AllProducts.clickAddToCart();
			if (GOR.freeProductOfferPopUpHandled == false) {
				cart_AllProducts.closeFreeProductOfferPopup();
			}
			waitTime(4);
			headerAndfooter.clickOnMiniCart();
			secondProductPrice = miniCart.verifyProductPriceInMiniCart(secondProductPrice);
			miniCart.verifyCartSubTotalInMiniCart(firstProductPrice + secondProductPrice);

		} catch (Exception e) {
			testStepFailed("The product count updated in the PDP page is not reflected in the Shopping Cart");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || headerAndfooter.testFailure || miniCart.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : MiniCart_ML_TS_050 Description : Verify clicking on 'View & Edit
	 * Cart' is taking the user to cart page
	 */
	public void verifyViewAndEditCart() {
		try {
			if (GOR.agePopUpHandled == false) {
				headerAndfooter.clickOver21Age();
			}
			headerAndfooter.goTo_Home();
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Products(0);
				cart_AllProducts.selectFirstAvailableGummiesProduct();
				cart_AllProducts.chooseFromGummyFlavorDropdown();
				cart_AllProducts.clickAddToCart();
				if (GOR.freeProductOfferPopUpHandled == false) {
					cart_AllProducts.closeFreeProductOfferPopup();
				}
			}
			headerAndfooter.clickOnMiniCart();
			miniCart.clickViewAndEditCart();
			shoppingCart.verifyShoppingCartHeader();
			headerAndfooter.goTo_Home();
			testStepInfo("The function of View & Edit Cart was verified");

		} catch (Exception e) {
			testStepFailed("The function of View & Edit Cart could not be verified");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || headerAndfooter.testFailure || miniCart.testFailure
				|| shoppingCart.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : MiniCart_ML_TS_051 Description : Verify clicking on 'Go to
	 * checkout' from mini cart is taking the user to checkout page with specific
	 * products
	 */
	public void verifyGoToCheckout() {
		try {
			if (GOR.agePopUpHandled == false) {
				headerAndfooter.clickOver21Age();
			}
			headerAndfooter.goTo_Home();
			headerAndfooter.clickOnMiniCart();
			miniCart.removeAllProducts();
			String productName_PDP;
			headerAndfooter.goTo_Products(0);
			cart_AllProducts.selectFirstAvailableGummiesProduct();
			cart_AllProducts.chooseFromGummyFlavorDropdown();
			productName_PDP = cart_AllProducts.getProductTitle_PDP();
			cart_AllProducts.clickAddToCart();
			if (GOR.freeProductOfferPopUpHandled == false) {
				cart_AllProducts.closeFreeProductOfferPopup();
			}
			headerAndfooter.clickOnMiniCart();
			miniCart.clickGoToCheckout();
			checkout.verifyShippingHeader();
			checkout.expandOrderSummary();
			checkout.checkItemsInCartProductsDisplay();
			checkout.verifyPresenceOfExpectedProduct(productName_PDP);
			headerAndfooter.clickLogo();
			testStepInfo("The function of Go To Checkout was verified");
		} catch (Exception e) {
			testStepFailed("The function of Go To Checkout could not be verified");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || headerAndfooter.testFailure || miniCart.testFailure
				|| checkout.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

}
