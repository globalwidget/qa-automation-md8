package scenarios.LabTests;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.Cart_AllProducts;
import pages.HomePage;

public class LabTests extends ApplicationKeywords {
	BaseClass obj;
	pages.LabTests labTests;
	HomePage homePage;
	pages.HeaderAndFooters headerAndFooters;
	Cart_AllProducts cart_AllProducts;
	private boolean status = false;

	String password;

	public LabTests(BaseClass obj) {
		super(obj);
		this.obj = obj;
		homePage = new HomePage(obj);
		labTests = new pages.LabTests(obj);
		headerAndFooters = new pages.HeaderAndFooters(obj);
		cart_AllProducts = new Cart_AllProducts(obj);
	}

	/*
	 * TestCaseid : Lab Test - Batch Search Description : Verify proper error
	 * message is displayed for the invalid batch search
	 */
	public void verifyInvalidSearch() {
		try {
			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0);
				cart_AllProducts.selectFirstAvailableGummiesProduct();
				cart_AllProducts.closeNewsletterOfferPopup();
			}
			labTests.goTo_LabTests();
			labTests.clickSearchIcon();
			labTests.enterBatchNumber("  ");
			labTests.clickSearchIcon();
			labTests.verifyNullResult();
		} catch (Exception e) {
			testStepFailed("Could not verify lab test results using the invalid batch number");
		}
		if (obj.testFailure || homePage.testFailure || labTests.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Lab Test - Batch Search Description : Verify if the user is able
	 * to search for lab test results using the valid batch number
	 */
	public void verifyValidSearch() {
		try {
			String batchNumber = retrieve("batchNumber");

			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0);
				cart_AllProducts.selectFirstAvailableGummiesProduct();
				cart_AllProducts.closeNewsletterOfferPopup();
			}
			labTests.goTo_LabTests();
			labTests.enterBatchNumber(batchNumber);
			labTests.clickSearchIcon();
			labTests.checkViewResults();
		} catch (Exception e) {
			testStepFailed("Could not verify lab test results using the valid batch number");
		}
		if (obj.testFailure || homePage.testFailure || labTests.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}
}