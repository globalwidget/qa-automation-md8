package scenarios.compareProducts;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.Cart_AllProducts;
import pages.HeaderAndFooters;
import pages.HomePage;
import pages.MiniCart;
import pages.MyAccount;
import pages.ShoppingCart;
import pages.SignIn;
import pages.WishList;

public class CompareProducts extends ApplicationKeywords {
	BaseClass obj;
	HeaderAndFooters headerAndFooters;
	HomePage homepage;
	Cart_AllProducts cartAllProducts;
	ShoppingCart shoppingCart;
	pages.CompareProducts compareProducts;
	WishList wishList;
	SignIn signIn;
	MyAccount myAccount;
	MiniCart miniCart;
	private boolean status = false;

	public CompareProducts(BaseClass obj) {
		super(obj);
		headerAndFooters = new HeaderAndFooters(obj);
		homepage = new HomePage(obj);
		cartAllProducts = new Cart_AllProducts(obj);
		shoppingCart = new ShoppingCart(obj);
		compareProducts = new pages.CompareProducts(obj);
		wishList = new WishList(obj);
		signIn = new SignIn(obj);
		myAccount = new MyAccount(obj);
		miniCart = new MiniCart(obj);
		this.obj = obj;
	}

	/*
	 * TestCaseid : CompareProducts_ML_TS_126 Description : To Verify if the user is
	 * able to add products to comparison list.
	 */
	public void addProductToComparisonList() {
		try {
			String email = retrieve("email");
			String password = retrieve("password");
			String productName;

			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0);
				cartAllProducts.selectFirstAvailableGummiesProduct();
				cartAllProducts.closeNewsletterOfferPopup();
			}

			if (GOR.loggedIn == false) {
				headerAndFooters.goTo_Home();
				headerAndFooters.goToSignIn();
				signIn.signIn(email, password);
			}

			headerAndFooters.goTo_Home();
			headerAndFooters.goTo_Products(0);
			productName = cartAllProducts.getProductNameFirstGummiesProduct_PGP();
			cartAllProducts.clickAddToComparisonList_FirstAvailableGummies_PGP();
			cartAllProducts.clickComparisonListLink();
			compareProducts.verifyPresenceOfExpectedProduct(productName);
			compareProducts.removeAllProducts();

		} catch (Exception e) {
			testStepFailed("Adding product to comparison list could not be verified");
		}
		if (obj.testFailure || headerAndFooters.testFailure || homepage.testFailure || cartAllProducts.testFailure
				|| compareProducts.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : CompareProducts_ML_TS_127 Description : To Verify if the user is
	 * able to add products to comparison list.
	 */
	public void removeProductFromComparisonList() {
		try {
			String email = retrieve("email");
			String password = retrieve("password");
			String productName;

			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0);
				cartAllProducts.selectFirstAvailableGummiesProduct();
				cartAllProducts.closeNewsletterOfferPopup();
			}

			if (GOR.loggedIn == false) {
				headerAndFooters.goTo_Home();
				headerAndFooters.goToSignIn();
				signIn.signIn(email, password);
			}

			headerAndFooters.goTo_Home();
			headerAndFooters.goTo_Products(0);
			cartAllProducts.selectFirstAvailableGummiesProduct();
			productName = cartAllProducts.getProductTitle_PDP();
			cartAllProducts.clickAddToComparisonList_PDP();
			cartAllProducts.clickComparisonListLink();
			compareProducts.verifyPresenceOfExpectedProduct(productName);
			compareProducts.clickRemoveProduct();

		} catch (Exception e) {
			testStepFailed("Removing product from comparison list could not be verified");
		}
		if (obj.testFailure || headerAndFooters.testFailure || homepage.testFailure || cartAllProducts.testFailure
				|| compareProducts.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : CompareProducts_ML_TS_128 Description : To Verify if the user
	 * can clear all from the Products from comparison list.
	 */
	public void clearComparisonList() {
		try {
			String email = retrieve("email");
			String password = retrieve("password");

			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0);
				cartAllProducts.selectFirstAvailableGummiesProduct();
				cartAllProducts.closeNewsletterOfferPopup();
			}

			if (GOR.loggedIn == false) {
				headerAndFooters.goTo_Home();
				headerAndFooters.goToSignIn();
				signIn.signIn(email, password);
			}

			headerAndFooters.goTo_Home();
			headerAndFooters.goTo_Products(0);
			cartAllProducts.clickAddToComparisonList_FirstAvailableGummies_PGP();
			cartAllProducts.selectSecondProduct_NonGummies();
			cartAllProducts.clickAddToComparisonList_PDP();
			cartAllProducts.clickComparisonListLink();
			compareProducts.removeAllProducts();

		} catch (Exception e) {
			testStepFailed("Clearing comparison list could not be verified");
		}
		if (obj.testFailure || headerAndFooters.testFailure || homepage.testFailure || cartAllProducts.testFailure
				|| compareProducts.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

}
