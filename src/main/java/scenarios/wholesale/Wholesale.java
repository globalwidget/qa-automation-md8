package scenarios.wholesale;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.Wholesale_BecomeDistributor;
import pages.Cart_AllProducts;
import pages.HeaderAndFooters;
import pages.HomePage;

public class Wholesale extends ApplicationKeywords {
	BaseClass obj;
	HomePage homePage;
	Wholesale_BecomeDistributor becomeDistributor;
	HeaderAndFooters headerAndFooter;
	Cart_AllProducts cartAllProducts;
	private boolean status = false;

	String password;

	public Wholesale(BaseClass obj) {
		super(obj);
		this.obj = obj;
		homePage = new HomePage(obj);
		becomeDistributor = new Wholesale_BecomeDistributor(obj);
		headerAndFooter = new HeaderAndFooters(obj);
		cartAllProducts = new Cart_AllProducts(obj);
	}

	/*
	 * TestCaseid : ML_TS_010 Description : Verify if the user is able to submit the
	 * 'Become a Distributor' form with all the valid details
	 * 
	 */
	public void wholesale_submitWithvalidDetails() {
		try {
			String email = retrieve("email");
			String firstName = retrieve("firstName");
			String lastName = retrieve("lastName");
			String businessName = retrieve("businessName");
			String ein = retrieve("ein");
			String phone = retrieve("phone");
			String address = retrieve("address");
			String city = retrieve("city");
			String company = retrieve("company");
			String state = retrieve("state");
			String zip = retrieve("zip");
			String message = retrieve("message");

			if (GOR.agePopUpHandled == false) {
				headerAndFooter.clickOver21Age();
			}

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooter.goTo_Products(0);
				cartAllProducts.selectFirstAvailableGummiesProduct();
				cartAllProducts.closeNewsletterOfferPopup();
			}

			headerAndFooter.goTo_Wholesale();
			becomeDistributor.FillAllDetails(firstName, lastName, businessName, 1, ein, email, phone, address, city,
					company, state, zip, message);
			becomeDistributor.clickTellMeMore();
			becomeDistributor.VerifySucessMessage();
		} catch (

		Exception e) {
			testStepFailed("Submitting valid details in distributor form could not be verified");
		}
		if (obj.testFailure || homePage.testFailure || becomeDistributor.testFailure || headerAndFooter.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : ML_TS_011 Description : Verify if the user is not able to submit
	 * the 'Become a Distributor' form with invalid details
	 * 
	 */
	public void wholesale_submitWithInvalidDetails() {
		try {
			String invalidEmail = retrieve("invalidEmail");

			String email = retrieve("email");
			String firstName = retrieve("firstName");
			String lastName = retrieve("lastName");
			String businessName = retrieve("businessName");
			String ein = retrieve("ein");
			String phone = retrieve("phone");
			String address = retrieve("address");
			String city = retrieve("city");
			String company = retrieve("company");
			String state = retrieve("state");
			String zip = retrieve("zip");
			String message = retrieve("message");

			if (GOR.agePopUpHandled == false) {
				headerAndFooter.clickOver21Age();
			}

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooter.goTo_Products(0);
				cartAllProducts.selectFirstAvailableGummiesProduct();
				cartAllProducts.closeNewsletterOfferPopup();
			}
			headerAndFooter.goTo_Wholesale();
			becomeDistributor.FillAllDetails("", "", "", 0, "", invalidEmail, "", "", "", "", "", "", "");
			becomeDistributor.clickTellMeMore();
			driver.switchTo().activeElement();
			becomeDistributor.VerifyErrorMessage_EmptyFields();
			becomeDistributor.FillAllDetails(firstName, lastName, businessName, 1, ein, email, phone, address, city,
					company, state, zip, message);
		} catch (

		Exception e) {
			testStepFailed("Error message for Login using Invalid Credentials could not be verified");
		}
		if (obj.testFailure || homePage.testFailure || becomeDistributor.testFailure || headerAndFooter.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

}
