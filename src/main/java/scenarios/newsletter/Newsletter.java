package scenarios.newsletter;

import java.util.HashMap;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.Cart_AllProducts;
import pages.Checkout;
import pages.HeaderAndFooters;
import pages.HomePage;
import pages.ShoppingCart;

public class Newsletter extends ApplicationKeywords {
	BaseClass obj;
	HeaderAndFooters headerAndFooters;
	pages.Newsletter newsletter;
	Cart_AllProducts cart_AllProducts;
	ShoppingCart shoppingCart;
	Checkout checkout;
	HomePage homepage;
	private boolean status = false;

	String password;

	public Newsletter(BaseClass obj) {
		super(obj);
		this.obj = obj;
		headerAndFooters = new HeaderAndFooters(obj);
		newsletter = new pages.Newsletter(obj);
		cart_AllProducts = new Cart_AllProducts(obj);
		shoppingCart = new ShoppingCart(obj);
		checkout = new Checkout(obj);
		homepage = new HomePage(obj);
	}

	/*
	 * TestCaseid : ML_TS_131 Description : Verify if the error messages for invalid
	 * data are displayed
	 */
	public void submitInvalidDetails() {
		try {

			String invalidEmail = retrieve("invalidEmail");

			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0);
				cart_AllProducts.selectFirstAvailableGummiesProduct();
				cart_AllProducts.closeNewsletterOfferPopup();
			}

			if (GOR.loggedIn == true) {
				headerAndFooters.navigateMyAccountMenu("signOut");
				headerAndFooters.clickOver21Age();
			}

			headerAndFooters.navigateFooter("newsletter");
			newsletter.clickSubscribe();
			newsletter.verifyErrors("emptyEmail");
			newsletter.enterEmail(invalidEmail);
			newsletter.clickSubscribe();
			newsletter.verifyErrors("invalidEmail");
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || headerAndFooters.testFailure || newsletter.testFailure || homepage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : ML_TS_015_016 Description : Verify if the user is able to sign
	 * up for Mystic Labs Newsletter and the instant coupon code is getting
	 * displayed after subscribing for a newsletter.
	 */
	public void submitValidDetails() {
		try {

			String email = retrieve("email");

			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0);
				cart_AllProducts.selectFirstAvailableGummiesProduct();
				cart_AllProducts.closeNewsletterOfferPopup();
			}

			if (GOR.loggedIn == true) {
				headerAndFooters.navigateMyAccountMenu("signOut");
				headerAndFooters.clickOver21Age();
			}

			headerAndFooters.navigateFooter("newsletter");
			newsletter.enterEmail(email);
			newsletter.clickSubscribe();
			newsletter.verify_SuccessMessage_InstantCouponCode();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || headerAndFooters.testFailure || newsletter.testFailure || homepage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : ML_TS_017 Description :Verify if the user is able to apply the
	 * newsletter instant discount code in cart / checkout pages
	 */
	public void verifyCouponCode() {
		try {
			String couponCode_Newsletter = retrieve("couponCode_Newsletter");
			HashMap<String, String> shippingData = new HashMap<String, String>();

			shippingData.put("firstName_data", retrieve("firstName_shipping"));
			shippingData.put("lastName_data", retrieve("lastName_shipping"));
			shippingData.put("company_data", retrieve("company"));
			shippingData.put("billingCountry_data", null);
			shippingData.put("streetAddressFirstLine_data", retrieve("streetAddressFirstLine_shipping"));
			shippingData.put("streetAddressSecondLine_data", retrieve("streetAddressSecondLine_shipping"));
			shippingData.put("streetAddressThirdLine_data", retrieve("streetAddressThirdLine_shipping"));
			shippingData.put("city_data", retrieve("city_shipping"));
			shippingData.put("state_data", retrieve("state_shipping"));
			shippingData.put("phoneNumber_data", retrieve("phoneNumber"));
			shippingData.put("postCode_data", retrieve("postCode"));
			shippingData.put("email_data", retrieve("email"));

			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0);
				cart_AllProducts.selectFirstAvailableGummiesProduct();
				cart_AllProducts.closeNewsletterOfferPopup();
			}

			if (GOR.loggedIn == true) {
				headerAndFooters.navigateMyAccountMenu("signOut");
				headerAndFooters.clickOver21Age();
			}

			headerAndFooters.goTo_Products(0);
			cart_AllProducts.clickAddToCartNonGummies_PGP();
			cart_AllProducts.clickShoppingCartLink();
			shoppingCart.clickApplyDiscountCodeHeader();
			shoppingCart.enterDiscountCode(couponCode_Newsletter);
			shoppingCart.clickApplyDiscount();
			shoppingCart.verifyExpectedMessageAlert("", "used coupon code", "NEWS10");
			shoppingCart.clickCancel_DiscountCoupon();
			shoppingCart.clickProceedToCheckOut();
			checkout.fillDetailsShipping(shippingData);
			checkout.clickDesiredShippingOption("upsOvernightShipping");
			checkout.clickNext();
			checkout.clickApplyDiscountCodeHeader();
			checkout.enterDiscountCode(couponCode_Newsletter);
			checkout.clickApplyDiscount();
			checkout.clickCancel_DiscountCoupon();
			headerAndFooters.clickLogo();
			testStepInfo("Successfully tested applying the coupon in both cart and checkout pages");

		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || headerAndFooters.testFailure || newsletter.testFailure || shoppingCart.testFailure
				|| cart_AllProducts.testFailure || homepage.testFailure || checkout.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

}
