package scenarios.General;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.Cart_AllProducts;
import pages.HomePage;
import pages.MiniCart;
import pages.ShoppingCart;

public class General extends ApplicationKeywords {
	BaseClass obj;
	HomePage homePage;
	pages.HeaderAndFooters headerAndFooters;
	Cart_AllProducts cart_AllProducts;
	MiniCart miniCart;
	ShoppingCart shoppingCart;
	HomePage homepage;
	private boolean status = false;

	String password;

	public General(BaseClass obj) {
		super(obj);
		this.obj = obj;
		homePage = new HomePage(obj);
		miniCart = new MiniCart(obj);
		headerAndFooters = new pages.HeaderAndFooters(obj);
		shoppingCart = new ShoppingCart(obj);
		cart_AllProducts = new Cart_AllProducts(obj);
		homepage = new HomePage(obj);
	}

	/*
	 * TestCaseid : General_ML_TS_001 Description : Verify if the user is able to
	 * launch the Mystic Labs application successfully
	 */
	public void verifyApplicationLaunchedSuccessfully() {
		try {
			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0);
				cart_AllProducts.selectFirstAvailableGummiesProduct();
				cart_AllProducts.closeNewsletterOfferPopup();
			}
			headerAndFooters.goTo_Home();
			headerAndFooters.verifyNavigation("home");
			testStepInfo("Mystic Labs application was launched successfully");
		} catch (Exception e) {
			testStepFailed("Mystic Labs application was not launched successfully");
			e.printStackTrace();
		}
		if (obj.testFailure || headerAndFooters.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : General_ML_TS_002 Description : Verify all the header menus are
	 * navigating to the respective pages
	 */
	public void verifyHeader() {
		try {
			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0);
				cart_AllProducts.selectFirstAvailableGummiesProduct();
				cart_AllProducts.closeNewsletterOfferPopup();
			}

			headerAndFooters.goToSignIn();
			headerAndFooters.verifyNavigation("SignInPage");
			headerAndFooters.goToCreateAnAccount();
			headerAndFooters.verifyNavigation("RegisterPage");
			headerAndFooters.goTo_Home();
			headerAndFooters.verifyNavigation("home");
			headerAndFooters.goTo_Products(0);
			headerAndFooters.verifyNavigation("products");
			headerAndFooters.goTo_Products(1);
			headerAndFooters.verifyNavigation("Delta-8 Gummies");
			headerAndFooters.goTo_Products(2);
			headerAndFooters.verifyNavigation("Delta-8 Vape");
			headerAndFooters.goTo_Products(3);
			headerAndFooters.verifyNavigation("Delta-8 Oils");
	//		headerAndFooters.goTo_Products(4);
	//		headerAndFooters.verifyNavigation("Trifecta Products");
			headerAndFooters.goTo_Products(5);
			headerAndFooters.verifyNavigation("Delta 8 Pain Rub");

			headerAndFooters.goTo_AboutUs();
			headerAndFooters.verifyNavigation("About Us");

			headerAndFooters.goTo_faq();
			headerAndFooters.verifyNavigation("FAQ");

			headerAndFooters.goTo_Wholesale();
			headerAndFooters.verifyNavigation("Wholesale");

			headerAndFooters.goTo_Contact();
			headerAndFooters.verifyNavigation("contact");

			headerAndFooters.clickOnMiniCart();
			miniCart.removeAllProducts();
			headerAndFooters.verifyNavigation("emptyMiniCart");
			headerAndFooters.goTo_Products(0);
			cart_AllProducts.selectFirstAvailableGummiesProduct();
			cart_AllProducts.chooseFromGummyFlavorDropdown();
			cart_AllProducts.clickAddToCart();
			if (GOR.freeProductOfferPopUpHandled == false) {
				cart_AllProducts.closeFreeProductOfferPopup();
			}
			headerAndFooters.clickOnMiniCart();
			headerAndFooters.verifyNavigation("miniCartWithProducts");
			miniCart.clickViewAndEditCart();
			headerAndFooters.verifyNavigation("shoppingCart");
			shoppingCart.clickProceedToCheckOut();
			headerAndFooters.verifyNavigation("checkout");
			headerAndFooters.clickLogo();

		} catch (Exception e) {
			testStepFailed("Header Navigation could not be verified");
			e.printStackTrace();
		}
		if (obj.testFailure || headerAndFooters.testFailure || cart_AllProducts.testFailure || miniCart.testFailure
				|| shoppingCart.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : General_ML_TS_003 Description : Verify all the footer menus are
	 * navigating to the respective pages
	 */
	public void verifyFooter() {
		try {
			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0);
				cart_AllProducts.selectFirstAvailableGummiesProduct();
				cart_AllProducts.closeNewsletterOfferPopup();
			}

			headerAndFooters.clickLogo();
			headerAndFooters.navigateFooter("footerLogo");
			headerAndFooters.verifyNavigation("home");
			headerAndFooters.navigateFooter("instagram");
			headerAndFooters.verifyNavigation("instagram");
//			headerAndFooters.navigateFooter("twitter");
//			headerAndFooters.verifyNavigation("twitter");
			headerAndFooters.navigateFooter("home");
			headerAndFooters.verifyNavigation("home");
			headerAndFooters.navigateFooter("about");
			headerAndFooters.verifyNavigation("About Us");
			headerAndFooters.navigateFooter("Wholesale");
			headerAndFooters.verifyNavigation("Wholesale");
			headerAndFooters.navigateFooter("terms");
			headerAndFooters.verifyNavigation("terms");
			headerAndFooters.navigateFooter("privacy");
			headerAndFooters.verifyNavigation("privacy");
			headerAndFooters.navigateFooter("shipping");
			headerAndFooters.verifyNavigation("shipping");
			headerAndFooters.navigateFooter("returns");
			headerAndFooters.verifyNavigation("refund");
			headerAndFooters.navigateFooter("newsletter");
			headerAndFooters.verifyNavigation("Newsletter");
			headerAndFooters.navigateFooter("labTesting");
			headerAndFooters.verifyNavigation("LabTests");
//			headerAndFooters.navigateFooter("contact");
//			headerAndFooters.verifyNavigation("contact");
			headerAndFooters.navigateFooter("ccpa");
			headerAndFooters.verifyNavigation("ccpa");

		} catch (Exception e) {
			testStepFailed("Footer Navigation could not be verified");
			e.printStackTrace();
		}
		if (obj.testFailure || headerAndFooters.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : General_ML_TS_004 Description : Verify clicking on logo from any
	 * page should take the user to the home page
	 */
	public void verifyLogoLink() {
		try {

			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			headerAndFooters.goTo_Products(0);
			headerAndFooters.clickLogo();
			headerAndFooters.verifyNavigation("home");
			testStepInfo("Mystic Labs application was launched successfully");
		} catch (Exception e) {
			testStepFailed("Mystic Labs application was not launched successfully");
			e.printStackTrace();
		}
		if (obj.testFailure || headerAndFooters.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

}
