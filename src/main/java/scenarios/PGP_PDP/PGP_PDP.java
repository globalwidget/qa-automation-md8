package scenarios.PGP_PDP;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.Cart_AllProducts;
import pages.CompareProducts;
import pages.HeaderAndFooters;
import pages.HomePage;
import pages.MiniCart;
import pages.MyAccount;
import pages.ShoppingCart;
import pages.SignIn;
import pages.WishList;

public class PGP_PDP extends ApplicationKeywords {
	BaseClass obj;
	HeaderAndFooters headerAndFooters;
	HomePage homepage;
	Cart_AllProducts cartAllProducts;
	ShoppingCart shoppingCart;
	CompareProducts compareProducts;
	WishList wishList;
	SignIn signIn;
	MyAccount myAccount;
	MiniCart miniCart;
	private boolean status = false;

	public PGP_PDP(BaseClass obj) {
		super(obj);
		headerAndFooters = new HeaderAndFooters(obj);
		homepage = new HomePage(obj);
		cartAllProducts = new Cart_AllProducts(obj);
		shoppingCart = new ShoppingCart(obj);
		compareProducts = new CompareProducts(obj);
		wishList = new WishList(obj);
		signIn = new SignIn(obj);
		myAccount = new MyAccount(obj);
		miniCart = new MiniCart(obj);
		this.obj = obj;
	}

	/*
	 * TestCaseid : PGP_ML_TS_027 Description : To Verify if the user is able to add
	 * product to cart from PGP page.
	 */
	public void addToCart_PGP() {
		try {
			String productName;

			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0);
				cartAllProducts.selectFirstAvailableGummiesProduct();
				cartAllProducts.closeNewsletterOfferPopup();
			}

			if (GOR.loggedIn == true) {
				headerAndFooters.navigateMyAccountMenu("signOut");
				headerAndFooters.clickOver21Age();
			}

			headerAndFooters.goTo_Home();
			headerAndFooters.goTo_Products(0);
			productName = cartAllProducts.getProductNameFirstNonGummiesProduct_PGP();
			cartAllProducts.clickAddToCartNonGummies_PGP();
			if (GOR.freeProductOfferPopUpHandled == false)
			{
			cartAllProducts.closeFreeProductOfferPopup();
			}
			cartAllProducts.clickShoppingCartLink();
			shoppingCart.verifyPresenceOfExpectedProduct(productName);
			headerAndFooters.goTo_Home();
			headerAndFooters.clickOnMiniCart();
			miniCart.removeAllProducts();

		} catch (Exception e) {
			testStepFailed("Adding product to cart from PGP Page could not be verified");
		}
		if (obj.testFailure || headerAndFooters.testFailure || homepage.testFailure || cartAllProducts.testFailure
				|| shoppingCart.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : PGP_ML_TS_028 Description : To Verify if the user is able to add
	 * product to comparison list from PGP page.
	 */
	public void addProductToComparisonCart_PGP() {
		try {
			String productName;

			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0);
				cartAllProducts.selectFirstAvailableGummiesProduct();
				cartAllProducts.closeNewsletterOfferPopup();
			}

			if (GOR.loggedIn == true) {
				headerAndFooters.navigateMyAccountMenu("signOut");
				headerAndFooters.clickOver21Age();
			}

			headerAndFooters.goTo_Home();
			headerAndFooters.goTo_Products(0);
			productName = cartAllProducts.getProductNameFirstGummiesProduct_PGP();
			cartAllProducts.clickAddToComparisonList_FirstAvailableGummies_PGP();
			cartAllProducts.clickComparisonListLink();
			compareProducts.verifyPresenceOfExpectedProduct(productName);
			compareProducts.removeAllProducts();

		} catch (Exception e) {
			testStepFailed("Adding product to comparison list from PGP Page could not be verified");
		}
		if (obj.testFailure || headerAndFooters.testFailure || homepage.testFailure || cartAllProducts.testFailure
				|| compareProducts.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : PGP_ML_TS_030 Description : To Verify if the user is not able to
	 * add products to wish list as a guest user from the PGP page.
	 */
	public void notAbleToAddProductToWishList_GuestUser_PGP() {
		try {
			String email = retrieve("email");
			String password = retrieve("password");

			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0);
				cartAllProducts.selectFirstAvailableGummiesProduct();
				cartAllProducts.closeNewsletterOfferPopup();
			}

			if (GOR.loggedIn == true) {
				headerAndFooters.navigateMyAccountMenu("signOut");
				headerAndFooters.clickOver21Age();
			}

			headerAndFooters.goTo_Home();
			headerAndFooters.goTo_Products(0);
			cartAllProducts.clickAddToWishList_PGP_WithoutDropdown();
			signIn.verifyErrorMessage_SignIn("wishListError");
			signIn.signIn(email, password);
			wishList.removeAllProducts();
			headerAndFooters.navigateMyAccountMenu("signOut");
			headerAndFooters.clickOver21Age();

		} catch (Exception e) {
			testStepFailed(
					"Not able to add product to wish List as a guest user from the PGP page could not be verified");
		}
		if (obj.testFailure || headerAndFooters.testFailure || homepage.testFailure || cartAllProducts.testFailure
				|| signIn.testFailure || myAccount.testFailure || wishList.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : PGP_ML_TS_029 Description : To Verify if the user is able to add
	 * products to wish list from the PGP page.
	 */
	public void AddProductToWishList_SignedInUser_PGP() {
		try {
			String productName;
			String email = retrieve("email");
			String password = retrieve("password");

			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0);
				cartAllProducts.selectFirstAvailableGummiesProduct();
				cartAllProducts.closeNewsletterOfferPopup();
			}

			headerAndFooters.goTo_Home();

			if (GOR.loggedIn == false) {
				headerAndFooters.goToSignIn();
				signIn.signIn(email, password);
			}

			headerAndFooters.goTo_Products(0);
			productName = cartAllProducts.getProductNameFirstProductWithoutDropdown_PGP();
			cartAllProducts.clickAddToWishList_PGP_WithoutDropdown();
			headerAndFooters.navigateMyAccountMenu("wishList");
			wishList.verifyPresenceOfExpectedProduct(productName);
			wishList.removeAllProducts();
		} catch (Exception e) {
			testStepFailed(
					"Able to add product to wish list as a signed In User from the PGP page could not be verified");
		}
		if (obj.testFailure || headerAndFooters.testFailure || homepage.testFailure || cartAllProducts.testFailure
				|| wishList.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : PGP_ML_TS_031 Description : Verify clicking on the products from
	 * PGP page is taking the user to PDP page.
	 */
	public void pgpTopdp_Verification() {
		try {
			String productName_pdp;
			String productName_pgp;

			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			headerAndFooters.goTo_Home();
			headerAndFooters.clickOnMiniCart();
			headerAndFooters.goTo_Products(0);
			productName_pgp = cartAllProducts.getProductNameFirstGummiesProduct_PGP();
			cartAllProducts.selectFirstAvailableGummiesProduct();
			productName_pdp = cartAllProducts.getProductTitle_PDP();
			if (productName_pgp.contains(productName_pdp)) {
				testStepInfo("Successfully navigated from PGP to PDP page.");
			} else {
				testStepFailed("The product selected in PGP page is not same as product in PDP page opened");
			}

		} catch (Exception e) {
			testStepFailed("Verification of navigation from PGP to PDP page could not be verified");
		}
		if (obj.testFailure || headerAndFooters.testFailure || cartAllProducts.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : PGP_ML_TS_037 Description : To Verify if the user is able to
	 * modify the quantity of the product and add to cart.
	 */
	public void modifyCountAndaddToCart() {
		try {
			String productName;

			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0);
				cartAllProducts.selectFirstAvailableGummiesProduct();
				cartAllProducts.closeNewsletterOfferPopup();
			}

			headerAndFooters.goTo_Home();
			headerAndFooters.clickOnMiniCart();
			miniCart.removeAllProducts();
			headerAndFooters.goTo_Products(0);
			cartAllProducts.selectFirstAvailableGummiesProduct();
			cartAllProducts.chooseFromGummyFlavorDropdown();
			productName = cartAllProducts.getProductTitle_PDP();
			cartAllProducts.updateProductQuantity(10);
			cartAllProducts.clickAddToCart();
			if (GOR.freeProductOfferPopUpHandled == false)
			{
			cartAllProducts.closeFreeProductOfferPopup();
			}
			cartAllProducts.clickShoppingCartLink();
			shoppingCart.verifyPresenceOfExpectedProduct(productName);
			shoppingCart.checkCartQuantity(10);
			headerAndFooters.clickOnMiniCart();
			miniCart.removeAllProducts();

		} catch (Exception e) {
			testStepFailed("Modifying the quantity of the product and adding to cart could not be verified");
		}
		if (obj.testFailure || headerAndFooters.testFailure || homepage.testFailure || miniCart.testFailure
				|| signIn.testFailure || cartAllProducts.testFailure || shoppingCart.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : PDP_ML_TS_038 Description : To Verify if the user is able to add
	 * product to comparison list from PDP page.
	 */
	public void addProductToComparisonCart_PDP() {
		try {
			String productName;

			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0);
				cartAllProducts.selectFirstAvailableGummiesProduct();
				cartAllProducts.closeNewsletterOfferPopup();
			}

			headerAndFooters.goTo_Home();
			headerAndFooters.goTo_Products(0);
			cartAllProducts.selectFirstAvailableGummiesProduct();
			productName = cartAllProducts.getProductTitle_PDP();
			cartAllProducts.clickAddToComparisonList_PDP();
			cartAllProducts.clickComparisonListLink();
			compareProducts.verifyPresenceOfExpectedProduct(productName);
			compareProducts.removeAllProducts();
		} catch (Exception e) {
			testStepFailed("Adding product to comparison list from PDP Page could not be verified");
		}
		if (obj.testFailure || headerAndFooters.testFailure || homepage.testFailure || cartAllProducts.testFailure
				|| compareProducts.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : PDP_ML_TS_040 Description : To Verify if the user is not able to
	 * add products to wish list as a guest user.
	 */
	public void notAbleToAddProductToWishList_GuestUser_PDP() {
		try {
			String email = retrieve("email");
			String password = retrieve("password");

			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0);
				cartAllProducts.selectFirstAvailableGummiesProduct();
				cartAllProducts.closeNewsletterOfferPopup();
			}

			if (GOR.loggedIn == true) {
				headerAndFooters.navigateMyAccountMenu("signOut");
				headerAndFooters.clickOver21Age();
			}

			headerAndFooters.goTo_Home();
			headerAndFooters.goTo_Products(0);
			cartAllProducts.selectFirstAvailableGummiesProduct();
			cartAllProducts.clickAddToWishList_PDP();
			signIn.verifyErrorMessage_SignIn("wishListError");
			signIn.signIn(email, password);
			wishList.removeAllProducts();
			headerAndFooters.navigateMyAccountMenu("signOut");
			headerAndFooters.clickOver21Age();

		} catch (Exception e) {
			testStepFailed(
					"Not able to add product to wish List as a guest user from the PDP page could not be verified");
		}
		if (obj.testFailure || headerAndFooters.testFailure || homepage.testFailure || cartAllProducts.testFailure
				|| signIn.testFailure || myAccount.testFailure || wishList.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : PDP_ML_TS_039 Description : To Verify if the user is able to add
	 * products to wish list as a signed in user from the PDP page.
	 */
	public void AddProductToWishList_SignedInUser_PDP() {
		try {
			String productName;
			String email = retrieve("email");
			String password = retrieve("password");

			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0);
				cartAllProducts.selectFirstAvailableGummiesProduct();
				cartAllProducts.closeNewsletterOfferPopup();
			}

			headerAndFooters.goTo_Home();

			if (GOR.loggedIn == false) {
				headerAndFooters.goToSignIn();
				signIn.signIn(email, password);
			}

			headerAndFooters.goTo_Home();
			headerAndFooters.goTo_Products(0);
			cartAllProducts.selectFirstAvailableGummiesProduct();
			cartAllProducts.chooseFromGummyFlavorDropdown();
			productName = cartAllProducts.getProductTitle_PDP();
			cartAllProducts.clickAddToWishList_PDP();
			headerAndFooters.navigateMyAccountMenu("wishList");
			wishList.verifyPresenceOfExpectedProduct(productName);
			wishList.removeAllProducts();

			if (GOR.loggedIn == true) {
				headerAndFooters.navigateMyAccountMenu("signOut");
				headerAndFooters.clickOver21Age();
			}

		} catch (Exception e) {
			testStepFailed(
					"Able to add product to wish list as a signed In User from the PDP page could not be verified");
		}
		if (obj.testFailure || headerAndFooters.testFailure || homepage.testFailure || cartAllProducts.testFailure
				|| wishList.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

}
