package scenarios.Register;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.Cart_AllProducts;
import pages.CreateAccount;
import pages.HeaderAndFooters;
import pages.HomePage;
import pages.MyAccount;

public class Register extends ApplicationKeywords {
	BaseClass obj;
	CreateAccount register;
	HeaderAndFooters headerAndFooters;
	HomePage homepage;
	MyAccount myAccount;
	Cart_AllProducts cartAllProducts;
	private boolean status = false;

	public Register(BaseClass obj) {
		super(obj);
		register = new CreateAccount(obj);
		headerAndFooters = new HeaderAndFooters(obj);
		homepage = new HomePage(obj);
		myAccount = new MyAccount(obj);
		cartAllProducts = new Cart_AllProducts(obj);
		this.obj = obj;
	}

	/*
	 * TestCaseid : Register_ML_TS_005 Description : To Verify if the user is able
	 * to create a new account with valid details.
	 */
	public void register_ValidDetails() {
		try {
			String firstName = retrieve("firstName");
			String lastName = retrieve("lastName");
			String email = retrieve("email");
			String password = retrieve("password");

			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			headerAndFooters.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0);
				cartAllProducts.selectFirstAvailableGummiesProduct();
				cartAllProducts.closeNewsletterOfferPopup();
			}

			if (GOR.loggedIn == true) {
				headerAndFooters.navigateMyAccountMenu("signOut");
				headerAndFooters.clickOver21Age();
			}

			headerAndFooters.goToCreateAnAccount();
			register.register_FillDetails(firstName, lastName, email, password, password);
			register.clickCreateAccount();
			register.verifyMessage_Register("successMessage");
			headerAndFooters.navigateMyAccountMenu("signOut");
			headerAndFooters.clickOver21Age();
		} catch (Exception e) {
			testStepFailed("Creating an account using valid credentials could not be verified");
		}
		if (obj.testFailure || headerAndFooters.testFailure || register.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Register_ML_TS_006 Description : To Verify if the user is not
	 * able to create a new account with invalid details.
	 */
	public void register_InvalidDetails() {
		try {
			String firstName = retrieve("firstName");
			String lastName = retrieve("lastName");
			String email = retrieve("email");
			String invalidEmail = retrieve("invalidEmail");
			String passwordWrongLength = retrieve("passwordWrongLength");
			String passwordWrongComposition = retrieve("passwordWrongComposition");
			String password = retrieve("password");
			String wrongPassword = retrieve("wrongPassword");

			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			headerAndFooters.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0);
				cartAllProducts.selectFirstAvailableGummiesProduct();
				cartAllProducts.closeNewsletterOfferPopup();
			}

			if (GOR.loggedIn == true) {
				headerAndFooters.navigateMyAccountMenu("signOut");
				headerAndFooters.clickOver21Age();
			}

			headerAndFooters.goToCreateAnAccount();
			register.clickCreateAccount();
			register.VerifyEmptyFieldErrors();
			register.register_FillDetails(firstName, lastName, invalidEmail, passwordWrongLength, null);
			register.verifyMessage_Register("passwordInvalidLength");
			register.register_FillDetails(null, null, null, passwordWrongComposition, wrongPassword);
			register.verifyMessage_Register("passsword_InvalidError_MinimumDifferentClasses");
			register.register_FillDetails(null, null, null, password, null);
			register.clickCreateAccount();
			register.verifyMessage_Register("emailInvalid");
			register.verifyMessage_Register("confirmPasswordInvalid");
//			register.register_FillDetails(null, null, email, null, password);
//			register.clickCreateAccount();
//			register.verifyMessage_Register("accountExistsError");

		} catch (Exception e) {
			testStepFailed("Error messages for account registration using Invalid details could not be verified");
		}
		if (obj.testFailure || headerAndFooters.testFailure || register.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : CreateAccount_ML_TS_010 Description : To Verify if the user is
	 * able sign up for Newsletter while creating an account.
	 */
	public void createAccount_VerifySignUpForNewsletter() {
		try {
			String firstName = retrieve("firstName");
			String lastName = retrieve("lastName");
			String email = retrieve("email");
			String password = retrieve("password");

			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			headerAndFooters.goTo_Home();

			if (GOR.loggedIn == true) {
				headerAndFooters.navigateMyAccountMenu("signOut");
				headerAndFooters.clickOver21Age();
			}

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0);
				cartAllProducts.selectFirstAvailableGummiesProduct();
				cartAllProducts.closeNewsletterOfferPopup();
			}

			headerAndFooters.goToCreateAnAccount();
			register.clickNewsletterCheckbox();
			register.register_FillDetails(firstName, lastName, email, password, password);
			register.clickCreateAccount();
			register.verifyMessage_Register("successMessage");
			myAccount.verifyMessage_MyAccount("newsLetterSubscribedMessage");
			headerAndFooters.navigateMyAccountMenu("signOut");
			headerAndFooters.clickOver21Age();
		} catch (Exception e) {
			testStepFailed("Creating an account along with subscribing to newsletter could not be verified");
		}
		if (obj.testFailure || headerAndFooters.testFailure || register.testFailure || homepage.testFailure
				|| myAccount.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

}
