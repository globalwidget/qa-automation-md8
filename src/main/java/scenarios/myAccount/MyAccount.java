package scenarios.myAccount;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.Cart_AllProducts;
import pages.CreateAccount;
import pages.HeaderAndFooters;
import pages.HomePage;
import pages.MiniCart;
import pages.ShoppingCart;
import pages.SignIn;
import pages.WishList;

public class MyAccount extends ApplicationKeywords {
	BaseClass obj;
	CreateAccount register;
	HeaderAndFooters headerAndFooters;
	HomePage homepage;
	pages.MyAccount myAccount;
	SignIn signIn;
	Cart_AllProducts cart_AllProducts;
	WishList wishList;
	MiniCart miniCart;
	ShoppingCart cart;
	private boolean status = false;

	public MyAccount(BaseClass obj) {
		super(obj);
		register = new CreateAccount(obj);
		headerAndFooters = new HeaderAndFooters(obj);
		homepage = new HomePage(obj);
		myAccount = new pages.MyAccount(obj);
		signIn = new SignIn(obj);
		cart_AllProducts = new Cart_AllProducts(obj);
		wishList = new WishList(obj);
		miniCart = new MiniCart(obj);
		cart = new ShoppingCart(obj);
		this.obj = obj;
	}

	/*
	 * TestCaseid : MyAccount_ML_TS_099 Description : To Verify if the user is able
	 * to edit the basic details of a account.
	 */
	public void myAccount_EditbasicDetails() {
		try {
			String firstName = retrieve("firstName");
			String lastName = retrieve("lastName");
			String email = retrieve("email");
			String password = retrieve("password");

			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			headerAndFooters.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0);
				cart_AllProducts.selectFirstAvailableGummiesProduct();
				cart_AllProducts.closeNewsletterOfferPopup();
			}

			if (GOR.loggedIn == false) {
				headerAndFooters.goTo_Home();
				headerAndFooters.goToSignIn();
				signIn.signIn(email, password);
			}

			myAccount.clickOnMyAccount();
			headerAndFooters.navigateMyAccountMenu("myAccount");
			myAccount.clickEditContactInformation();
			myAccount.enterFirstNameLastName(firstName, lastName);
			myAccount.clickSave();
			myAccount.clickEditContactInformation();
			myAccount.enterFirstNameLastName(lastName, firstName);
			myAccount.clickSave();

		} catch (Exception e) {
			testStepFailed("Editing basic details of a account could not be verified");
		}
		if (obj.testFailure || headerAndFooters.testFailure || myAccount.testFailure || signIn.testFailure
				|| homepage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : MyAccount_ML_TS_100 Description : To Verify is the user is able
	 * to change the password.
	 */
	public void myAccount_ChangePassword() {
		try {
			String email = retrieve("email");
			String password = retrieve("password");
			String oldPassword = retrieve("oldPassword");
			String newPassword = retrieve("newPassword");

			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			headerAndFooters.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0);
				cart_AllProducts.selectFirstAvailableGummiesProduct();
				cart_AllProducts.closeNewsletterOfferPopup();
			}

			if (GOR.loggedIn == false) {
				headerAndFooters.goTo_Home();
				headerAndFooters.goToSignIn();
				signIn.signIn(email, password);
			}

			headerAndFooters.navigateMyAccountMenu("myAccount");
			myAccount.clickOnMyAccount();
			myAccount.clickChangePassword();
			myAccount.enterOldAndNewPassword(oldPassword, newPassword);
			myAccount.clickSave();
			headerAndFooters.clickOver21Age();
			signIn.signIn(email, newPassword);
			myAccount.clickChangePassword();
			myAccount.enterOldAndNewPassword(newPassword, oldPassword);
			myAccount.clickSave();
			headerAndFooters.clickOver21Age();
			signIn.signIn(email, oldPassword);

		} catch (Exception e) {
			testStepFailed("Editing basic details of a account could not be verified");
		}
		if (obj.testFailure || headerAndFooters.testFailure || myAccount.testFailure || signIn.testFailure
				|| homepage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : MyAccount_ML_TS_101 Description : To Verify if the user is able
	 * to change email.
	 */
	public void myAccount_ChangeEmail() {
		try {
			String newEmail = retrieve("newEmail");
			String password = retrieve("password");
			String oldEmail = retrieve("oldEmail");

			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			headerAndFooters.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0);
				cart_AllProducts.selectFirstAvailableGummiesProduct();
				cart_AllProducts.closeNewsletterOfferPopup();
			}

			if (GOR.loggedIn == false) {
				headerAndFooters.goTo_Home();
				headerAndFooters.goToSignIn();
				signIn.signIn(oldEmail, password);
			}

			headerAndFooters.navigateMyAccountMenu("myAccount");
			myAccount.clickOnMyAccount();
			myAccount.clickEditContactInformation();
			myAccount.clickChangeEmail();
			myAccount.enterDetailsEmail(newEmail, password);
			myAccount.clickSave();
			headerAndFooters.clickOver21Age();
			signIn.signIn(newEmail, password);
			myAccount.clickEditContactInformation();
			myAccount.clickChangeEmail();
			myAccount.enterDetailsEmail(oldEmail, password);
			myAccount.clickSave();
			headerAndFooters.clickOver21Age();
			signIn.signIn(oldEmail, password);

		} catch (Exception e) {
			testStepFailed("Changing the Email of a account could not be verified");
		}
		if (obj.testFailure || headerAndFooters.testFailure || myAccount.testFailure || signIn.testFailure
				|| homepage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : MyAccount_ML_TS_103 Description : To Verify if the user is able
	 * to subscribe / unsubscribe to Newsletters.
	 */
	public void myAccount_verifyNewsletterSubscription() {
		try {
			String email = retrieve("email");
			String password = retrieve("password");

			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			headerAndFooters.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0);
				cart_AllProducts.selectFirstAvailableGummiesProduct();
				cart_AllProducts.closeNewsletterOfferPopup();
			}

			if (GOR.loggedIn == false) {
				headerAndFooters.goTo_Home();
				headerAndFooters.goToSignIn();
				signIn.signIn(email, password);
			}

			headerAndFooters.navigateMyAccountMenu("myAccount");
			myAccount.clickOnMyAccount();
			myAccount.clickNewsletterSubscriptionTab();
			myAccount.clickGeneralSubscription();
			myAccount.clickSave();
			myAccount.clickNewsletterSubscriptionTab();
			myAccount.verifyGeneralSubscriptionChecked();
			myAccount.clickGeneralSubscription();
			myAccount.clickSave();

		} catch (Exception e) {
			testStepFailed("Newsletter Subscribing and Unsubscribing could not be verified");
		}
		if (obj.testFailure || headerAndFooters.testFailure || myAccount.testFailure || signIn.testFailure
				|| homepage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : MyAccount_ML_TS_114 Description : To Verify if the user is able
	 * to add all to cart from the wish list.
	 */
	public void myAccount_verifyAddAllToCartFromWishList() {
		try {
			String email = retrieve("email");
			String password = retrieve("password");
			String firstProduct;
			String secondProduct;

			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			headerAndFooters.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0);
				cart_AllProducts.selectFirstAvailableGummiesProduct();
				cart_AllProducts.closeNewsletterOfferPopup();
			}

			if (GOR.loggedIn == false) {
				headerAndFooters.goTo_Home();
				headerAndFooters.goToSignIn();
				signIn.signIn(email, password);
			}

			headerAndFooters.goTo_Products(0);
			firstProduct = cart_AllProducts.getProductNameFirstProductWithoutDropdown_PGP();
			cart_AllProducts.clickAddToWishList_PGP_WithoutDropdown();
			waitTime(3);
			headerAndFooters.goTo_Products(0);
			cart_AllProducts.selectSecondProduct_Gummies();
			secondProduct = cart_AllProducts.getProductTitle_PDP();
			cart_AllProducts.chooseFromGummyFlavorDropdown();
			cart_AllProducts.clickAddToWishList_PDP();
			headerAndFooters.navigateMyAccountMenu("wishList");
			wishList.clickAddAllToCart();
			headerAndFooters.goTo_Home();
			headerAndFooters.clickOnMiniCart();
			miniCart.clickViewAndEditCart();
			cart.verifyPresenceOfExpectedProduct(firstProduct);
			cart.verifyPresenceOfExpectedProduct(secondProduct);
			headerAndFooters.goTo_Home();
			headerAndFooters.clickOnMiniCart();
			miniCart.removeAllProducts();

		} catch (Exception e) {
			testStepFailed("Adding all products in wish list to cart could not be verified");
		}
		if (obj.testFailure || headerAndFooters.testFailure || signIn.testFailure || homepage.testFailure
				|| wishList.testFailure || cart_AllProducts.testFailure || cart.testFailure || miniCart.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : MyAccount_ML_TS_115 Description : To Verify if the user is able
	 * to delete the products from wishlist.
	 */
	public void myAccount_verifyDeleteAllProductsFromWishList() {
		try {
			String email = retrieve("email");
			String password = retrieve("password");

			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			headerAndFooters.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0);
				cart_AllProducts.selectFirstAvailableGummiesProduct();
				cart_AllProducts.closeNewsletterOfferPopup();
			}

			if (GOR.loggedIn == false) {
				headerAndFooters.goTo_Home();
				headerAndFooters.goToSignIn();
				signIn.signIn(email, password);
			}

			headerAndFooters.goTo_Products(0);
			cart_AllProducts.clickAddToWishList_PGP_WithoutDropdown();
			headerAndFooters.goTo_Products(0);
			cart_AllProducts.selectSecondProduct_Gummies();
			cart_AllProducts.clickAddToWishList_PDP();
			wishList.removeAllProducts();
			wishList.verifyWishListEmptyMessage();
			testStepInfo("All products from the wish List was removed successfully");

		} catch (Exception e) {
			testStepFailed("Removing products from wish list not be verified");
		}
		if (obj.testFailure || headerAndFooters.testFailure || signIn.testFailure || homepage.testFailure
				|| wishList.testFailure || cart_AllProducts.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

}
