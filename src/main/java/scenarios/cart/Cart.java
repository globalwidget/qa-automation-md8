package scenarios.cart;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.Cart_AllProducts;
import pages.HeaderAndFooters;
import pages.HomePage;
import pages.MiniCart;
import pages.ShoppingCart;

public class Cart extends ApplicationKeywords {
	BaseClass obj;
	HomePage homePage;
	Cart_AllProducts cart_AllProducts;
	HeaderAndFooters headerAndfooter;
	ShoppingCart shoppingCart;
	MiniCart miniCart;
	private boolean status = false;

	String password;

	public Cart(BaseClass obj) {
		super(obj);
		this.obj = obj;
		homePage = new HomePage(obj);
		cart_AllProducts = new Cart_AllProducts(obj);
		headerAndfooter = new HeaderAndFooters(obj);
		shoppingCart = new ShoppingCart(obj);
		miniCart = new MiniCart(obj);
	}

//	/*
//	 * TestCaseid : Cart and PGP Description : To add a product to the cart.
//	 */
//	public void addProductToCart_PopUpverify() {
//		try {
//			String productName;
//			float cartSubTotal;
//	headerAndfooter.goTo_Home();
//	
//	if (GOR.OfferPopUpHandled == false) {
//		homePage.closeOfferPopup();
//	}
//			headerAndfooter.clickOnMiniCart();
//			miniCart.removeAllProducts();
//			headerAndfooter.goTo_Products(0);
//			cart_AllProducts.selectFirstProduct();
//			productName = cart_AllProducts.getProductTitle_PDP();
////			cart_AllProducts.chooseCapsuleStrength();
//			cartSubTotal = cart_AllProducts.getProductPrice();
//			cart_AllProducts.clickAddToCart();
//			cart_AllProducts.checkDetails_AddToCart(productName, 1, cartSubTotal);
//			cart_AllProducts.closeProceedToCartPopUp();
//		} catch (Exception e) {
//			testStepFailed("Add To Cart could not be done");
//		}
//		if (obj.testFailure || homePage.testFailure || cart_AllProducts.testFailure || headerAndfooter.testFailure
//				|| miniCart.testFailure) {
//			status = true;
//		}
//		this.testFailure = status;
//	}

	/*
	 * TestCaseid : ML_TS_052 Description : Verify if the user is able to delete the
	 * product from the cart.
	 */
	public void verifyDeleteProductFromCart() {
		try {

			if (GOR.agePopUpHandled == false) {
				headerAndfooter.clickOver21Age();
			}
			String productRemoved;

			headerAndfooter.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				headerAndfooter.goTo_Products(0);
				cart_AllProducts.selectFirstAvailableGummiesProduct();
				cart_AllProducts.closeNewsletterOfferPopup();

			}

			headerAndfooter.clickOnMiniCart();
			miniCart.removeAllProducts();
			headerAndfooter.goTo_Products(0);
			cart_AllProducts.selectFirstAvailableGummiesProduct();
			cart_AllProducts.chooseFromGummyFlavorDropdown();
			cart_AllProducts.clickAddToCart();
			if (GOR.freeProductOfferPopUpHandled == false) {
				cart_AllProducts.closeFreeProductOfferPopup();
			}
			cart_AllProducts.clickShoppingCartLink();
			productRemoved = shoppingCart.clickOnRemoveProduct();
			headerAndfooter.verifyNavigation("emptyCart");
			testStepInfo("The product - " + productRemoved + "was removed from the Cart");

		} catch (Exception e) {
			testStepFailed("Deleting a product from Shopping Cart could not be verified");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || headerAndfooter.testFailure || miniCart.testFailure
				|| shoppingCart.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : ML_TS_053 Description : Verify if the user is able to update the
	 * quantity of the products in the cart page
	 */
	public void verifyUpdateQuantityInCart() {
		try {
			headerAndfooter.goTo_Home();

			if (GOR.agePopUpHandled == false) {
				headerAndfooter.clickOver21Age();
			}

			if (GOR.OfferPopUpHandled == false) {
				headerAndfooter.goTo_Products(0);
				cart_AllProducts.selectFirstAvailableGummiesProduct();
				cart_AllProducts.closeNewsletterOfferPopup();
			}

			int quantity_input = 10;
			headerAndfooter.clickOnMiniCart();
			miniCart.removeAllProducts();
			headerAndfooter.goTo_Products(0);
			cart_AllProducts.selectFirstAvailableGummiesProduct();
			cart_AllProducts.chooseFromGummyFlavorDropdown();
			cart_AllProducts.clickAddToCart();
			if (GOR.freeProductOfferPopUpHandled == false) {
				cart_AllProducts.closeFreeProductOfferPopup();
			}
			cart_AllProducts.clickShoppingCartLink();
			shoppingCart.updateCartQuantity(quantity_input);
			shoppingCart.clickUpdateCart();
			shoppingCart.checkCartQuantity(quantity_input);
		} catch (Exception e) {
			testStepFailed("The user is not able to modify the quantity of the product in the Shopping Cart");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || headerAndfooter.testFailure || miniCart.testFailure
				|| shoppingCart.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : ML_TS_055 Description : Verify proper validation error message
	 * is getting displayed upon submitting invalid coupon code.
	 */
	public void applyInvalidCoupon() {
		try {

			String invalidCouponCode = retrieve("invalidCouponCode");

			if (GOR.agePopUpHandled == false) {
				headerAndfooter.clickOver21Age();
			}

			headerAndfooter.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				headerAndfooter.goTo_Products(0);
				cart_AllProducts.selectFirstAvailableGummiesProduct();
				cart_AllProducts.closeNewsletterOfferPopup();
			}

			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Products(0);
				cart_AllProducts.selectFirstAvailableGummiesProduct();
				cart_AllProducts.chooseFromGummyFlavorDropdown();
				cart_AllProducts.clickAddToCart();
				if (GOR.freeProductOfferPopUpHandled == false) {
					cart_AllProducts.closeFreeProductOfferPopup();
				}
				cart_AllProducts.clickShoppingCartLink();
			} else {
				headerAndfooter.clickOnMiniCart();
				miniCart.clickViewAndEditCart();
			}
			shoppingCart.clickApplyDiscountCodeHeader();
			shoppingCart.clickApplyDiscount();
			shoppingCart.verifyRelevantMessage("Coupon Code Missing");
			shoppingCart.enterDiscountCode(invalidCouponCode);
			shoppingCart.clickApplyDiscount();
			shoppingCart.verifyRelevantMessage("Invalid Coupon");
		} catch (Exception e) {
			testStepFailed("Could not verify errors for invalid coupon data");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || shoppingCart.testFailure || miniCart.testFailure
				|| shoppingCart.testFailure || headerAndfooter.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : ML_TS_054 Description : Verify if the user is able to apply the
	 * valid coupon code in the cart page
	 */
	public void applyValidCoupon() {
		try {

			String validCouponCode = retrieve("validCouponCode");

			if (GOR.agePopUpHandled == false) {
				headerAndfooter.clickOver21Age();
			}

			headerAndfooter.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				headerAndfooter.goTo_Products(0);
				cart_AllProducts.selectFirstAvailableGummiesProduct();
				cart_AllProducts.closeNewsletterOfferPopup();
			}

			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Products(0);
				cart_AllProducts.selectFirstAvailableGummiesProduct();
				cart_AllProducts.chooseFromGummyFlavorDropdown();
				cart_AllProducts.clickAddToCart();
				if (GOR.freeProductOfferPopUpHandled == false) {
					cart_AllProducts.closeFreeProductOfferPopup();
				}
				cart_AllProducts.clickShoppingCartLink();
			} else {
				headerAndfooter.clickOnMiniCart();
				miniCart.clickViewAndEditCart();
			}
			shoppingCart.clickApplyDiscountCodeHeader();
			shoppingCart.enterDiscountCode(validCouponCode);
			shoppingCart.clickApplyDiscount();
			shoppingCart.verifyRelevantMessage("Coupon Applied");
			shoppingCart.clickCancel_DiscountCoupon();
			shoppingCart.verifyRelevantMessage("Coupon Cancelled");
			testStepInfo("Valid Coupon was applied successfully");
		} catch (Exception e) {
			testStepFailed("Could not verify successful applying of valid coupon data");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || shoppingCart.testFailure || miniCart.testFailure
				|| headerAndfooter.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : ML_TS_056 Description : Verify the price is getting updated in
	 * the cart page while doing the below changes 1. Adding or Deleting products 2.
	 * Modifying the quantity 3. Applying Coupon Code 4. Updating shipping address
	 */
	public void verifyPriceUpdateInCart() {
		try {

			String validCouponCode = retrieve("validCouponCode");
			String state_Shipping = retrieve("ShippingState");
			String zipCode_Shipping = retrieve("ShippingZipCode");
			float price_first = 0;
			float price_second;
			float price_shipping = 0;

			if (GOR.agePopUpHandled == false) {
				headerAndfooter.clickOver21Age();
			}

			headerAndfooter.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				headerAndfooter.goTo_Products(0);
				cart_AllProducts.selectFirstAvailableGummiesProduct();
				cart_AllProducts.closeNewsletterOfferPopup();
			}

			headerAndfooter.clickOnMiniCart();
			miniCart.removeAllProducts();
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Products(0);
				cart_AllProducts.selectFirstAvailableGummiesProduct();
				cart_AllProducts.chooseFromGummyFlavorDropdown();
				price_first = cart_AllProducts.getProductPrice();
				cart_AllProducts.clickAddToCart();
				if (GOR.freeProductOfferPopUpHandled == false) {
					cart_AllProducts.closeFreeProductOfferPopup();
				}
				cart_AllProducts.clickShoppingCartLink();
			} else {
				headerAndfooter.clickOnMiniCart();
				miniCart.clickViewAndEditCart();
			}
			headerAndfooter.goTo_Products(0);
			cart_AllProducts.selectSecondProduct_NonGummies();
			price_second = cart_AllProducts.getProductPrice();
			cart_AllProducts.clickAddToCart();
			if (GOR.freeProductOfferPopUpHandled == false) {
				cart_AllProducts.closeFreeProductOfferPopup();
			}
			cart_AllProducts.clickShoppingCartLink();
			shoppingCart.collapseEstimateShippingHeader();
			shoppingCart.clickDesiredShippingOption("Standard");
			price_shipping = shoppingCart.getShippingPrice();
			shoppingCart.checkOrderTotal(price_first, price_second, price_shipping);
			shoppingCart.clickOnRemoveProduct();
			price_shipping = shoppingCart.getShippingPrice();
			shoppingCart.checkOrderTotal(price_second, 0, price_shipping);
			shoppingCart.updateCartQuantity(30);
			shoppingCart.clickUpdateCart();
			shoppingCart.collapseEstimateShippingHeader();
			shoppingCart.clickDesiredShippingOption("FreeExpedited");
			shoppingCart.clickUpdateCart();
			price_second = price_second * 30;
			price_second = (float) (Math.round((price_second) * 30) / 30.0);
			shoppingCart.checkOrderTotal(0, price_second, 0);
			waitTime(3);
			shoppingCart.clickApplyDiscountCodeHeader();
			shoppingCart.enterDiscountCode(validCouponCode);
			shoppingCart.clickApplyDiscount();
			shoppingCart.verifyRelevantMessage("Coupon Applied");
			price_first = shoppingCart.getDiscountPrice();
			shoppingCart.checkOrderTotal((-price_first), (price_second), 0);
			shoppingCart.clickCancel_DiscountCoupon();
			shoppingCart.verifyRelevantMessage("Coupon Cancelled");
			shoppingCart.collapseEstimateShippingHeader();
			shoppingCart.fillShippingDetails(state_Shipping, zipCode_Shipping);
			shoppingCart.clickDesiredShippingOption("FreeExpedited");
			shoppingCart.clickUpdateCart();
			price_first = shoppingCart.getTaxPrice();
			shoppingCart.checkOrderTotal(price_second, price_first, 0);
		} catch (Exception e) {
			testStepFailed("Could not verify price updation for various scenarios in Cart Page");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || shoppingCart.testFailure || miniCart.testFailure
				|| headerAndfooter.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : ML_TS_058 Description :Verify if the user is able to choose the
	 * below shipping method and total price should be calculated based on the
	 * selection: orders less than $75 show USPS standard + UPS Overnight orders
	 * between $75 - $124.99 show USPS Free standard + UPS Overnight orders $125+
	 * show Free Expedited 2 day + UPS Overnight
	 */

	public void verifyPriceForShippingMethods() {
		try {

			String state_Shipping = retrieve("ShippingState");
			String zipCode_Shipping = retrieve("ShippingZipCode");
			float price_first = 0;
			float price_second;
			float price_third;
			float totalPrice = 0;
			int count = 0;

			if (GOR.agePopUpHandled == false) {
				headerAndfooter.clickOver21Age();
			}

			headerAndfooter.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				headerAndfooter.goTo_Products(0);
				cart_AllProducts.selectFirstAvailableGummiesProduct();
				cart_AllProducts.closeNewsletterOfferPopup();
			}

			headerAndfooter.clickOnMiniCart();
			miniCart.removeAllProducts();

			// With Vape Products
			headerAndfooter.goTo_Products(0);
			cart_AllProducts.selectFirstAvailableGummiesProduct();
			cart_AllProducts.chooseFromGummyFlavorDropdown();
			price_first = cart_AllProducts.getProductPrice();
			cart_AllProducts.chooseFromGummyFlavorDropdown();
			cart_AllProducts.clickAddToCart();
			if (GOR.freeProductOfferPopUpHandled == false) {
				cart_AllProducts.closeFreeProductOfferPopup();
			}
			headerAndfooter.goTo_Products(2);
			cart_AllProducts.selectFirstVapeProduct();
			price_second = cart_AllProducts.getProductPrice();
			cart_AllProducts.clickAddToCart();
			if (GOR.freeProductOfferPopUpHandled == false) {
				cart_AllProducts.closeFreeProductOfferPopup();
			}
			headerAndfooter.clickOnMiniCart();
			miniCart.clickViewAndEditCart();
			shoppingCart.collapseEstimateShippingHeader();
			shoppingCart.fillShippingDetails(state_Shipping, zipCode_Shipping);
			shoppingCart.clickDesiredShippingOption("Standard");
			price_third = shoppingCart.getShippingPrice();
			shoppingCart.checkOrderTotal(price_first, price_second, price_third);
			headerAndfooter.goTo_Home();
			price_second = price_second * 20;
			price_second = (float) (Math.round((price_second) * 20) / 20.0);
			headerAndfooter.clickOnMiniCart();
			miniCart.updateCartQuantity(20);
			miniCart.clickUpdateProductCountIcon();
			miniCart.clickViewAndEditCart();
			shoppingCart.collapseEstimateShippingHeader();
			shoppingCart.fillShippingDetails(state_Shipping, zipCode_Shipping);
			shoppingCart.clickDesiredShippingOption("FreeStandard");
			price_third = shoppingCart.getShippingPrice();
			shoppingCart.checkOrderTotal(price_first, price_second, price_third);
			headerAndfooter.clickLogo();
			waitTime(2);
			headerAndfooter.clickOnMiniCart();
			miniCart.removeAllProducts();

			// Without Vape Products
			headerAndfooter.goTo_Products(0);
			cart_AllProducts.selectFirstAvailableGummiesProduct();
			cart_AllProducts.chooseFromGummyFlavorDropdown();
			price_first = cart_AllProducts.getProductPrice();
			cart_AllProducts.clickAddToCart();
			if (GOR.freeProductOfferPopUpHandled == false) {
				cart_AllProducts.closeFreeProductOfferPopup();
			}
			cart_AllProducts.clickShoppingCartLink();
			shoppingCart.collapseEstimateShippingHeader();
			shoppingCart.fillShippingDetails(state_Shipping, zipCode_Shipping);
			shoppingCart.clickDesiredShippingOption("Standard");
			shoppingCart.clickUpdateCart();
			price_second = shoppingCart.getShippingPrice();
			shoppingCart.checkOrderTotal(price_first, price_second, 0);
			shoppingCart.clickDesiredShippingOption("Overnight");
			shoppingCart.clickUpdateCart();
			price_second = shoppingCart.getShippingPrice();
			shoppingCart.checkOrderTotal(price_first, price_second, 0);
			if (price_first < 7) {
				count = 15;
			} else if (price_first < 15) {
				count = 6;
			} else if (price_first < 24) {
				count = 4;
			} else if (price_first > 24 && price_first < 30) {
				count = 3;
			} else if (price_first > 30) {
				count = 2;
			}
			shoppingCart.updateCartQuantity(count);
			shoppingCart.clickUpdateCart();
			totalPrice = price_first * count;
			shoppingCart.collapseEstimateShippingHeader();
			shoppingCart.fillShippingDetails(state_Shipping, zipCode_Shipping);
			shoppingCart.fillShippingDetails(state_Shipping, "10017");
			shoppingCart.clickDesiredShippingOption("FreeStandard");
			shoppingCart.clickUpdateCart();
			shoppingCart.checkOrderTotal(totalPrice, 0, 0);
			shoppingCart.clickDesiredShippingOption("Overnight");
			shoppingCart.clickUpdateCart();
			price_second = shoppingCart.getShippingPrice();
			shoppingCart.checkOrderTotal(totalPrice, price_second, 0);
			shoppingCart.updateCartQuantity(30);
			shoppingCart.clickUpdateCart();
			totalPrice = price_first * 30;
			totalPrice = (float) (Math.round((totalPrice) * 30) / 30.0);
			shoppingCart.clickDesiredShippingOption("FreeExpedited");
			shoppingCart.clickUpdateCart();
			shoppingCart.checkOrderTotal(totalPrice, 0, 0);
			shoppingCart.clickDesiredShippingOption("Overnight");
			shoppingCart.clickUpdateCart();
			price_second = shoppingCart.getShippingPrice();
			shoppingCart.checkOrderTotal(totalPrice, price_second, 0);
			shoppingCart.removeAllProducts();

		} catch (Exception e) {
			testStepFailed("Could not verify price updation for different shipping methods in Cart Page");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || shoppingCart.testFailure || miniCart.testFailure
				|| headerAndfooter.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : ML_TS_059 Description : Verify the sales tax is getting
	 * calculated (Only For FL, WA) based on the given shipping address
	 */
	// Works only for Prod. Since tax is disabled for lower env.
	public void verifySalesTax() {
		try {

			String state_Shipping = retrieve("ShippingState");
			String secondState_Shipping = retrieve("ShippingStateTwo");
			String zipCode_Shipping = retrieve("ShippingZipCode");
			String secondZipCode_Shipping = retrieve("ShippingZipCodeTwo");

			float price_first = 0;
			float price_second;

			if (GOR.agePopUpHandled == false) {
				headerAndfooter.clickOver21Age();
			}

			headerAndfooter.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				headerAndfooter.goTo_Products(0);
				cart_AllProducts.selectFirstAvailableGummiesProduct();
				cart_AllProducts.closeNewsletterOfferPopup();
			}

			headerAndfooter.clickOnMiniCart();
			miniCart.removeAllProducts();
			headerAndfooter.goTo_Products(0);
			cart_AllProducts.selectFirstAvailableGummiesProduct();
			cart_AllProducts.chooseFromGummyFlavorDropdown();
			price_first = cart_AllProducts.getProductPrice();
			cart_AllProducts.clickAddToCart();
			if (GOR.freeProductOfferPopUpHandled == false) {
				cart_AllProducts.closeFreeProductOfferPopup();
			}
			cart_AllProducts.clickShoppingCartLink();
			shoppingCart.updateCartQuantity(30);
			shoppingCart.clickUpdateCart();
			shoppingCart.collapseEstimateShippingHeader();
			shoppingCart.fillShippingDetails(state_Shipping, zipCode_Shipping);
			shoppingCart.clickDesiredShippingOption("FreeExpedited");
			shoppingCart.clickUpdateCart();
			price_first = price_first * 30;
			price_first = (float) (Math.round((price_first) * 30) / 30.0);
			price_second = shoppingCart.getTaxPrice();
			shoppingCart.checkOrderTotal(price_first, price_second, 0);
			shoppingCart.fillShippingDetails(secondState_Shipping, secondZipCode_Shipping);
			shoppingCart.clickUpdateCart();
			price_second = shoppingCart.getTaxPrice();
			shoppingCart.checkOrderTotal(price_first, price_second, 0);
		} catch (Exception e) {
			testStepFailed("Could not verify sales tax for desired states in Cart Page");
		}
		if (obj.testFailure || cart_AllProducts.testFailure || shoppingCart.testFailure || miniCart.testFailure
				|| headerAndfooter.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

}
