package scenarios.Search;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.Cart_AllProducts;
import pages.HeaderAndFooters;
import pages.HomePage;

public class Search extends ApplicationKeywords {
	BaseClass obj;
	HeaderAndFooters headerAndFooters;
	HomePage homepage;
	Cart_AllProducts cartAllProducts;
	private boolean status = false;

	public Search(BaseClass obj) {
		super(obj);
		headerAndFooters = new HeaderAndFooters(obj);
		homepage = new HomePage(obj);
		cartAllProducts = new Cart_AllProducts(obj);
		this.obj = obj;
	}

	/*
	 * TestCaseid : Search_ML_TS_014 Description : To Verify 'no results' page is
	 * displayed upon searching with invalid / not available products.
	 */
	public void invalid_Search() {
		try {

			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0);
				cartAllProducts.selectFirstAvailableGummiesProduct();
				cartAllProducts.closeNewsletterOfferPopup();
			}

			if (GOR.loggedIn == true) {
				headerAndFooters.navigateMyAccountMenu("signOut");
				headerAndFooters.clickOver21Age();
			}

			headerAndFooters.goTo_Home();
			headerAndFooters.enterQuery_SearchBox(retrieve("invalidLengthQuery"));
			headerAndFooters.verifyNavigation("Minimum Query Length");
			headerAndFooters.enterQuery_SearchBox(retrieve("invalidQuery"));
			headerAndFooters.clickSearchButton();
			headerAndFooters.verifyNavigation("noSearchResults Message");
		} catch (Exception e) {
			testStepFailed("Invalid Search could not be verified");
		}
		if (obj.testFailure || homepage.testFailure || headerAndFooters.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Search_ML_TS_012_013 Description : To Verify the user is able to
	 * search the valid products and user is able to see the proper results based on
	 * the search criteria.
	 */
	public void valid_Search() {
		try {

			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0);
				cartAllProducts.selectFirstAvailableGummiesProduct();
				cartAllProducts.closeNewsletterOfferPopup();
			}

			if (GOR.loggedIn == true) {
				headerAndFooters.navigateMyAccountMenu("signOut");
				headerAndFooters.clickOver21Age();
			}

			headerAndFooters.goTo_Home();
			headerAndFooters.enterQuery_SearchBox(retrieve("validQuery"));
			headerAndFooters.clickSearchButton();
			headerAndFooters.verifyNavigation("Number of Items");
			headerAndFooters.verifyNavigation("Gummies Search Product Title");

		} catch (Exception e) {
			testStepFailed("Valid Search could not be verified");
		}
		if (obj.testFailure || homepage.testFailure || headerAndFooters.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

}
