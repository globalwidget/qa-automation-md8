package scenarios.Login_Logout;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.Cart_AllProducts;
import pages.HeaderAndFooters;
import pages.HomePage;
import pages.SignIn;

public class Login_Logout extends ApplicationKeywords {
	BaseClass obj;
	SignIn signIn;
	HeaderAndFooters headerAndFooters;
	HomePage homepage;
	Cart_AllProducts cartAllProducts;
	private boolean status = false;

	public Login_Logout(BaseClass obj) {
		super(obj);
		signIn = new SignIn(obj);
		headerAndFooters = new HeaderAndFooters(obj);
		homepage = new HomePage(obj);
		cartAllProducts = new Cart_AllProducts(obj);
		this.obj = obj;
	}

	/*
	 * TestCaseid : Login_ML_TS_007 Description : To Verify if the user is able to
	 * login to Mystic Labs application with valid credentials.
	 */
	public void login_ValidDetails() {
		try {

			String email = retrieve("email");
			String password = retrieve("password");

			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0);
				cartAllProducts.selectFirstAvailableGummiesProduct();
				cartAllProducts.closeNewsletterOfferPopup();
			}

			headerAndFooters.goTo_Home();
			headerAndFooters.goToSignIn();
			signIn.signIn(email, password);
		} catch (Exception e) {
			testStepFailed("Login using valid credentials could not be verified");
		}
		if (obj.testFailure || headerAndFooters.testFailure || signIn.testFailure || homepage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Login_ML_TS_008 Description : To Verify if the user is not able
	 * to create a new account with invalid details.
	 */
	public void login_InvalidDetails() {
		try {
			String invalidEmail = retrieve("invalidEmail");
			String invalidPassword = retrieve("invalidPassword");
			String email = retrieve("email");

			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0);
				cartAllProducts.selectFirstAvailableGummiesProduct();
				cartAllProducts.closeNewsletterOfferPopup();
			}

			if (GOR.loggedIn == true) {
				headerAndFooters.navigateMyAccountMenu("signOut");
				headerAndFooters.clickOver21Age();
			}

			headerAndFooters.goTo_Home();
			headerAndFooters.goToSignIn();
			signIn.signIn("", "");
			signIn.verifyErrorMessage_SignIn("emailEmpty");
			signIn.verifyErrorMessage_SignIn("passwordEmpty");
			signIn.signIn(invalidEmail, invalidPassword);
			signIn.verifyErrorMessage_SignIn("emailInvalid");
			signIn.signIn(email, invalidPassword);
			signIn.verifyErrorMessage_SignIn("invalidCredentialsError");

		} catch (Exception e) {
			testStepFailed("Error message for Login using Invalid Credentials could not be verified");
		}
		if (obj.testFailure || headerAndFooters.testFailure || signIn.testFailure || homepage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Logout_ML_TS_011 Description : To Verify if the user is able to
	 * logout in Mystic Labs application.
	 */
	public void logout() {
		try {

			String email = retrieve("email");
			String password = retrieve("password");

			if (GOR.agePopUpHandled == false) {
				headerAndFooters.clickOver21Age();
			}

			if (GOR.OfferPopUpHandled == false) {
				headerAndFooters.goTo_Products(0);
				cartAllProducts.selectFirstAvailableGummiesProduct();
				cartAllProducts.closeNewsletterOfferPopup();
			}

			if (GOR.loggedIn == false) {
				headerAndFooters.goTo_Home();
				headerAndFooters.goToSignIn();
				signIn.signIn(email, password);
			}
			headerAndFooters.navigateMyAccountMenu("signOut");
			headerAndFooters.clickOver21Age();
			if (GOR.loggedIn == true) {
				testStepInfo("User already Logged in");
			}
			headerAndFooters.goToSignIn();
			headerAndFooters.verifyNavigation("SignInPage");
			testStepInfo("Successfully logged out from the user account");

		} catch (Exception e) {
			testStepFailed("Logout could not be verified");
		}
		if (obj.testFailure || headerAndFooters.testFailure || signIn.testFailure || homepage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

}
